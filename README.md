# Introduction
This is a demo project whose main function is to help customers manage their loans. The customer can register a new loan, view and repay their loan at schedule.

# Why Hexagon and DDD Architecture
The most important reason is this architecture helps the application separate concerns, flexibility, and maintainability. This design pattern keeps the core business logic independent of external interfaces. So, the core business logic can serve many external interfaces such as web interfaces, databases, and external APIs and does not depend on infrastructures.

Another key reason is to strongly align with the Dependency Inversion Principle by relying on interfaces (ports) that are implemented by adapters, allowing the core to depend on abstractions. The loose coupling will help the application be more flexible, testable, and maintainable

# Business Logic Explanation
1.  I don't know how in your country, in Vietnam's banking industry, when we apply for a new loan, we must register a bank account and use it to repay our loans, make online payments, transfer money, and so on.
2. When the customers apply for a new loan and do not have any bank account, the System will create a new bank account for the customer.
3. The customer's loan is one type of bank account. It's just the same with the credit card account.
4. The customers have 2 options to repay their loan.
- First, they can use their bank account to transfer to their loan account. If the transferred amount is greater than the debt amount in the current period, the System only subtracts exactly the debt amount.
- Second, the customer also can deposit money to repay their loan. If the cash-in amount is greater than the debt amount in the period, the excessive amount will be added to their bank account.
5. The customer's loan is activated from the date of approval and the start date is the date of approval.
6. The scheduled repayment information will be created when the due date of the previous period has passed.
7. The customer's loan is complete when all of the scheduled repayments are completed

# Installation
<p align="center">
    <a href="https://php.net"><img src="https://img.shields.io/badge/PHP-7.2-7A86B8.svg?logo=php" alt="php"/></a>
    <a href="https://phalcon.io"><img src="https://img.shields.io/badge/phalcon-3.4-99ddc0.svg?&style=flat-square&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjc3RTA0NTM1MEE1ODExRUFBNENEOUNDREQ1RDJGQ0FBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjc3RTA0NTM2MEE1ODExRUFBNENEOUNDREQ1RDJGQ0FBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NzdFMDQ1MzMwQTU4MTFFQUE0Q0Q5Q0NERDVEMkZDQUEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NzdFMDQ1MzQwQTU4MTFFQUE0Q0Q5Q0NERDVEMkZDQUEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4mTnOmAAACyklEQVR42nxSyU8TcRj9fvObaQsMhS5UW8ACUkG2sEqiLCGRi0S2gxoPGmP8B4xHa6IHPRg0kZh4ksQgIhE5CCYmKC4NrTExlgqhbLULDJ1pZWmhnbYz4wBe9SUv3+W9vOR9DzXU1gEmSbh67zpYqspoVZLoJABeAiBBAgn2IIkCiJIIlKyb8y7DpcZOIBIpsSce3Wl9OzgG/s2gfkdM3JQkNCbrNfAfEPqe1oAqXTngcfx88bn/1VEmFFyLkamzctaknFj+T6OhrZHJv3w6TBu1F75OfHwzZO1X2O3TsSjwtZRCOSVruhAgINABkcw9YM+X79tpKVGT21beTpv0FDv3y+x656CWF5cgS6/JMOblnttN8vaYJKwIsoHb+A21+aWAuzraoMJc5Eqy0QrBkFaibSyWe0EQ+OYGx/gUaHValDBpDCNzM8POECuuMwHorDwJiBEZ4BM8RDaTeU7nzHBY2j0VouLgda9AaNYL8dUNaO8949/JwSVcNBLjI3EYuHILcF1pHTRVN4GB1m67JXZKBOl4UbbRaCksRqYaCyIrD0GGLotuMJfVL6z7TDwA01PVvIkZhgGO5cA974bx9xObId3u8w2eP+LfYqu0RDpWCwrkm18Jp0tkQndY3xJkgue7alofEySB4fXoKDzqe4jXFj1qTbH5NkK4u7m4WlmYaUAL4/Znk31DNT8mp08UENpjgRHb3b1WSZGmQJFGglKvxpaOlhvupzZrc1ktcLqN7Q+2T4LXv5CuUinzF2fdq+sPngjsWnBw31hvvQjyngDotOSS3eniHR7B0NuNbb45QsSSFacghSlyjWNY4JigHKLa/yPJBzmIyAQVJUUDvnljgfG+Vq8vo7ZUFlpBX4t5QlZMYR+SiIPJSAf7JXfCEQj7w0BSCsgQVV6WW72jlqiCvJwCPO1azI7x8SVamQnEX4Moivv3jwADAA9sObdCbqMjAAAAAElFTkSuQmCC" alt="phalcon"/></a>
    <a href="https://www.mysql.com"><img src="https://img.shields.io/badge/MySQL-8.0-4479A1.svg?logo=mysql&logoColor=yellow" alt="MySQL"/></a>
    <a href="https://www.docker.com"><img src="https://img.shields.io/badge/Docker-23.0.5-1d63ed.svg?logo=docker" alt="Docker"/></a>
</p>

Note: Php and Phalcon must follow the version requirements. MySQL and Docker can use lower stable versions. However, the above versions are recommended because the application is running stably with them

# 🚀 Docker Environment Setup
1. [Install Docker](https://www.docker.com/get-started)
2. Clone this project: `git clone https://gitlab.com/cuibap_tiensinh/aspire.git`
3. Move to the docker folder inside project
4. Start docker: `docker-compose up -d`
5. Go to the `http://localhost:8000` to get `Hello World`

Note: The local environment variables are stored in `.env` file inside `docker` folder. You can change it if needed

# Project Operation
1. The administrator account: aspire01/aspire01
2. The customer account and the administrator account are different, so do not use the same access token for both
3. There are 2 cronjob:
- LoanStatusTask: run every 2 minutes to update a repayment status of loan.
- ScheduleRepaymentLoanTask: run every minute to create a next scheduled repayment.
4. The loan and bank account have a currency. When the customer deposits to a bank account or repays the loan with another currency, the system converts automatically to bank account currency or loan currency. The supported currency list: USD, EUR, JPY, GBP, CAD, and CHF
5. The endpoints and their action meaning:
- For the administrator
   - [POST] `{{aspire}}/admin/login` using to log in to the account for the admin user.
   - [GET] `{{aspire}}/admin/loans/{loan_id}` using to show the loan detail for the admin user.
   - [GET] `{{aspire}}/admin/loans?page=1&size=10` using to show the loan list for the admin user.
   - [PUT] `{{aspire}}/admin/loans/{loan_id}/approval` using to approve the loan for the admin user
- For the customer
   - [POST] `{{aspire}}/customers` using to register a new the customer account
   - [POST] `{{aspire}}/customers/login` using to log in to the account for the customer
   - [GET] `{{aspire}}/customers/me` using to show the customer's registered information
   - [POST] `{{aspire}}/loans` using to apply for a new loan for the customer
   - [GET] `{{aspire}}/loans?page=1&size=10` using to show the customer's loan list
   - [GET] `{{aspire}}/loans/{loan_id}` using to show the customer's loan detail
   - [POST] `{{aspire}}/loans/{loan_id}/repayments/transfer` using to repay the customer's loan by transferring money
   - [POST] `{{aspire}}/loans/{loan_id}/repayments/cash-in-offline` using to repay the customer's loan by depositing money at cashiers
   - [GET] `{{aspire}}/bank-accounts?page=1&size=10` using to show the customer's bank account list
   - [POST] `{{aspire}}/bank-accounts/{bank_account_id}/deposit` using to deposit money to the customer's bank account

# File Structure

```
aspire
├── public
├── docker
└── src
    ├── Application
    │   ├── Api
    │   │   ├── Admin
    │   │   │   ├── Controllers
    │   │   ├── Client
    │   │   │   ├── Controllers
    │   │   └── Configs
    │   └── Console
    │       ├── Configs
    │       └── Tasks
    │           └── Cronjob
    ├── Common
    ├── Core
    │   ├── Application
    │   │   ├── Admin
    │   │   ├── Auth
    │   │   ├── CurrencyRate
    │   │   ├── Customer
    │   │   ├── CustomerBankAccount
    │   │   └── CustomerLoan
    │   └── Domain
    │       ├── Models
    │       ├── Repositories
    │       └── ValueObjects
    └── Infrastructure
        └── Mysql
            ├── Mapper
            └── Record
```

# Database structure
![Database Diagram](db-diagram.png "Database Diagram")
1. `admin_security` stores the sensitive information of the admin account, like username, password, access_token
2. `customer_security` stores the sensitive information of the customer account, like username, password, access_token
3. `customer` stores the common information of the customer, like name, email, phone
4. `customer_bank_account` is the primary wallet to hold the money of the customer. The customer will deposit to this account and use it for payment or repayment.
   - `version` is increased automatically value when updated
5. `customer_balance_history` records the change of the customer account balance, including deposit and withdraw
6. `customer_loan` is the loan account of the customer. It's just like a credit card account
   - `scheduled at` is the nearest scheduled date for repayment.
   - `repaid_count` is the number of times that the customer repays the full amount according to the schedule.
   - `repaid_amount` is the total money that the customer repays the full amount according to the schedule.
   - **So, the loan is complete when amount = repaid_amount and loan_term = repaid_count**
7. `customer_scheduled_repayment` is the customer's debt information for this period
   - `version` is increased automatically value when updated
   - `due_amount` is the amount that the customer must repay at the period
   - `due_at` is the time that the customer must repay at the period
   - `status` has `PENDING` value when created and is PAID after the customer completes repayment
   - `repay_type` has `NONE` value when created. Then, its value depends on the customer's repayment method. If the customer repays by transferring money from the main account, the value will be TRANSFER. If the customer repays by depositing money, the value will be CASH_IN
8. `customer_repayment_history` records the repayment of the customer for their loan

# Unit Test
The Unit Test is applied to this project. Use command below to run:

1. Run this command to start unit test and build coverage report

```docker exec aspire php ./vendor/bin/phpunit```
2. Go to http://localhost:8000/report/index.html to view report


# Postman collection
Here is the Postman collection [Postman](Aspire.postman_collection.json "Postman collection")

# Contact
If you have any trouble, please feel free to contact me:

[![Gmail](https://img.shields.io/badge/dangquocthang0101@gmail.com-white.svg?logo=gmail "Gmail")](mailto:dangquocthang0101@gmail.com)
[![Telegram](https://img.shields.io/badge/dangquocthang0101-white.svg?logo=telegram "Telegram")](t.me/dangquocthang0101)
[![LinkedIn](https://img.shields.io/badge/dang--quoc--thang0101-white.svg?logo=linkedin&logoColor=blue "LinkedIn")](https://www.linkedin.com/in/dang-quoc-thang)
