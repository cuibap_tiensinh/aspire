-- Host: localhost    Database: aspire
-- ------------------------------------------------------
-- Server version       8.0.32

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
                            `id` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
                            `customer_name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
                            `customer_phone` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
                            `customer_email` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
                            `created_at` datetime,
                            `updated_at` datetime,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `customer_balance_history`
--

DROP TABLE IF EXISTS `customer_balance_history`;
CREATE TABLE `customer_balance_history` (
                                            `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            `customer_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            `bank_account_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            `deposit_amount` decimal(20,4) NOT NULL,
                                            `withdraw_amount` decimal(20,4) NOT NULL,
                                            `submitted_deposit_amount` decimal(20,4) NOT NULL,
                                            `submitted_withdraw_amount` decimal(20,4) NOT NULL,
                                            `account_currency` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            `submitted_currency` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                            `exchange_rate` double(10,4) NOT NULL,
  `reason` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `version` bigint unsigned NOT NULL,
  `created_at` datetime,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `customer_bank_account`
--

DROP TABLE IF EXISTS `customer_bank_account`;
CREATE TABLE `customer_bank_account` (
                                         `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                         `customer_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                         `balance` decimal(20,4) NOT NULL,
                                         `currency` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                         `version` bigint unsigned NOT NULL,
                                         `is_primary` tinyint unsigned NOT NULL,
                                         `created_at` datetime,
                                         `updated_at` datetime,
                                         PRIMARY KEY (`id`),
                                         KEY `customer_id_idx` (`customer_id`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `customer_loan`
--

DROP TABLE IF EXISTS `customer_loan`;
CREATE TABLE `customer_loan` (
                                 `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `customer_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `amount` decimal(20,4) NOT NULL,
                                 `currency` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `loan_term` smallint unsigned NOT NULL,
                                 `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `approved_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                 `started_at` datetime,
                                 `ended_at` datetime,
                                 `scheduled_at` datetime,
                                 `repaid_count` smallint unsigned DEFAULT '0',
                                 `repaid_amount` decimal(20,4) NOT NULL DEFAULT '0',
                                 `created_at` datetime,
                                 `updated_at` datetime,
                                 PRIMARY KEY (`id`),
                                 KEY `customer_status_idx` (`customer_id`,`status`),
                                 KEY `status_scheduled_at_idx` (`status`,`scheduled_at`) USING BTREE,
                                 KEY `amount_loan_term_idx` (`amount`,`loan_term`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `customer_repayment_history`
--

DROP TABLE IF EXISTS `customer_repayment_history`;
CREATE TABLE `customer_repayment_history` (
                                              `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                              `customer_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                              `bank_account_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                              `loan_account_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                              `repaid_amount` decimal(20,4) NOT NULL,
                                              `remaining_due_amount_in_period` decimal(20,4) NOT NULL,
                                              `currency` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                              `version` bigint unsigned NOT NULL,
                                              `repaid_type` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
                                              `created_at` datetime,
                                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `customer_scheduled_repayment`
--

DROP TABLE IF EXISTS `customer_scheduled_repayment`;
CREATE TABLE `customer_scheduled_repayment` (
                                                `id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                                `customer_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                                `loan_account_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                                `due_amount` decimal(20,4) NOT NULL,
                                                `currency` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                                `due_at` datetime,
                                                `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                                `version` bigint unsigned NOT NULL,
                                                `repaid_type` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
                                                `created_at` datetime,
                                                `updated_at` datetime,
                                                PRIMARY KEY (`id`),
                                                KEY `loan_customer_status_due_amount_idx` (`loan_account_id`,`customer_id`,`status`,`due_amount`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `customer_security`
--

DROP TABLE IF EXISTS `customer_security`;
CREATE TABLE `customer_security` (
                                     `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                     `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                     `password` varchar(72) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                     `account_token` varchar(40) COLLATE utf8mb4_general_ci NOT NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `admin_security_UN` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `admin_security`;
CREATE TABLE `admin_security` (
                                     `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                     `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                     `password` varchar(72) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                     `account_token` varchar(40) COLLATE utf8mb4_general_ci NOT NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `admin_security_UN` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO aspire.admin_security
(id, username, password, account_token)
VALUES('aspire01', 'aspire01', '$2a$12$0pGGWb.BdW2w9/rGk3Jh2uUqgeUPdikpMSLGsd6UyFWS6znsMrZT6', '52cc152f76ab83f5bc5ff8a83f342405');




