<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerSecurity;
use App\Core\Domain\Repositories\CustomerSecurityRepository;
use App\Infrastructure\Mysql\Mapper\CustomerSecurityRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerSecurityRecord;
use Phalcon\Db\Column;

class MysqlCustomerSecurityRepository extends MysqlRepositoryBase implements CustomerSecurityRepository
{
    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerSecurityRecordMapper();
    }

    /**
     * @return CustomerSecurityRecordMapper
     */
    protected function getMapper(): CustomerSecurityRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerSecurityRecord::class;
    }

    /**
     * @param CustomerSecurity $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(CustomerSecurity $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param CustomerSecurity $model
     * @return bool
     * @throws BusinessException
     */
    public function update(CustomerSecurity $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return CustomerSecurity|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerSecurity
    {
        return $this->fetchOneBase($id, $forUpdate);
    }

    /**
     * @param string $username
     * @return CustomerSecurity|null
     * @throws BusinessException
     */
    public function fetchOneByUsername(string $username): ?CustomerSecurity
    {
        try {
            $record = CustomerSecurityRecord::findFirst([
                'conditions' => 'username = :username:',
                'bind' => [
                    'username' => $username,
                ],
                'bindTypes' => [
                    'username' => Column::BIND_PARAM_STR,
                ],
            ]);

            return $record != null ? $this->mapper->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}