<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\Customer;
use App\Core\Domain\Repositories\CustomerRepository;
use App\Infrastructure\Mysql\Mapper\CustomerRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerRecord;
use Exception;
use Phalcon\Db\Column;

class MysqlCustomerRepository extends MysqlRepositoryBase implements CustomerRepository
{
    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerRecordMapper();
    }

    protected function getMapper(): CustomerRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerRecord::class;
    }

    /**
     * @param Customer $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(Customer $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param Customer $model
     * @return bool
     * @throws BusinessException
     */
    public function update(Customer $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return Customer|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?Customer
    {
        return $this->fetchOneBase($id, $forUpdate);
    }

    /**
     * @param string $id
     * @return bool
     * @throws BusinessException
     */
    public function isExisted(string $id): bool
    {
        try {
            $rowcount = CustomerRecord::count([
                'conditions' => 'id = :id:',
                'bind' => ['id' => $id],
                'bindTypes' => ['id' => Column::BIND_PARAM_STR]
            ]);

            return $rowcount > 0;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}