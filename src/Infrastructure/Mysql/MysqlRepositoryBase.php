<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\ModelBase;
use Phalcon\Db\Column;

abstract class MysqlRepositoryBase
{
    private $db;
    protected abstract function getMapper();
    protected abstract function getRecordClass(): string;

    public function __construct()
    {
        $this->db = \Phalcon\DI::getDefault()->getShared('db');
    }

    /**
     * @param ModelBase $model
     * @return bool
     * @throws BusinessException
     */
    protected function insertBase(ModelBase $model): bool
    {
        try {
            $record = $this->getMapper()->toRecord($model);
            if($record->create()) {
                return true;
            }

            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::DATABASE_ERROR);
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param ModelBase $model
     * @return bool
     * @throws BusinessException
     */
    protected function updateBase(ModelBase $model): bool
    {
        try {
            $record = $this->getMapper()->toRecord($model);
            $record->setDirtyState(\Phalcon\Mvc\Model::DIRTY_STATE_PERSISTENT);
            if($record->update()) {
                return true;
            }

            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::DATABASE_ERROR);
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException($exception->getMessage(), ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return null
     * @throws BusinessException
     */
    protected function fetchOneBase(string $id, bool $forUpdate = false)
    {
        try {
            $record = $this->getRecordClass()::findFirst([
                'conditions' => 'id = :id:',
                'bind' => ['id' => $id],
                'bindTypes' => [
                    'id' => Column::BIND_PARAM_STR,
                ],
                'for_update' => $forUpdate
            ]);

            return $record != null ? $this->getMapper()->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}