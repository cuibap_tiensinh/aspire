<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\CustomerRepaymentHistory;
use App\Infrastructure\Mysql\Record\CustomerRepaymentHistoryRecord;
use Exception;

class CustomerRepaymentHistoryRecordMapper extends MapperBase
{

    /**
     * @param CustomerRepaymentHistory $model
     * @return CustomerRepaymentHistoryRecord
     */
    public function toRecord($model): CustomerRepaymentHistoryRecord
    {
        return (new CustomerRepaymentHistoryRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setBankAccountId($model->getBankAccountId())
            ->setLoanAccountId($model->getLoanAccountId())
            ->setRepaidAmount($model->getRepaidAmount())
            ->setRemainingDueAmountInPeriod($model->getRemainingDueAmountInPeriod())
            ->setCurrency($model->getCurrency())
            ->setVersion($model->getVersion())
            ->setRepaidType($model->getRepaidType())
            ->setCreatedAt($model->getCreatedAt());
    }

    /**
     * @param CustomerRepaymentHistoryRecord $record
     * @return CustomerRepaymentHistory
     * @throws Exception
     */
    public function toModel($record): CustomerRepaymentHistory
    {
        return (new CustomerRepaymentHistory())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setBankAccountId($record->getBankAccountId())
            ->setLoanAccountId($record->getLoanAccountId())
            ->setRepaidAmount($record->getRepaidAmount())
            ->setRemainingDueAmountInPeriod($record->getRemainingDueAmountInPeriod())
            ->setCurrency($record->getCurrency())
            ->setVersion($record->getVersion())
            ->setRepaidType($record->getRepaidType())
            ->setCreatedAt($record->getCreatedAtAsDateTime());
    }
}