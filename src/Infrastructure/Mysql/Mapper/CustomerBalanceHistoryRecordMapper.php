<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\CustomerBalanceHistory;
use App\Infrastructure\Mysql\Record\CustomerBalanceHistoryRecord;
use Exception;

class CustomerBalanceHistoryRecordMapper extends MapperBase
{
    /**
     * @param CustomerBalanceHistory $model
     * @return CustomerBalanceHistoryRecord
     */
    public function toRecord($model): CustomerBalanceHistoryRecord
    {
        return (new CustomerBalanceHistoryRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setBankAccountId($model->getBankAccountId())
            ->setDepositAmount($model->getDepositAmount())
            ->setWithdrawAmount($model->getWithdrawAmount())
            ->setAccountCurrency($model->getAccountCurrency())
            ->setSubmittedDepositAmount($model->getSubmittedDepositAmount())
            ->setSubmittedWithdrawAmount($model->getSubmittedWithdrawAmount())
            ->setSubmittedCurrency($model->getSubmittedCurrency())
            ->setExchangeRate($model->getExchangeRate())
            ->setVersion($model->getVersion())
            ->setReason($model->getReason())
            ->setCreatedAt($model->getCreatedAt());
    }

    /**
     * @param CustomerBalanceHistoryRecord $record
     * @return CustomerBalanceHistory
     * @throws Exception
     */
    public function toModel($record): CustomerBalanceHistory
    {
        return (new CustomerBalanceHistory())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setBankAccountId($record->getBankAccountId())
            ->setDepositAmount($record->getDepositAmount())
            ->setWithdrawAmount($record->getWithdrawAmount())
            ->setAccountCurrency($record->getAccountCurrency())
            ->setSubmittedDepositAmount($record->getSubmittedDepositAmount())
            ->setSubmittedWithdrawAmount($record->getSubmittedWithdrawAmount())
            ->setSubmittedCurrency($record->getSubmittedCurrency())
            ->setExchangeRate($record->getExchangeRate())
            ->setVersion($record->getVersion())
            ->setReason($record->getReason())
            ->setCreatedAt($record->getCreatedAtAsDateTime());
    }
}