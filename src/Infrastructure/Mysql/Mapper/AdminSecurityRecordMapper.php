<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\AdminSecurity;
use App\Infrastructure\Mysql\Record\AdminSecurityRecord;

class AdminSecurityRecordMapper extends MapperBase
{
    /**
     * @param AdminSecurity $model
     * @return AdminSecurityRecord
     */
    public function toRecord($model): AdminSecurityRecord
    {
        return (new AdminSecurityRecord())
            ->setId($model->getId())
            ->setUsername($model->getUsername())
            ->setPassword($model->getPassword())
            ->setAccountToken($model->getAccountToken());
    }

    /**
     * @param AdminSecurityRecord $record
     * @return AdminSecurity
     */
    public function toModel($record): AdminSecurity
    {
        return (new AdminSecurity())
            ->setId($record->getId())
            ->setUsername($record->getUsername())
            ->setPassword($record->getPassword())
            ->setAccountToken($record->getAccountToken());
    }
}