<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\CustomerLoan;
use App\Infrastructure\Mysql\Record\CustomerLoanRecord;
use Exception;

class CustomerLoanRecordMapper extends MapperBase
{

    /**
     * @param CustomerLoan $model
     * @return CustomerLoanRecord
     */
    public function toRecord($model): CustomerLoanRecord
    {
        return (new CustomerLoanRecord())
            ->setCustomerId($model->getCustomerId())
            ->setId($model->getId())
            ->setAmount($model->getBalance())
            ->setCurrency($model->getCurrency())
            ->setLoanTerm($model->getLoanTerm())
            ->setStatus($model->getStatus())
            ->setApprovedBy($model->getApprovedBy())
            ->setStartedAt($model->getStartedAt())
            ->setEndedAt($model->getEndedAt())
            ->setScheduledAt($model->getScheduledAt())
            ->setRepaidCount($model->getRepaidCount())
            ->setRepaidAmount($model->getRepaidAmount())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());
    }

    /**
     * @param CustomerLoanRecord $record
     * @return CustomerLoan
     * @throws Exception
     */
    public function toModel($record): CustomerLoan
    {
        return (new CustomerLoan())
            ->setCustomerId($record->getCustomerId())
            ->setId($record->getId())
            ->setBalance($record->getAmount())
            ->setCurrency($record->getCurrency())
            ->setLoanTerm($record->getLoanTerm())
            ->setStatus($record->getStatus())
            ->setApprovedBy($record->getApprovedBy())
            ->setStartedAt($record->getStartedAtAsDateTime())
            ->setEndedAt($record->getEndedAtAsDateTime())
            ->setScheduledAt($record->getScheduledAtAsDateTime())
            ->setRepaidCount($record->getRepaidCount())
            ->setRepaidAmount($record->getRepaidAmount())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime())
            ->setVersion(1)
            ->setIsPrimary(false);
    }
}