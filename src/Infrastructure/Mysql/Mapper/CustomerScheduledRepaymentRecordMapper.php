<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Infrastructure\Mysql\Record\CustomerScheduledRepaymentRecord;
use Exception;

class CustomerScheduledRepaymentRecordMapper extends MapperBase
{
    /**
     * @param CustomerScheduledRepayment $model
     * @return CustomerScheduledRepaymentRecord
     */
    public function toRecord($model): CustomerScheduledRepaymentRecord
    {
        return (new CustomerScheduledRepaymentRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setLoanAccountId($model->getLoanAccountId())
            ->setDueAmount($model->getDueAmount())
            ->setCurrency($model->getCurrency())
            ->setDueAt($model->getDueAt())
            ->setStatus($model->getStatus())
            ->setVersion($model->getVersion())
            ->setRepaidType($model->getRepaidType())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());
    }

    /**
     * @param CustomerScheduledRepaymentRecord $record
     * @return CustomerScheduledRepayment
     * @throws Exception
     */
    public function toModel($record): CustomerScheduledRepayment
    {
        return (new CustomerScheduledRepayment())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setLoanAccountId($record->getLoanAccountId())
            ->setDueAmount($record->getDueAmount())
            ->setCurrency($record->getCurrency())
            ->setDueAt($record->getDueAtAsDateTime())
            ->setStatus($record->getStatus())
            ->setVersion($record->getVersion())
            ->setRepaidType($record->getRepaidType())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime());
    }
}