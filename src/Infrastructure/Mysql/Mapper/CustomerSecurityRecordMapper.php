<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\CustomerSecurity;
use App\Infrastructure\Mysql\Record\CustomerSecurityRecord;

class CustomerSecurityRecordMapper extends MapperBase
{
    /**
     * @param CustomerSecurity $model
     * @return CustomerSecurityRecord
     */
    public function toRecord($model): CustomerSecurityRecord
    {
        return (new CustomerSecurityRecord())
            ->setId($model->getId())
            ->setUsername($model->getUsername())
            ->setPassword($model->getPassword())
            ->setAccountToken($model->getAccountToken());
    }

    /**
     * @param CustomerSecurityRecord $record
     * @return CustomerSecurity
     */
    public function toModel($record): CustomerSecurity
    {
        return (new CustomerSecurity())
            ->setId($record->getId())
            ->setUsername($record->getUsername())
            ->setPassword($record->getPassword())
            ->setAccountToken($record->getAccountToken());
    }
}