<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\Customer;
use App\Infrastructure\Mysql\Record\CustomerRecord;
use Exception;

class CustomerRecordMapper extends MapperBase
{
    /**
     * @param Customer $model
     * @return CustomerRecord
     * @throws Exception
     */
    public function toRecord($model): CustomerRecord
    {
//        $createdAt = $model->getCreatedAt() ?? DateTimeUtils::now();
//        $updatedAt = $model->getUpdatedAt() ?? DateTimeUtils::now();
        return (new CustomerRecord())
            ->setId($model->getId())
            ->setCustomerName($model->getCustomerName())
            ->setCustomerPhone($model->getCustomerPhone())
            ->setCustomerEmail($model->getCustomerEmail())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());
    }

    /**
     * @param CustomerRecord $record
     * @return Customer
     * @throws Exception
     */
    public function toModel($record): Customer
    {
        return (new Customer())
            ->setId($record->getId())
            ->setCustomerName($record->getCustomerName())
            ->setCustomerPhone($record->getCustomerPhone())
            ->setCustomerEmail($record->getCustomerEmail())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime());
    }
}