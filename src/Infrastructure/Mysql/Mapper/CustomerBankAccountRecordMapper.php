<?php

namespace App\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\CustomerBankAccount;
use App\Infrastructure\Mysql\Record\CustomerBankAccountRecord;
use Exception;

class CustomerBankAccountRecordMapper extends MapperBase
{

    /**
     * @param CustomerBankAccount $model
     * @return CustomerBankAccountRecord
     */
    public function toRecord($model): CustomerBankAccountRecord
    {
        return (new CustomerBankAccountRecord())
            ->setCustomerId($model->getCustomerId())
            ->setId($model->getId())
            ->setBalance($model->getBalance())
            ->setCurrency($model->getCurrency())
            ->setIsPrimary($model->getIsPrimary())
            ->setVersion($model->getVersion())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());
    }

    /**
     * @param CustomerBankAccountRecord $record
     * @return CustomerBankAccount
     * @throws Exception
     */
    public function toModel($record): CustomerBankAccount
    {
        return (new CustomerBankAccount())
            ->setCustomerId($record->getCustomerId())
            ->setId($record->getId())
            ->setBalance($record->getBalance())
            ->setCurrency($record->getCurrency())
            ->setIsPrimary($record->getIsPrimary())
            ->setVersion($record->getVersion())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime());
    }
}