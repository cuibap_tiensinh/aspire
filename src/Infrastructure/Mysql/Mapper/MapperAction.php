<?php

namespace App\Infrastructure\Mysql\Mapper;

interface MapperAction
{
    public function toRecord($model);
    public function toModel($record);
}