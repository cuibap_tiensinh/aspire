<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\Repositories\CustomerBankAccountRepository;
use App\Infrastructure\Mysql\Mapper\CustomerBankAccountRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerBankAccountRecord;
use http\Message\Body;
use Phalcon\Db\Column;

class MysqlCustomerBankAccountRepository extends MysqlRepositoryBase implements CustomerBankAccountRepository
{
    use ModelFilterTrait;

    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerBankAccountRecordMapper();
    }

    protected function getMapper(): CustomerBankAccountRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerBankAccountRecord::class;
    }

    /**
     * @param CustomerBankAccount $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(CustomerBankAccount $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param CustomerBankAccount $model
     * @return bool
     * @throws BusinessException
     */
    public function update(CustomerBankAccount $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return CustomerBankAccount|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerBankAccount
    {
        return $this->fetchOneBase($id, $forUpdate);
    }

    /**
     * @param string $customerId
     * @param bool $forUpdate
     * @return CustomerBankAccount|null
     * @throws BusinessException
     */
    public function fetchOneByCustomer(string $customerId, bool $forUpdate = false): ?CustomerBankAccount
    {
        try {
            $record = CustomerBankAccountRecord::findFirst([
                'conditions' => 'customer_id = :customer_id:',
                'bind' => ['customer_id' => $customerId],
                'bindTypes' => [
                    'customer_id' => Column::BIND_PARAM_STR,
                ],
                'for_update' => $forUpdate,
            ]);

            return $record != null ? $this->mapper->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $id
     * @param string $customerId
     * @param bool $forUpdate
     * @return CustomerBankAccount|null
     * @throws BusinessException
     */
    public function fetchOneAndCustomer(string $id, string $customerId, bool $forUpdate = false): ?CustomerBankAccount
    {
        try {
            $record = CustomerBankAccountRecord::findFirst([
                'conditions' => 'id = :id: AND customer_id = :customer_id:',
                'bind' => ['id' => $id, 'customer_id' => $customerId],
                'bindTypes' => [
                    'id' => Column::BIND_PARAM_STR,
                    'customer_id' => Column::BIND_PARAM_STR,
                ],
                'for_update' => $forUpdate,
            ]);

            return $record != null ? $this->mapper->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @return bool
     * @throws BusinessException
     */
    public function isExistByCustomerId(string $customerId): bool
    {
        try {
            $rowcount = CustomerBankAccountRecord::count([
                'conditions' => 'customer_id = :id:',
                'bind' => ['id' => $customerId],
                'bindTypes' => ['id' => Column::BIND_PARAM_STR]
            ]);

            return $rowcount > 0;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param array $filter
     * @return int
     * @throws BusinessException
     */
    public function countByFilter(array $filter): int
    {
        try {
            list($conditions, $bind, $bindTypes) = $this->buildModelQuery($filter);

            return CustomerBankAccountRecord::count([
                'conditions' => $conditions,
                'bind' => $bind,
                'bindTypes' => $bindTypes
            ]);
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param array $filter
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws BusinessException
     */
    public function fetchAllByFilter(array $filter, int $limit, int $offset): array
    {
        try {
            list($conditions, $bind, $bindTypes) = $this->buildModelQuery($filter);

            $records = CustomerBankAccountRecord::find([
                'conditions' => $conditions,
                'bind' => $bind,
                'bindTypes' => $bindTypes,
                'limit' => $limit,
                'offset' => $offset
            ]);

            $models = [];
            foreach ($records as $record) {
                $models[] = $this->mapper->toModel($record);
            }

            return $models;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}