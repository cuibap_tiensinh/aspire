<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Repositories\CustomerLoanRepository;
use App\Core\Domain\ValueObjects\LoanStatus;
use App\Infrastructure\Mysql\Mapper\CustomerLoanRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerLoanRecord;
use Phalcon\Db\Column;

class MysqlCustomerLoanRepository extends MysqlRepositoryBase implements CustomerLoanRepository
{
    use ModelFilterTrait;

    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerLoanRecordMapper();
    }

    protected function getMapper(): CustomerLoanRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerLoanRecord::class;
    }

    /**
     * @param CustomerLoan $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(CustomerLoan $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param CustomerLoan $model
     * @return bool
     * @throws BusinessException
     */
    public function update(CustomerLoan $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return CustomerLoan|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerLoan
    {
        return $this->fetchOneBase($id, $forUpdate);
    }

    /**
     * @param string $id
     * @param string $customerId
     * @return CustomerLoan|null
     * @throws BusinessException
     */
    public function fetchOneAndCustomer(string $id, string $customerId): ?CustomerLoan
    {
        try {
            $record = CustomerLoanRecord::findFirst([
                'conditions' => 'id = :id: AND customer_id = :customer_id:',
                'bind' => ['id' => $id, 'customer_id' => $customerId],
                'bindTypes' => [
                    'id' => Column::BIND_PARAM_STR,
                    'customer_id' => Column::BIND_PARAM_STR,
                ]
            ]);

            return $record != null ? $this->mapper->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @return array
     * @throws BusinessException
     */
    public function fetchAllByCustomer(string $customerId): array
    {
        try {
            $records = CustomerLoanRecord::find([
                'conditions' => 'customer_id = :customer_id:',
                'bind' => ['customer_id' => $customerId],
                'bindTypes' => [
                    'customer_id' => Column::BIND_PARAM_STR,
                ]
            ]);

            $models = [];
            foreach ($records as $record) {
                $models[] = $this->mapper->toModel($record);
            }

            return $models;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @return bool
     * @throws BusinessException
     */
    public function isExistedPendingLoanByCustomer(string $customerId): bool
    {
        try {
            $rowcount = CustomerLoanRecord::count([
                'conditions' => 'customer_id = :customer_id: AND status = :status:',
                'bind' => [
                    'customer_id' => $customerId,
                    'status' => LoanStatus::PENDING
                ],
                'bindTypes' => [
                    'customer_id' => Column::BIND_PARAM_STR,
                    'status' => Column::BIND_PARAM_STR,
                ]
            ]);
            return $rowcount > 0;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param array $filter
     * @return int
     * @throws BusinessException
     */
    public function countByFilter(array $filter): int
    {
        try {
            list($conditions, $bind, $bindTypes) = $this->buildModelQuery($filter);

            return CustomerLoanRecord::count([
                'conditions' => $conditions,
                'bind' => $bind,
                'bindTypes' => $bindTypes
            ]);
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param array $filter
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws BusinessException
     */
    public function fetchAllByFilter(array $filter, int $limit, int $offset): array
    {
        try {
            list($conditions, $bind, $bindTypes) = $this->buildModelQuery($filter);

            $records = CustomerLoanRecord::find([
                'conditions' => $conditions,
                'bind' => $bind,
                'bindTypes' => $bindTypes,
                'limit' => $limit,
                'offset' => $offset
            ]);

            $models = [];
            foreach ($records as $record) {
                $models[] = $this->mapper->toModel($record);
            }

            return $models;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $scheduledAt
     * @param int $limit
     * @param int $offset
     * @return CustomerLoan[]
     * @throws BusinessException
     */
    public function fetchAllForScheduledRepayment(string $scheduledAt, int $limit, int $offset = 0): array
    {
        try {
            $records = CustomerLoanRecord::find([
                'conditions' => 'status = "APPROVED" AND scheduled_at <= :scheduled_at:',
                'bind' => ['scheduled_at' => $scheduledAt],
                'bindTypes' => ['scheduled_at' => Column::BIND_PARAM_STR],
                'limit' => $limit
            ]);

            $models = [];
            foreach ($records as $record) {
                $models[] = $this->mapper->toModel($record);
            }

            return $models;

        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException($exception->getMessage(), ErrorCode::UNKNOWN_ERROR);
        }
    }
}