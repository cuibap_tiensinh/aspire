<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use DateTime;
use Exception;

/**
 * Class CustomerRecord
 *
 * @package App\Infrastructure\Mysql\Record;
 * @author thang.dang <dangquocthang0101@gmail.com>
 *
 */
class CustomerRecord extends RecordBase
{
    private const TABLE_NAME = "customer";

    private $id;
    private $customer_name;
    private $customer_phone;
    private $customer_email;
    private $created_at;
    private $updated_at;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->created_at)) {
            $this->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
    }

    public function beforeUpdate()
    {
        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
    }

    public function afterFetch()
    {
        $this->created_at = $this->addTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->addTimezoneToDateTimeString($this->updated_at);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerRecord
     */
    public function setId(string $id): CustomerRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName(): string
    {
        return $this->customer_name;
    }

    /**
     * @param string $customer_name
     * @return CustomerRecord
     */
    public function setCustomerName(string $customer_name): CustomerRecord
    {
        $this->customer_name = $customer_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerPhone(): string
    {
        return $this->customer_phone;
    }

    /**
     * @param string $customer_phone
     * @return CustomerRecord
     */
    public function setCustomerPhone(string $customer_phone): CustomerRecord
    {
        $this->customer_phone = $customer_phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail(): string
    {
        return $this->customer_email;
    }

    /**
     * @param string $customer_email
     * @return CustomerRecord
     */
    public function setCustomerEmail(string $customer_email): CustomerRecord
    {
        $this->customer_email = $customer_email;
        return $this;
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function getCreatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->created_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->created_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerRecord
     */
    public function setCreatedAt(?DateTime $created_at): CustomerRecord
    {
        if ($created_at != null)
            $this->created_at = DateTimeUtils::convertToDefaultTimeZoneAsString($created_at);
        else $this->created_at = null;
        return $this;
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function getUpdatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->updated_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->updated_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return (string)$this->updated_at;
    }

    /**
     * @param DateTime|null $updated_at
     * @return CustomerRecord
     */
    public function setUpdatedAt(?DateTime $updated_at): CustomerRecord
    {
        if ($updated_at != null)
            $this->updated_at = DateTimeUtils::convertToDefaultTimeZoneAsString($updated_at);
        else $this->updated_at = null;
        return $this;
    }
}
