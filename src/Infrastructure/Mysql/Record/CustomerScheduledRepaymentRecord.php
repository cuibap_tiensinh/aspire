<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerRepaymentHistory;
use DateTime;
use Exception;

/**
 * Class CustomerScheduledRepaymentRecord
 *
 * @package App\Infrastructure\Mysql\Record;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerScheduledRepaymentRecord extends RecordBase
{
    private const TABLE_NAME = "customer_scheduled_repayment";

    private $id;
    // The Id of the customer. This field identifies who do repayment
    private $customer_id;
    // This field identifies which the loan the customer repays for
    private $loan_account_id;
    private $due_amount;
    private $currency;
    private $due_at;
    private $status;
    private $version;
    private $repaid_type;
    private $created_at;
    private $updated_at;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->created_at)) {
            $this->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->due_at = $this->removeTimezoneToDateTimeString($this->due_at);
        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
    }

    public function beforeUpdate()
    {
        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->due_at = $this->removeTimezoneToDateTimeString($this->due_at);
        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
    }

    public function afterFetch()
    {
        $this->due_at = $this->addTimezoneToDateTimeString($this->due_at);
        $this->created_at = $this->addTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->addTimezoneToDateTimeString($this->updated_at);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerScheduledRepaymentRecord
     */
    public function setId(string $id): CustomerScheduledRepaymentRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerScheduledRepaymentRecord
     */
    public function setCustomerId(string $customer_id): CustomerScheduledRepaymentRecord
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loan_account_id;
    }

    /**
     * @param string $loan_account_id
     * @return CustomerScheduledRepaymentRecord
     */
    public function setLoanAccountId(string $loan_account_id): CustomerScheduledRepaymentRecord
    {
        $this->loan_account_id = $loan_account_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDueAmount()
    {
        return $this->due_amount;
    }

    /**
     * @param mixed $due_amount
     * @return CustomerScheduledRepaymentRecord
     */
    public function setDueAmount($due_amount): CustomerScheduledRepaymentRecord
    {
        $this->due_amount = $due_amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerScheduledRepaymentRecord
     */
    public function setCurrency(string $currency): CustomerScheduledRepaymentRecord
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getDueAtAsDateTime(): ?DateTime
    {
        if (!empty($this->due_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->due_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getDueAt(): string
    {
        return (string)$this->due_at;
    }

    /**
     * @param Datetime|null $due_at
     * @return CustomerScheduledRepaymentRecord
     */
    public function setDueAt(?DateTime $due_at): CustomerScheduledRepaymentRecord
    {
        if ($due_at != null)
            $this->due_at = DateTimeUtils::convertToDefaultTimeZoneAsString($due_at);
        else $this->due_at = null;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return CustomerScheduledRepaymentRecord
     */
    public function setStatus(string $status): CustomerScheduledRepaymentRecord
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return CustomerScheduledRepaymentRecord
     */
    public function setVersion($version): CustomerScheduledRepaymentRecord
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepaidType(): string
    {
        return (string)$this->repaid_type;
    }

    /**
     * @param string $repaid_type
     * @return CustomerScheduledRepaymentRecord
     */
    public function setRepaidType(string $repaid_type): CustomerScheduledRepaymentRecord
    {
        $this->repaid_type = $repaid_type;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getCreatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->created_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->created_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->created_at;
    }

    /**
     * @param Datetime|null $created_at
     * @return CustomerScheduledRepaymentRecord
     */
    public function setCreatedAt(?DateTime $created_at): CustomerScheduledRepaymentRecord
    {
        if ($created_at != null)
            $this->created_at = DateTimeUtils::convertToDefaultTimeZoneAsString($created_at);
        else $this->created_at = null;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getUpdatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->updated_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->updated_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return (string)$this->updated_at;
    }

    /**
     * @param Datetime|null $updated_at
     * @return CustomerScheduledRepaymentRecord
     */
    public function setUpdatedAt(?DateTime $updated_at): CustomerScheduledRepaymentRecord
    {
        if ($updated_at != null)
            $this->updated_at = DateTimeUtils::convertToDefaultTimeZoneAsString($updated_at);
        else $this->updated_at = null;
        return $this;
    }
}