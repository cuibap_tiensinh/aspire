<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use DateTime;
use Exception;

/**
 * Class CustomerRepaymentHistoryRecord
 *
 * @package App\Infrastructure\Mysql\Record;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerRepaymentHistoryRecord extends RecordBase
{
    private const TABLE_NAME = "customer_repayment_history";

    private $id;
    // The Id of the customer. This field identifies who do repayment
    private $customer_id;
    private $bank_account_id;
    private $loan_account_id;
    private $repaid_amount;
    private $currency;
    private $remaining_due_amount_in_period;
    private $version;
    private $repaid_type;
    private $created_at;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->created_at)) {
            $this->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
    }

    public function beforeUpdate()
    {
        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
    }

    public function afterFetch()
    {
        $this->created_at = $this->addTimezoneToDateTimeString($this->created_at);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerRepaymentHistoryRecord
     */
    public function setId(string $id): CustomerRepaymentHistoryRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerRepaymentHistoryRecord
     */
    public function setCustomerId(string $customer_id): CustomerRepaymentHistoryRecord
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bank_account_id;
    }

    /**
     * @param string $bank_account_id
     * @return CustomerRepaymentHistoryRecord
     */
    public function setBankAccountId(string $bank_account_id): CustomerRepaymentHistoryRecord
    {
        $this->bank_account_id = $bank_account_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loan_account_id;
    }

    /**
     * @param string $loan_account_id
     * @return CustomerRepaymentHistoryRecord
     */
    public function setLoanAccountId(string $loan_account_id): CustomerRepaymentHistoryRecord
    {
        $this->loan_account_id = $loan_account_id;
        return $this;
    }

    /**
     * @return float
     */
    public function getRepaidAmount(): float
    {
        return $this->repaid_amount;
    }

    /**
     * @param float $repaid_amount
     * @return CustomerRepaymentHistoryRecord
     */
    public function setRepaidAmount(float $repaid_amount): CustomerRepaymentHistoryRecord
    {
        $this->repaid_amount = $repaid_amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerRepaymentHistoryRecord
     */
    public function setCurrency(string $currency): CustomerRepaymentHistoryRecord
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getRemainingDueAmountInperiod(): float
    {
        return $this->remaining_due_amount_in_period;
    }

    /**
     * @param float $remaining_due_amount_in_period
     * @return CustomerRepaymentHistoryRecord
     */
    public function setRemainingDueAmountInperiod(float $remaining_due_amount_in_period): CustomerRepaymentHistoryRecord
    {
        $this->remaining_due_amount_in_period = $remaining_due_amount_in_period;
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return CustomerRepaymentHistoryRecord
     */
    public function setVersion(int $version): CustomerRepaymentHistoryRecord
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepaidType(): string
    {
        return $this->repaid_type;
    }

    /**
     * @param string $repaid_type
     * @return CustomerRepaymentHistoryRecord
     */
    public function setRepaidType(string $repaid_type): CustomerRepaymentHistoryRecord
    {
        $this->repaid_type = $repaid_type;
        return $this;
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function getCreatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->created_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->created_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerRepaymentHistoryRecord
     */
    public function setCreatedAt(?DateTime $created_at): CustomerRepaymentHistoryRecord
    {
        if ($created_at != null)
            $this->created_at = DateTimeUtils::convertToDefaultTimeZoneAsString($created_at);
        else $this->created_at = null;
        return $this;
    }
}