<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use DateTime;
use Exception;

/**
 * Class CustomerLoanRecord
 *
 * @package App\Infrastructure\Mysql\Record;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerLoanRecord extends RecordBase
{
    private const TABLE_NAME = "customer_loan";

    // The Id of the customer's bank loan account.
    // This account is created when the customer submit a loan request.
    private $id;
    // The Id of the customer. This field identifies who is the owner of this loan
    private $customer_id;
    private $amount;
    private $currency;
    private $loan_term;
    private $status;
    private $approved_by;
    private $started_at;
    private $ended_at;
    private $scheduled_at;
    private $repaid_count;
    private $repaid_amount;
    private $created_at;
    private $updated_at;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->created_at)) {
            $this->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
        $this->started_at = $this->removeTimezoneToDateTimeString($this->started_at);
        $this->ended_at = $this->removeTimezoneToDateTimeString($this->ended_at);
        $this->scheduled_at = $this->removeTimezoneToDateTimeString($this->scheduled_at);
    }

    public function beforeUpdate()
    {
        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
        $this->started_at = $this->removeTimezoneToDateTimeString($this->started_at);
        $this->ended_at = $this->removeTimezoneToDateTimeString($this->ended_at);
        $this->scheduled_at = $this->removeTimezoneToDateTimeString($this->scheduled_at);
    }

    public function afterFetch()
    {
        $this->repaid_count = intval($this->repaid_count);
        $this->repaid_amount = floatval($this->repaid_amount);
        $this->created_at = $this->addTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->addTimezoneToDateTimeString($this->updated_at);
        $this->started_at = $this->addTimezoneToDateTimeString($this->started_at);
        $this->ended_at = $this->addTimezoneToDateTimeString($this->ended_at);
        $this->scheduled_at = $this->addTimezoneToDateTimeString($this->scheduled_at);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerLoanRecord
     */
    public function setId(string $id): CustomerLoanRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerLoanRecord
     */
    public function setCustomerId(string $customer_id): CustomerLoanRecord
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return CustomerLoanRecord
     */
    public function setAmount(float $amount): CustomerLoanRecord
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerLoanRecord
     */
    public function setCurrency(string $currency): CustomerLoanRecord
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoanTerm(): int
    {
        return $this->loan_term;
    }

    /**
     * @param int $loan_term
     * @return CustomerLoanRecord
     */
    public function setLoanTerm(int $loan_term): CustomerLoanRecord
    {
        $this->loan_term = $loan_term;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return CustomerLoanRecord
     */
    public function setStatus(string $status): CustomerLoanRecord
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getApprovedBy(): string
    {
        return $this->approved_by;
    }

    /**
     * @param string $approved_by
     * @return CustomerLoanRecord
     */
    public function setApprovedBy(string $approved_by): CustomerLoanRecord
    {
        $this->approved_by = $approved_by;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getCreatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->created_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->created_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerLoanRecord
     */
    public function setCreatedAt(?DateTime $created_at): CustomerLoanRecord
    {
        if ($created_at != null)
            $this->created_at = DateTimeUtils::convertToDefaultTimeZoneAsString($created_at);
        else $this->created_at = null;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getUpdatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->updated_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->updated_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return (string)$this->updated_at;
    }

    /**
     * @param DateTime|null $updated_at
     * @return CustomerLoanRecord
     */
    public function setUpdatedAt(?DateTime $updated_at): CustomerLoanRecord
    {
        if ($updated_at != null)
            $this->updated_at = DateTimeUtils::convertToDefaultTimeZoneAsString($updated_at);
        else $this->updated_at = null;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getStartedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->started_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->started_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getStartedAt(): string
    {
        return (string)$this->started_at;
    }

    /**
     * @param DateTime|null $started_at
     * @return CustomerLoanRecord
     */
    public function setStartedAt(?DateTime $started_at): CustomerLoanRecord
    {
        if ($started_at != null)
            $this->started_at = DateTimeUtils::convertToDefaultTimeZoneAsString($started_at);
        else $this->started_at = null;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getEndedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->ended_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->ended_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getEndedAt(): string
    {
        return (string)$this->ended_at;
    }

    /**
     * @param DateTime|null $ended_at
     * @return CustomerLoanRecord
     */
    public function setEndedAt(?DateTime $ended_at): CustomerLoanRecord
    {
        if ($ended_at != null)
            $this->ended_at = DateTimeUtils::convertToDefaultTimeZoneAsString($ended_at);
        else $this->ended_at = null;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getScheduledAtAsDateTime(): ?DateTime
    {
        if (!empty($this->scheduled_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->scheduled_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getScheduledAt(): string
    {
        return (string)$this->scheduled_at;
    }

    /**
     * @param DateTime|null $scheduled_at
     * @return CustomerLoanRecord
     */
    public function setScheduledAt(?DateTime $scheduled_at): CustomerLoanRecord
    {
        if ($scheduled_at != null)
            $this->scheduled_at = DateTimeUtils::convertToDefaultTimeZoneAsString($scheduled_at);
        else $this->scheduled_at = null;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepaidCount()
    {
        return $this->repaid_count;
    }

    /**
     * @param mixed $repaid_count
     * @return CustomerLoanRecord
     */
    public function setRepaidCount($repaid_count): CustomerLoanRecord
    {
        $this->repaid_count = $repaid_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepaidAmount()
    {
        return $this->repaid_amount;
    }

    /**
     * @param mixed $repaid_amount
     * @return CustomerLoanRecord
     */
    public function setRepaidAmount($repaid_amount): CustomerLoanRecord
    {
        $this->repaid_amount = $repaid_amount;
        return $this;
    }
}