<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use DateTime;
use Exception;
use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

abstract class RecordBase extends Model
{
    public function init() {}

    public function initialize()
    {
        $this->useDynamicUpdate(true);
        $this->init();
    }

    public function addTimezoneToDateTimeString(?string $dateTime): ?string
    {
        if (empty($dateTime)) return $dateTime;
        if (strpos($dateTime, DateTimeUtils::DEFAULT_TIMEZONE_STRING) === false)
            return $dateTime . " " . DateTimeUtils::DEFAULT_TIMEZONE_STRING;
        return $dateTime;
    }

    public function removeTimezoneToDateTimeString(?string $dateTime): ?string
    {
        if (empty($dateTime)) return $dateTime;
        if (strpos($dateTime, DateTimeUtils::DEFAULT_TIMEZONE_STRING) !== false)
            return trim(str_replace(DateTimeUtils::DEFAULT_TIMEZONE_STRING, "", $dateTime));
        return $dateTime;
    }

    public static function getTableName(): string
    {
        return "";
    }

    public static function insertBatch(array $records)
    {
        $db = \Phalcon\DI::getDefault()->getShared('db');
        $insertCommand = self::buildInsertBulkCommand($records);
        return $db->execute($insertCommand);
    }

    private static function buildInsertBulkCommand(array $records): string
    {
        $columns = sprintf('`%s`', implode('`,`', array_keys($records[0])));

        $str = '';
        foreach ($records as $record) {
            foreach ($record as $key => &$val) {
                if (is_null($val)) {
                    $val = 'NULL';
                    continue;
                }
                elseif (in_array($key, ['due_at', 'created_at', 'updated_at', 'scheduled_at', 'started_at', 'ended_at'])) {
                    $val = "'" . substr($val, 0, 19) . "'";
                }
                elseif (is_string($val)) {
                    $val = "'".$val."'";
                }
                elseif ($val instanceof RawValue) {
                    $val = $val->getValue();
                }
            }

            $str .= sprintf('(%s),', implode(',', $record));
        }
        $str = rtrim($str, ',');

        return sprintf("INSERT INTO `%s` (%s) VALUES %s",
            static::getTableName(),
            $columns,
            $str
        );
    }

    public function getMessageArray()
    {
        $messages = array();
        foreach ($this->getMessages() as $message) {
            $messages[] = $message->getMessage();
        }
        return $messages;
    }

    public function getMessage($sep = ' ')
    {
        $messages = $this->getMessageArray();
        return join($sep, $messages);
    }
}