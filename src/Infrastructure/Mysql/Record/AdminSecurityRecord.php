<?php

namespace App\Infrastructure\Mysql\Record;

/**
 * Class AdminSecurityRecord
 *
 * @package App\Infrastructure\Mysql\Record
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class AdminSecurityRecord extends RecordBase
{
    private const TABLE_NAME = "admin_security";

    private $id;
    private $username;
    private $password;
    private $account_token;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->account_token)) {
            $this->account_token = md5($this->username . "|" . $this->password);
        }
    }

    public function beforeUpdate()
    {
        if (empty($this->account_token)) {
            $this->account_token = md5($this->username . "|" . $this->password);
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return AdminSecurityRecord
     */
    public function setId(string $id): AdminSecurityRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return AdminSecurityRecord
     */
    public function setUsername(string $username): AdminSecurityRecord
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return AdminSecurityRecord
     */
    public function setPassword(string $password): AdminSecurityRecord
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountToken(): string
    {
        return $this->account_token;
    }

    /**
     * @param string $account_token
     * @return AdminSecurityRecord
     */
    public function setAccountToken(string $account_token): AdminSecurityRecord
    {
        $this->account_token = $account_token;
        return $this;
    }
}