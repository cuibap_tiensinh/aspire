<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use DateTime;
use Exception;
use Phalcon\Db\RawValue;

/**
 * Class CustomerBankAccountRecord
 *
 * @package App\Infrastructure\Mysql\Record;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerBankAccountRecord extends RecordBase
{
    private const TABLE_NAME = "customer_bank_account";

    // The Id of the customer's bank account.
    // The customer can deposit money, transfer money, and so on.
    private $id;
    // The Id of the customer. This field identifies who is the owner of this bank
    private $customer_id;
    private $balance;
    private $currency;
    private $version;
    // Only the primary bank account can do loan repayment
    private $is_primary;
    private $created_at;
    private $updated_at;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->created_at)) {
            $this->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
    }

    public function beforeUpdate()
    {
        $this->version = new RawValue("version + 1");
        $this->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->removeTimezoneToDateTimeString($this->updated_at);
    }

    public function afterFetch()
    {
        $this->balance = floatval($this->balance);
        $this->version = intval($this->version);
        $this->created_at = $this->addTimezoneToDateTimeString($this->created_at);
        $this->updated_at = $this->addTimezoneToDateTimeString($this->updated_at);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerBankAccountRecord
     */
    public function setId(string $id): CustomerBankAccountRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerBankAccountRecord
     */
    public function setCustomerId(string $customer_id): CustomerBankAccountRecord
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     * @return CustomerBankAccountRecord
     */
    public function setBalance($balance): CustomerBankAccountRecord
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerBankAccountRecord
     */
    public function setCurrency(string $currency): CustomerBankAccountRecord
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return CustomerBankAccountRecord
     */
    public function setVersion($version): CustomerBankAccountRecord
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsPrimary(): bool
    {
        return (bool)$this->is_primary;
    }

    /**
     * @param bool $is_primary
     * @return CustomerBankAccountRecord
     */
    public function setIsPrimary(bool $is_primary): CustomerBankAccountRecord
    {
        $this->is_primary = $is_primary ? 1 : 0;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getCreatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->created_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->created_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerBankAccountRecord
     */
    public function setCreatedAt(?DateTime $created_at): CustomerBankAccountRecord
    {
        if ($created_at != null)
            $this->created_at = DateTimeUtils::convertToDefaultTimeZoneAsString($created_at);
        else $this->created_at = null;
        return $this;
    }

    /**
     * @return Datetime|null
     * @throws Exception
     */
    public function getUpdatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->updated_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->updated_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return (string)$this->updated_at;
    }

    /**
     * @param DateTime|null $updated_at
     * @return CustomerBankAccountRecord
     */
    public function setUpdatedAt(?DateTime $updated_at): CustomerBankAccountRecord
    {
        if ($updated_at != null)
            $this->updated_at = DateTimeUtils::convertToDefaultTimeZoneAsString($updated_at);
        else $this->updated_at = null;
        return $this;
    }
}