<?php

namespace App\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use DateTime;
use Exception;
use Phalcon\Db\RawValue;

/**
 * Class CustomerBalanceHistoryRecord
 *
 * @package App\Infrastructure\Mysql\Record;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerBalanceHistoryRecord extends RecordBase
{
    private const TABLE_NAME = "customer_balance_history";

    private $id;
    private $customer_id;
    private $bank_account_id;
    private $deposit_amount;
    private $withdraw_amount;
    private $account_currency;
    private $submitted_deposit_amount;
    private $submitted_withdraw_amount;
    private $submitted_currency;
    private $exchange_rate;
    private $reason;
    private $version;
    private $created_at;

    public static function getTableName(): string
    {
        return self::TABLE_NAME;
    }

    public function getSource(): string
    {
        return self::getTableName();
    }

    public function beforeCreate()
    {
        if (empty($this->created_at)) {
            $this->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
    }

    public function beforeUpdate()
    {
        $this->created_at = $this->removeTimezoneToDateTimeString($this->created_at);
    }

    public function afterFetch()
    {
        $this->created_at = $this->addTimezoneToDateTimeString($this->created_at);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerBalanceHistoryRecord
     */
    public function setId(string $id): CustomerBalanceHistoryRecord
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerBalanceHistoryRecord
     */
    public function setCustomerId(string $customer_id): CustomerBalanceHistoryRecord
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bank_account_id;
    }

    /**
     * @param string $bank_account_id
     * @return CustomerBalanceHistoryRecord
     */
    public function setBankAccountId(string $bank_account_id): CustomerBalanceHistoryRecord
    {
        $this->bank_account_id = $bank_account_id;
        return $this;
    }

    /**
     * @return float
     */
    public function getDepositAmount(): float
    {
        return $this->deposit_amount;
    }

    /**
     * @param float $deposit_amount
     * @return CustomerBalanceHistoryRecord
     */
    public function setDepositAmount(float $deposit_amount): CustomerBalanceHistoryRecord
    {
        $this->deposit_amount = $deposit_amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getWithdrawAmount(): float
    {
        return $this->withdraw_amount;
    }

    /**
     * @param float $withdraw_amount
     * @return CustomerBalanceHistoryRecord
     */
    public function setWithdrawAmount(float $withdraw_amount): CustomerBalanceHistoryRecord
    {
        $this->withdraw_amount = $withdraw_amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountCurrency(): string
    {
        return $this->account_currency;
    }

    /**
     * @param string $account_currency
     * @return CustomerBalanceHistoryRecord
     */
    public function setAccountCurrency(string $account_currency): CustomerBalanceHistoryRecord
    {
        $this->account_currency = $account_currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubmittedDepositAmount(): float
    {
        return $this->submitted_deposit_amount;
    }

    /**
     * @param float $submitted_deposit_amount
     * @return CustomerBalanceHistoryRecord
     */
    public function setSubmittedDepositAmount(float $submitted_deposit_amount): CustomerBalanceHistoryRecord
    {
        $this->submitted_deposit_amount = $submitted_deposit_amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubmittedWithdrawAmount(): float
    {
        return $this->submitted_withdraw_amount;
    }

    /**
     * @param float $submitted_withdraw_amount
     * @return CustomerBalanceHistoryRecord
     */
    public function setSubmittedWithdrawAmount(float $submitted_withdraw_amount): CustomerBalanceHistoryRecord
    {
        $this->submitted_withdraw_amount = $submitted_withdraw_amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubmittedCurrency(): string
    {
        return $this->submitted_currency;
    }

    /**
     * @param string $submitted_currency
     * @return CustomerBalanceHistoryRecord
     */
    public function setSubmittedCurrency(string $submitted_currency): CustomerBalanceHistoryRecord
    {
        $this->submitted_currency = $submitted_currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->exchange_rate;
    }

    /**
     * @param float $exchange_rate
     * @return CustomerBalanceHistoryRecord
     */
    public function setExchangeRate(float $exchange_rate): CustomerBalanceHistoryRecord
    {
        $this->exchange_rate = $exchange_rate;
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return CustomerBalanceHistoryRecord
     */
    public function setVersion(int $version): CustomerBalanceHistoryRecord
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return CustomerBalanceHistoryRecord
     */
    public function setReason(string $reason): CustomerBalanceHistoryRecord
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return Datetime
     * @throws Exception
     */
    public function getCreatedAtAsDateTime(): ?DateTime
    {
        if (!empty($this->created_at)) {
            $dateTime = new DateTime($this->addTimezoneToDateTimeString($this->created_at));
            return DateTimeUtils::convertToCurrentTimezone($dateTime);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerBalanceHistoryRecord
     */
    public function setCreatedAt(?DateTime $created_at): CustomerBalanceHistoryRecord
    {
        if ($created_at != null)
            $this->created_at = DateTimeUtils::convertToDefaultTimeZoneAsString($created_at);
        else $this->created_at = null;
        return $this;
    }
}