<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\AdminSecurity;
use App\Core\Domain\Repositories\AdminSecurityRepository;
use App\Infrastructure\Mysql\Mapper\AdminSecurityRecordMapper;
use App\Infrastructure\Mysql\Record\AdminSecurityRecord;
use Phalcon\Db\Column;

class MysqlAdminSecurityRepository extends MysqlRepositoryBase implements AdminSecurityRepository
{
    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new AdminSecurityRecordMapper();
    }

    protected function getMapper(): AdminSecurityRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return AdminSecurityRecord::class;
    }

    /**
     * @param AdminSecurity $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(AdminSecurity $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param AdminSecurity $model
     * @return bool
     * @throws BusinessException
     */
    public function update(AdminSecurity $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return AdminSecurity|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?AdminSecurity
    {
        return $this->fetchOneBase($id, $forUpdate);
    }

    /**
     * @param string $username
     * @return AdminSecurity|null
     * @throws BusinessException
     */
    public function fetchOneByUsername(string $username): ?AdminSecurity
    {
        try {
            $record = AdminSecurityRecord::findFirst([
                'conditions' => 'username = :username:',
                'bind' => [
                    'username' => $username,
                ],
                'bindTypes' => [
                    'username' => Column::BIND_PARAM_STR,
                ],
            ]);

            return $record != null ? $this->mapper->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}