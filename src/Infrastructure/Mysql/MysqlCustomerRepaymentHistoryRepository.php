<?php


namespace App\Infrastructure\Mysql;


use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerRepaymentHistory;
use App\Core\Domain\Repositories\CustomerRepaymentHistoryRepository;
use App\Infrastructure\Mysql\Mapper\CustomerRepaymentHistoryRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerRepaymentHistoryRecord;

class MysqlCustomerRepaymentHistoryRepository extends MysqlRepositoryBase implements CustomerRepaymentHistoryRepository
{
    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerRepaymentHistoryRecordMapper();
    }

    protected function getMapper(): CustomerRepaymentHistoryRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerRepaymentHistoryRecord::class;
    }

    /**
     * @param CustomerRepaymentHistory $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(CustomerRepaymentHistory $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param CustomerRepaymentHistory $model
     * @return bool
     * @throws BusinessException
     */
    public function update(CustomerRepaymentHistory $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return CustomerRepaymentHistory|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerRepaymentHistory
    {
        return $this->fetchOneBase($id, $forUpdate);
    }
}