<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\Repositories\CustomerScheduledRepaymentRepository;
use App\Core\Domain\ValueObjects\RepaymentStatus;
use App\Infrastructure\Mysql\Mapper\CustomerScheduledRepaymentRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerScheduledRepaymentRecord;
use Phalcon\Db\Column;

class MysqlCustomerScheduledRepaymentRepository extends MysqlRepositoryBase implements CustomerScheduledRepaymentRepository
{
    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerScheduledRepaymentRecordMapper();
    }

    protected function getMapper(): CustomerScheduledRepaymentRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerScheduledRepaymentRecord::class;
    }

    /**
     * @param CustomerScheduledRepayment $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(CustomerScheduledRepayment $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param CustomerScheduledRepayment $model
     * @return bool
     * @throws BusinessException
     */
    public function update(CustomerScheduledRepayment $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return CustomerScheduledRepayment|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerScheduledRepayment
    {
        return $this->fetchOneBase($id, $forUpdate);
    }

    /**
     * @param array $models
     * @return bool
     * @throws BusinessException
     */
    public function insertMany(array $models): bool
    {
        try {
            $records = [];
            foreach ($models as $model) {
                $record = $this->mapper->toRecord($model);
                $records[] = $record->toArray();
            }

            return (bool)CustomerScheduledRepaymentRecord::insertBatch($records);
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $loanAccountId
     * @param string $customerId
     * @param bool $forUpdate
     * @return CustomerScheduledRepayment|null
     * @throws BusinessException
     */
    public function fetchFirstUnpaidSchedule(string $loanAccountId, string $customerId, bool $forUpdate = false): ?CustomerScheduledRepayment
    {
        try {
            $record = CustomerScheduledRepaymentRecord::findFirst([
                'conditions' => 'loan_account_id = :id: AND customer_id = :customer_id: AND due_amount > 0 AND status <> :status:',
                'bind' => [
                    'id' => $loanAccountId,
                    'customer_id' => $customerId,
                    'status' => RepaymentStatus::PAID
                ],
                'bindTypes' => [
                    'id' => Column::BIND_PARAM_STR,
                    'customer_id' => Column::BIND_PARAM_STR,
                    'status' => Column::BIND_PARAM_STR,
                ],
                'order' => 'due_at',
                'for_update' => $forUpdate
            ]);

            return $record != null ? $this->mapper->toModel($record) : null;
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}