<?php

namespace App\Infrastructure\Mysql;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerBalanceHistory;
use App\Core\Domain\Repositories\CustomerBalanceHistoryRepository;
use App\Infrastructure\Mysql\Mapper\CustomerBalanceHistoryRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerBalanceHistoryRecord;

class MysqlCustomerBalanceHistoryRepository extends MysqlRepositoryBase implements CustomerBalanceHistoryRepository
{
    private $mapper;
    public function __construct()
    {
        parent::__construct();
        $this->mapper = new CustomerBalanceHistoryRecordMapper();
    }

    protected function getMapper(): CustomerBalanceHistoryRecordMapper
    {
        return $this->mapper;
    }

    protected function getRecordClass(): string
    {
        return CustomerBalanceHistoryRecord::class;
    }

    /**
     * @param CustomerBalanceHistory $model
     * @return bool
     * @throws BusinessException
     */
    public function insert(CustomerBalanceHistory $model): bool
    {
        return $this->insertBase($model);
    }

    /**
     * @param CustomerBalanceHistory $model
     * @return bool
     * @throws BusinessException
     */
    public function update(CustomerBalanceHistory $model): bool
    {
        return $this->updateBase($model);
    }

    /**
     * @param string $id
     * @param bool $forUpdate
     * @return CustomerBalanceHistory|null
     * @throws BusinessException
     */
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerBalanceHistory
    {
        return $this->fetchOneBase($id, $forUpdate);
    }
}