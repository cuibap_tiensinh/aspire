<?php

namespace App\Infrastructure\Mysql;

use Phalcon\Db\Column;

trait ModelFilterTrait
{
    private function buildModelQuery(array $filter): array
    {
        $conditions = [];
        $bind = [];
        $bindTypes = [];

        foreach ($filter as $key => $value) {
            $conditions[] = sprintf('%s = :%s:', $key, $key);
            $bind[$key] = $value;

            if (is_string($value)) $bindTypes[$key] = Column::BIND_PARAM_STR;
            elseif (is_numeric($value)) $bindTypes[$key] = Column::BIND_PARAM_INT;
            else $bindTypes[$key] = Column::BIND_SKIP;
        }

        return [
            join(" AND ", $conditions),
            $bind,
            $bindTypes
        ];
    }
}