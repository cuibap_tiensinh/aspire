<?php


namespace App\Common\Response;


class ErrorCode
{
    const SUCCESS = 0;
    const UNAUTHORIZED = -1;
    const ROUTE_NOT_FOUND = -2;
    const UNKNOWN_ERROR = -9999;
    const DATABASE_ERROR = -9998;

    const INVALID_DATA = 1000;

    // Customer: 1100 - 1199
    const CUSTOMER_NOT_FOUND = 1100;
    const CUSTOMER_PHONE_IS_EXIST = 1101;
    const CUSTOMER_PHONE_IS_NOT_EXIST = 1102;
    const CUSTOMER_PASSWORD_NOT_MATCH = 1103;

    // Customer Loan: 1200 - 1299
    const YOU_HAVE_UNAPPROVED_LOAN = 1200;
    const LOAN_NOT_FOUND = 1201;
    const LOAN_IS_APPROVED = 1202;
    const LOAN_IS_NOT_APPROVED = 1203;

    // Bank Account: 1300 - 1399
    const INVALID_DEPOSIT_DATA = 1300;
    const BANK_ACCOUNT_NOT_FOUND = 1301;
    const BANK_ACCOUNT_NOT_ENOUGH = 1302;

    // Schedule repayment: 1400 - 1499
    const SCHEDULED_REPAYMENT_NOT_EXIST = 1400;
    const YOU_REPAID_LOAN = 1401;
    const REPAID_AMOUNT_IS_LESS_THAN_DEBT = 1402;

}