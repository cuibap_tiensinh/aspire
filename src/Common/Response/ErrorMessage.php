<?php


namespace App\Common\Response;


class ErrorMessage
{
    const SUCCESS = "Success";
    const UNAUTHORIZED = "Unauthorized";
    const ROUTE_NOT_FOUND = "That route was not found on the server.";
    const INTERNAL_ERROR = "Something went wrong";
    const INVALID_REGISTER_ACCOUNT_DATA = "Request data is invalid. Please check and make sure that name, phone, email, and password field are not empty and valid value";
    const INVALID_LOGIN_DATA = "Username and Password are incorrect. Please check and try again";
    const INVALID_GET_LOAN_DETAIL_DATA = "Please choose one loan account and try again";
    const INVALID_REGISTER_LOAN_DATA = "Please check the amount, currency, and loan term. The amount must be greater than the minimum that belong to the currency and the loan term must be greater than 1 week and lesser 100 weeks";
    const INVALID_APPROVE_LOAN_DATA = "Please choose one loan account and try again";
    const INVALID_REPAY_LOAN_BY_TRANSFER_DATA = "Please choose one loan account, bank account, and valid amount to repay.";
    const INVALID_REPAY_LOAN_BY_CASH_IN_DATA = "Please choose one loan account, and valid amount to repay.";
    const CUSTOMER_NOT_FOUND = "The customer is not found. Please check and try again";
    const CUSTOMER_PHONE_IS_EXIST = "The customer phone is registered. Please check and try again";
    const CUSTOMER_PHONE_IS_NOT_EXIST = "The username and password is not correct. Please check and try again (L01)";
    const CUSTOMER_PASSWORD_NOT_MATCH = "The username and password is not correct. Please check and try again (L02)";
    const REGISTER_CUSTOMER_CUSTOMER = "The customer is not found. Please check and try again";
    const INVALID_DATA_UNAUTHORIZED = "The login session is expired. Please login and try again";
    const YOU_HAVE_UNAPPROVED_LOAN = "You have a loan being approved. Please wait for approval before applying for a new loan";
    const LOAN_NOT_FOUND = "The loan is not found. Please check and try again";
    const INVALID_DEPOSIT_DATA = "Please check the amount, currency, and bank account.";
    const BANK_ACCOUNT_NOT_FOUND = "The bank account is not found. Please check and try again";
    const LOAN_IS_APPROVED = "The loan is approved.";
    const LOAN_IS_NOT_APPROVED = "The loan is not approved. Please contact to our customer support for more detail";
    const BANK_ACCOUNT_NOT_ENOUGH = "Your balance is lesser than the amount that you submitted. Please check and try again.";
    const SCHEDULED_REPAYMENT_NOT_EXIST = "You are not due for repayment yet. Please come back later.";
    const YOU_REPAID_LOAN = "You have paid off this period's debt";
    const REPAID_AMOUNT_IS_LESS_THAN_DEBT = "The repaid amount must be equal to or greater than the debt amount";
}