<?php

namespace App\Common\Response;

class HTTPStatus
{
    // http status code
    // 2xx
    const HTTP_STATUS_SUCCESS = 200;
    const HTTP_STATUS_CREATED = 201;
    const HTTP_STATUS_NO_CONTENT = 204;
    // 4xx
    const HTTP_STATUS_BAD_REQUEST = 400;
    const HTTP_STATUS_UNAUTHORIZED = 401;
    const HTTP_STATUS_ACCESS_DENIED = 403;
    const HTTP_STATUS_NOT_FOUND = 404;
    const HTTP_STATUS_METHOD_NOT_ALLOW = 405;
    const HTTP_STATUS_CONFLICT = 409;
    //5xx
    const HTTP_STATUS_INTERNAL_ERROR = 500;
}