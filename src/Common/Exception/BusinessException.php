<?php

namespace App\Common\Exception;

class BusinessException extends \Exception {

    public $description;
    public $applicationCode;
    public $statusCode;

    /**
     * BusinessException constructor.
     * @param $message
     * @param int $code
     * @param array $errorArray
     * @param int $statusCode
     */
    public function __construct($message, $code = 0, array $errorArray = [], int $statusCode = 400) {
        parent::__construct($message, $code);

        $this->code = $code;
        $this->message = $message;
        $this->description = @$errorArray['description'];
        $this->applicationCode = @$errorArray['applicationCode'];
        $this->statusCode = $statusCode;
    }
}
