<?php

namespace App\Common\Utils;

class NumberUtils
{
    public static function truncate($number, $decimalLength = 4): float
    {
        $strNum = sprintf("%.{$decimalLength}f", $number);
        return floatval($strNum);
    }
}