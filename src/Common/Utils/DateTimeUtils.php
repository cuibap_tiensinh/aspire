<?php

namespace App\Common\Utils;

use DateTime;
use DateTimeZone;
use Exception;

class DateTimeUtils
{
    const DEFAULT_TIMEZONE_STRING = "GMT";
    const ASPIRE_DATETIME_FORMAT = "Y-m-d H:i:s T";
    const ASPIRE_DATETIME_WITHOUT_TIME_ZONE_FORMAT = "Y-m-d H:i:s";

    private static $currentTimezone;
    private static $defaultTimezone;

    public static function getCurrentTimezone(): DateTimeZone
    {
        if (!isset(self::$currentTimezone)) {
            self::$currentTimezone = new DateTimeZone(date_default_timezone_get());
        }
        return self::$currentTimezone;
    }

    public static function getDefaultTimezone(): DateTimeZone
    {
        if (!isset(self::$defaultTimezone)) {
            self::$defaultTimezone = new DateTimeZone(self::DEFAULT_TIMEZONE_STRING);
        }
        return self::$defaultTimezone;
    }

    public static function changeTimezone(?DateTime $dateTime, ?DateTimeZone $timezone): ?DateTime
    {
        if ($dateTime == null || $timezone == null) return $dateTime;

        $dateTime->setTimezone($timezone);
        return $dateTime;
    }

    public static function convertToDefaultTimezone(?DateTime $dateTime): ?DateTime
    {
        return self::changeTimezone($dateTime, self::getDefaultTimezone());
    }

    public static function convertToDefaultTimeZoneAsString(?DateTime $dateTime, bool $withTimezone = true): string
    {
        $dateTime = self::convertToDefaultTimezone($dateTime);
        $format = $withTimezone ? self::ASPIRE_DATETIME_FORMAT : self::ASPIRE_DATETIME_WITHOUT_TIME_ZONE_FORMAT;
        return $dateTime != null ? $dateTime->format($format) : "";
    }

    public static function convertToCurrentTimezone(?DateTime $dateTime): ?DateTime
    {
        return self::changeTimezone($dateTime, self::getCurrentTimezone());
    }

    public static function convertToCurrentTimezoneAsString(?DateTime $dateTime, bool $withTimezone = true): string
    {
        $dateTime = self::convertToCurrentTimezone($dateTime);
        $format = $withTimezone ? self::ASPIRE_DATETIME_FORMAT : self::ASPIRE_DATETIME_WITHOUT_TIME_ZONE_FORMAT;
        return $dateTime != null ? $dateTime->format($format) : "";
    }

    public static function nowWithoutMilliSec(DateTimeZone $timeZone = null): DateTime
    {
        $now = self::now($timeZone);
        $now->setTime($now->format('H'), $now->format('m'), $now->format('i'), 0);
        return $now;
    }

    public static function now(DateTimeZone $timeZone = null): DateTime
    {
        if ($timeZone == null) $timeZone = self::getCurrentTimezone();
        return new DateTime("now", $timeZone);
    }

    public static function today(DateTimeZone $timeZone = null): DateTime
    {
        if ($timeZone == null) $timeZone = self::getCurrentTimezone();
        return new DateTime("today", $timeZone);
    }
}