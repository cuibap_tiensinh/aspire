<?php

namespace App\Common\Utils\String;


class StringUtils
{
    public static function getSafeString($string): string
    {
        $string = self::trimMultipleSpaces(strip_tags($string));
        $invalidCharacters = array("$", "%", "#", "<", ">", "|");
        return str_replace($invalidCharacters, "", $string);
    }

    public static function isValidEmail($email): bool
    {
        if (empty($email)) return false;
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * The phone number must be formatted (+Country-code) XXXXXXXXX
     * XXXXXXXXX has at least 7 digits
     * @param $phone
     * @return bool
     */
    public static function isValidPhone($phone): bool
    {
        if (empty($phone)) return false;
        $phone = self::trimMultipleSpaces($phone);
        return (bool)preg_match('/^[(][+]{1}[0-9]{1,4}[)][\s]{0,1}[0-9]{7,}$/', $phone);
    }

    /**
     * The password has at least 8 characters
     *
     * @param $password
     * @return bool
     */
    public static function isValidPassword($password): bool
    {
        return strlen($password) >= 8;
    }

    public static function getGUID(): string
    {
        if (function_exists('com_create_guid') === true)
        {
            return strtolower(trim(com_create_guid(), '{}'));
        }

        return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
    }

    public static function trimMultipleSpaces($input)
    {
        return preg_replace('!\s+!', ' ', $input);
    }
}