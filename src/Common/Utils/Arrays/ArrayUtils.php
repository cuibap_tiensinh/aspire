<?php

namespace App\Common\Utils\Arrays;

use App\Common\Utils\String\StringUtils;

class ArrayUtils
{
    public static function stripTags(array $params): array
    {
        foreach ($params as $pkey => $param) {
            if (is_array($param)) {
                $params[$pkey] = self::stripTags($param);
            } elseif (is_object($param)) {
                $param = json_decode(json_encode($param), true);
                $params[$pkey] = self::stripTags($param);
            } else {
                $params[$pkey] = StringUtils::getSafeString($param);
            }
        }
        return $params;
    }

    public static function getOrDefault(?array $array, $key, $default = null)
    {
        if ($array === null) return $default;
        return $array[$key] ?? $default;
    }

    public static function getDeepValue(?array $array, $key = null, $default = null)
    {
        if ($array === null) {
            return $default;
        }

        $delimiter = '.';
        if (empty($key)) {
            return $array;
        }

        if (isset($array[$key])) {
            return $array[$key];
        }

        $value = $array;
        $segments = explode($delimiter, $key);
        foreach ($segments as $segment) {
            if (!is_array($value) || !isset($value[$segment])) {
                return $default;
            }

            $value = $value[$segment];
        }

        return $value;
    }
}