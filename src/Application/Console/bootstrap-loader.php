<?php

use Phalcon\Config;
use Phalcon\Loader;
use Phalcon\Logger;
use Phalcon\DI\FactoryDefault\CLI;
use Phalcon\CLI\Console;
use Phalcon\Cli\Dispatcher;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Events\Manager as EventsManager;

class ConsoleApplication
{
    protected $config;
    protected $di;

    public function __construct()
    {
        // Using the CLI factory default services container
        $this->di = new CLI();
    }

    protected function registerConfigs()
    {
        $this->config = new Config(include CONFIG_PATH . '/app.php');
    }

    protected function registerAutoloaders()
    {
        require ROOT_PATH . '/vendor/autoload.php';

        $loader = new Loader();
        $loader->registerNamespaces(array(
            'App' => APPLICATION_PATH,
        ))->register();
    }

    /**
     * This methods registers the services to be used by the application
     */
    protected function registerServices()
    {
        $config = $this->config;

        // Create a console application
        $console = new Console();
        $console->setDI($this->di);

        $this->di->setShared('console', $console);
        $this->di->setShared('config', $config);

        /**
         * Registering a logger
         */
        $this->di->set('logger', function() use ($config) {
            if (APPLICATION_ENV == 'local' || isset($_COOKIE['logger'])) {
                if (is_writable($config->application->logDir)) {
                    return new FileLogger($config->application->logDir . '/cli/' . date('Ymd') . '.log');
                }
            }
            return new \App\Library\Phalcon\Logger\Adapter\Null();
        });

        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $this->di->set('db', function() use ($config) {
            $db = new DbAdapter($config->mysql->toArray());

            if (APPLICATION_ENV == 'local') {

                $eventsManager = new EventsManager();

                $logger = new FileLogger($config->application->logDir . '/cli/' . 'sql_' . date('Ymd') . '.log');

                //Listen all the database events
                $eventsManager->attach('db', function($event, $db) use ($logger) {
                    if ($event->getType() == 'beforeQuery') {
                        $logger->log($db->getSQLStatement(), Logger::INFO);
                    }
                });

                //Assign the eventsManager to the db adapter instance
                $db->setEventsManager($eventsManager);
            }

            return $db;
        });

        /**
         * Set the default namespace for dispatcher
         */
        $this->di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('App\Application\Console\Tasks');
            return $dispatcher;
        });
    }

    protected function registerDebugging()
    {
        // TODO: profiler & debug
    }

    public function registerDependencies()
    {
        $this->registerConfigs();
        $this->registerAutoloaders();
        $this->registerServices();
        $this->registerDebugging();
    }

    public function handle($argv)
    {
        /**
         * Process the console arguments
         */
        $arguments = array();
        foreach ($argv as $k => $arg) {
            if ($k == 1) {
                $arguments['task'] = $arg;
            } elseif ($k == 2) {
                $arguments['action'] = $arg;
            } elseif ($k >= 3) {
                $arguments['params'][] = $arg;
            }
        }

        // Define global constants for the current task and action
        define('CURRENT_TASK',   (isset($argv[1]) ? $argv[1] : null));
        define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

        // Handle incoming arguments
        $console = $this->di->getShared('console');
        $console->handle($arguments);
    }
}

try {
    $app = new ConsoleApplication();
    $app->registerDependencies();
    $app->handle($argv);
} catch (\Exception $e) {
    echo $e->getMessage();
    exit(255);
}