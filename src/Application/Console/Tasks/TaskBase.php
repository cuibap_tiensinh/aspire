<?php

namespace App\Application\Console\Tasks;

/**
 * Class TaskBase
 * @package App\Application\Console\Tasks;
 * @author thang.dang
 */
class TaskBase extends \Phalcon\CLI\Task
{
    private $processTime;
    private $db;

    protected function getDb()
    {
        if (!isset($this->db)) {
            $this->db = $this->getDI()->getShared('db');
        }
        return $this->db;
    }

    protected function jobStarted()
    {
        print_r("\n--------- Job started ---------\n");
        print_r("\n--------- " . date('Y-m-d H:i:s') . "---------\n");
    }

    protected function jobStart($threshold = 300, $flagFile = false): bool
    {
        if (!$flagFile) {
            $flagFile = $this->getFlagFile();
        }

        if ($this->isRunning($threshold, $flagFile)) {
            print_r("\n--------- Job running ---------\n");
            return false;
        }

        $this->jobStarted();
        $this->processTime = microtime(true);
        file_put_contents($flagFile, time());
        print_r("\n--------- Job starting ---------\n");
        print_r("\n--------- " . date('Y-m-d H:i:s') . "---------\n");
        return true;
    }

    protected function jobEnd($flagFile = false)
    {
        if (!$flagFile) {
            $flagFile = $this->getFlagFile();
        }
        $time = round(microtime(true) - $this->processTime, 2);
        file_put_contents($flagFile, 0);
        print_r("\n--------- Job completed in {$time}s ---------\n");
        print_r("\n--------- " . date('Y-m-d H:i:s') . "---------\n");
    }

    protected function jobException(\Exception $exception, $flagFile = false)
    {
        if (!$flagFile) {
            $flagFile = $this->getFlagFile();
        }
        $time = round(microtime(true) - $this->processTime, 2);
        file_put_contents($flagFile, 0);
        print_r("\n--------- Job throw exception in {$time}s ---------\n");
        print_r("\n--------- Exception: " . $exception->getMessage() . ", Trace: " . $exception->getTraceAsString() . " ---------\n");
        print_r("\n--------- " . date('Y-m-d H:i:s') . "---------\n");
    }

    protected function isRunning($threshold = 300, $flagFile = false): bool
    {
        if (!$flagFile) {
            $flagFile = $this->getFlagFile();
        }
        $lastRun = intval(@file_get_contents($flagFile));
        return (time() - $lastRun < $threshold);
    }

    protected function ping($flagFile = false)
    {
        if (!$flagFile) {
            $flagFile = $this->getFlagFile();
        }
        file_put_contents($flagFile, time());
    }

    protected function getFlagFile(): string
    {
        $task = str_replace("\\", "_", CURRENT_TASK);
        $task = strtolower($task);
        return STORAGE_PATH . '/files/job_' . $task . '_' . CURRENT_ACTION . '.conf';
    }
}
