<?php

namespace App\Application\Console\Tasks\Cronjob;

use App\Application\Console\Tasks\TaskBase;

class LoanStatusTask extends TaskBase
{
    public function mainAction()
    {
        if ($this->jobStart()) {
            try {
                $sql = 'UPDATE customer_loan SET status = "PAID" WHERE loan_term = repaid_count AND amount <= repaid_amount';
                $this->getDb()->execute($sql);
            } catch (\Exception $e) {
                $this->jobException($e);
            }
            $this->jobEnd();
        }
    }
}