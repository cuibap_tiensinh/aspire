<?php

namespace App\Application\Console\Tasks\Cronjob;

use App\Application\Console\Tasks\TaskBase;
use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\ValueObjects\RepaymentStatus;
use App\Core\Domain\ValueObjects\RepaymentType;
use App\Infrastructure\Mysql\MysqlCustomerLoanRepository;
use App\Infrastructure\Mysql\MysqlCustomerScheduledRepaymentRepository;
use DateInterval;

class ScheduleRepaymentLoanTask extends TaskBase
{
    public function mainAction()
    {
        $customerLoanRepository = new MysqlCustomerLoanRepository();
        $scheduledRepaymentRepository = new MysqlCustomerScheduledRepaymentRepository();

        $isBeginTransaction = false;
        if ($this->jobStart()) {
            try {
                $today = DateTimeUtils::today(DateTimeUtils::getDefaultTimezone())->format("Y-m-d 00:00:00");
                $loans = $customerLoanRepository->fetchAllForScheduledRepayment($today, 100);

                foreach ($loans as $loan) {
                    $this->getDb()->begin();
                    $isBeginTransaction = true;

                    $currentDueAt = clone $loan->getScheduledAt();
                    $nextDueAt = $currentDueAt->add(DateInterval::createFromDateString('1 week'));
                    $dueAmount = intval($loan->getBalance() / (float)$loan->getLoanTerm() * 10000.0) / 10000.0;
                    if ($nextDueAt->format('Ymd') == $loan->getEndedAt()->format('Ymd')) {
                        $dueAmount = $loan->getBalance() - ($dueAmount * ($loan->getLoanTerm() - 1));
                    }

                    $scheduledRepayment = (new CustomerScheduledRepayment())
                        ->setCustomerId($loan->getCustomerId())
                        ->setLoanAccountId($loan->getId())
                        ->setStatus(RepaymentStatus::PENDING)
                        ->setVersion(0)
                        ->setCurrency($loan->getCurrency())
                        ->setDueAmount($dueAmount)
                        ->setRepaidType(RepaymentType::TYPE_NONE)
                        ->setDueAt($nextDueAt);
                    $scheduledRepayment->generateScheduleIdIfEmpty($loan->getRepaidCount());
                    $scheduledRepaymentRepository->insert($scheduledRepayment);

                    $loan->setScheduledAt($nextDueAt);
                    $customerLoanRepository->update($loan);

                    $this->getDb()->commit();
                    $isBeginTransaction = false;
                }
            } catch (\Exception $e) {
                if ($isBeginTransaction) $this->getDb()->rollback();
                $this->jobException($e);
            }
            $this->jobEnd();
        }
    }
}