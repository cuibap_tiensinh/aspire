<?php

namespace App\Application\Api\Client\Response;

use App\Core\Domain\Models\Customer;

class MyAccountResponse extends ResponseBase
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}