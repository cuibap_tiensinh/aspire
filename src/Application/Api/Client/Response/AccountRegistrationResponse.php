<?php

namespace App\Application\Api\Client\Response;

use App\Core\Domain\Models\Customer;

class AccountRegistrationResponse extends ResponseBase
{
    private $customer;
    private $access_token;

    public function __construct(Customer $customer, string $access_token)
    {
        $this->customer = $customer;
        $this->access_token = $access_token;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    /**
     * @param string $access_token
     */
    public function setAccessToken(string $access_token): void
    {
        $this->access_token = $access_token;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}