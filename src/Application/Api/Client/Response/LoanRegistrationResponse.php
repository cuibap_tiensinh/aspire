<?php

namespace App\Application\Api\Client\Response;

use App\Core\Domain\Models\CustomerLoan;

class LoanRegistrationResponse extends ResponseBase
{
    private $customer_loan;

    public function __construct(CustomerLoan $customerLoan)
    {
        $this->customer_loan = $customerLoan;
    }

    /**
     * @return CustomerLoan
     */
    public function getCustomerLoan(): CustomerLoan
    {
        return $this->customer_loan;
    }

    /**
     * @param CustomerLoan $customer_loan
     */
    public function setCustomerLoan(CustomerLoan $customer_loan): void
    {
        $this->customer_loan = $customer_loan;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}