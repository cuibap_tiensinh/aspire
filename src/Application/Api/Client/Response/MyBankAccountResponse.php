<?php

namespace App\Application\Api\Client\Response;

use App\Core\Domain\Models\CustomerBankAccount;

class MyBankAccountResponse extends ResponseBase
{
    private $bankAccount;

    public function __construct(CustomerBankAccount $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    /**
     * @return CustomerBankAccount
     */
    public function getBankAccount(): CustomerBankAccount
    {
        return $this->bankAccount;
    }

    /**
     * @param CustomerBankAccount $customer_loan
     */
    public function setBankAccount(CustomerBankAccount $customer_loan): void
    {
        $this->bankAccount = $customer_loan;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}