<?php

namespace App\Application\Api\Client\Response;

use App\Core\Domain\Models\CustomerScheduledRepayment;

class MyLoanRepaymentResponse extends ResponseBase
{
    private $scheduledRepayment;

    public function __construct(CustomerScheduledRepayment $scheduledRepayment)
    {
        $this->scheduledRepayment = $scheduledRepayment;
    }

    /**
     * @return CustomerScheduledRepayment
     */
    public function getScheduledRepayment(): CustomerScheduledRepayment
    {
        return $this->scheduledRepayment;
    }

    /**
     * @param CustomerScheduledRepayment $scheduledRepayment
     */
    public function setScheduledRepayment(CustomerScheduledRepayment $scheduledRepayment): void
    {
        $this->scheduledRepayment = $scheduledRepayment;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}