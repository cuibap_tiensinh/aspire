<?php

namespace App\Application\Api\Client\Response;

use App\Core\Domain\ValueObjects\Paging;

class MyBankAccountsResponse extends ResponseBase
{
    private $paging;
    private $bank_accounts;

    public function __construct(Paging $paging, array $bankAccounts)
    {
        $this->paging = $paging;
        $this->bank_accounts = $bankAccounts;
    }

    /**
     * @return Paging
     */
    public function getPaging(): Paging
    {
        return $this->paging;
    }

    /**
     * @param Paging $paging
     */
    public function setPaging(Paging $paging): void
    {
        $this->paging = $paging;
    }

    /**
     * @return array
     */
    public function getBankAccounts(): array
    {
        return $this->bank_accounts;
    }

    /**
     * @param array $bank_accounts
     */
    public function setBankAccounts(array $bank_accounts): void
    {
        $this->bank_accounts = $bank_accounts;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}