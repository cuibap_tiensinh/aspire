<?php

namespace App\Application\Api\Client\Handlers;

interface AuthorizedHandlerAction
{
    public function authorizeAction($request, $params = []);
}