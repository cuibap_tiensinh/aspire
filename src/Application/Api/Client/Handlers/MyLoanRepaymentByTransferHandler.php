<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\RepayLoanByTransferRequest;
use App\Application\Api\Client\Response\MyLoanRepaymentResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerLoan\CustomerLoanService;
use App\Core\Domain\ValueObjects\LoanRepaymentData;

class MyLoanRepaymentByTransferHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerService $customerService, CustomerLoanService $customerLoanService)
    {
        parent::__construct($customerService);
        $this->customerLoanService = $customerLoanService;
    }

    /**
     * @param RepayLoanByTransferRequest $request
     * @param array $params
     * @return MyLoanRepaymentResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): MyLoanRepaymentResponse
    {
        $request->validate();

        $loanRepaymentData = new LoanRepaymentData(
            $request->getCustomerId(),
            $request->getLoanAccountId(),
            $request->getBankAccountId(),
            $request->getRepaidAmount(),
            ""
        );

        $scheduledRepayment = $this->customerLoanService->repayByTransferMoney($loanRepaymentData);
        return new MyLoanRepaymentResponse($scheduledRepayment);
    }
}