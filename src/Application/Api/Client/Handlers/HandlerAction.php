<?php

namespace App\Application\Api\Client\Handlers;

interface HandlerAction
{
    public function doAction($request, $params = []);
}