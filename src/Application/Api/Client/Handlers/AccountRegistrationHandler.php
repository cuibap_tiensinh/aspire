<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\RegisterAccountRequest;
use App\Application\Api\Client\Response\AccountRegistrationResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Auth\AuthService;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\Customer\CustomerServiceImpl;
use App\Core\Domain\ValueObjects\AccountRegistrationData;
use App\Infrastructure\Mysql\MysqlCustomerRepository;
use App\Infrastructure\Mysql\MysqlCustomerSecurityRepository;

class AccountRegistrationHandler extends HandlerBase
{
    private $customerService;
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * @param RegisterAccountRequest $request
     * @param array $params
     * @return AccountRegistrationResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): AccountRegistrationResponse
    {
        $request->validate();

        $registrationData = new AccountRegistrationData(
            $request->getName(),
            $request->getPhone(),
            $request->getEmail(),
            $request->getPassword()
        );

        $customerWithExtraObj = $this->customerService->register($registrationData);
        return new AccountRegistrationResponse($customerWithExtraObj->getCustomer(), $customerWithExtraObj->getAccessToken());
    }
}