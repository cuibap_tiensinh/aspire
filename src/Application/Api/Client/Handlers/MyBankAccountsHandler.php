<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\GetBankAccountsRequest;
use App\Application\Api\Client\Response\MyBankAccountsResponse;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountService;

class MyBankAccountsHandler extends AuthorizedHandlerBase
{
    private $bankAccountService;

    public function __construct(CustomerService $customerService, CustomerBankAccountService $bankAccountService)
    {
        parent::__construct($customerService);
        $this->bankAccountService = $bankAccountService;
    }

    /**
     * @param GetBankAccountsRequest $request
     * @param array $params
     * @return MyBankAccountsResponse
     */
    public function doAction($request, $params = []): MyBankAccountsResponse
    {
        $request->validate();

        $filter = $this->buildFilter($request);

        $pagingObj = $this->bankAccountService->list($filter, $request->getPage(), $request->getSize(), "");
        return new MyBankAccountsResponse($pagingObj->getPaging(), $pagingObj->getBankAccounts());
    }

    private function buildFilter(GetBankAccountsRequest $request): array
    {
        return [
            'customer_id' => $request->getCustomerId()
        ];
    }
}