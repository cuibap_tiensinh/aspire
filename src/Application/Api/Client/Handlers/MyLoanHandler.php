<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\GetLoanDetailRequest;
use App\Application\Api\Client\Response\MyLoanResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerLoan\CustomerLoanService;

class MyLoanHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerService $customerService, CustomerLoanService $customerLoanService)
    {
        parent::__construct($customerService);
        $this->customerLoanService = $customerLoanService;
    }

    /**
     * @param GetLoanDetailRequest $request
     * @param array $params
     * @return MyLoanResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): MyLoanResponse
    {
        $request->validate();

        $customerLoan = $this->customerLoanService->fetchOneAndCustomer($request->getLoanAccountId(), $request->getCustomerId());
        return new MyLoanResponse($customerLoan);
    }
}