<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\LoginAccountRequest;
use App\Application\Api\Client\Response\AccountLoginResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Auth\AuthService;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\Customer\CustomerServiceImpl;
use App\Infrastructure\Mysql\MysqlCustomerRepository;
use App\Infrastructure\Mysql\MysqlCustomerSecurityRepository;

class AccountLoginHandler extends HandlerBase
{
    private $customerService;
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * @param LoginAccountRequest $request
     * @param array $params
     * @return AccountLoginResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): AccountLoginResponse
    {
        $request->validate();

        $customerWithExtraObj = $this->customerService->login($request->getUsername(), $request->getPassword());
        return new AccountLoginResponse($customerWithExtraObj->getCustomer(), $customerWithExtraObj->getAccessToken());
    }
}