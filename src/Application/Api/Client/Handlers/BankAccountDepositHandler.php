<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\DepositBankAccountRequest;
use App\Application\Api\Client\Response\MyBankAccountResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountService;

class BankAccountDepositHandler extends AuthorizedHandlerBase
{
    private $bankAccountService;

    public function __construct(CustomerService $customerService, CustomerBankAccountService $bankAccountService)
    {
        parent::__construct($customerService);
        $this->bankAccountService = $bankAccountService;
    }

    /**
     * @param DepositBankAccountRequest $request
     * @param array $params
     * @return MyBankAccountResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): MyBankAccountResponse
    {
        $request->validate();

        $bankAccount = $this->bankAccountService->deposit(
            $request->getCustomerId(),
            $request->getAccountId(),
            $request->getAmount(),
            $request->getCurrency(),
            $request->getReason()
        );

        return new MyBankAccountResponse($bankAccount);
    }
}