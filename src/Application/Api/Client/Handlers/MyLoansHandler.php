<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\GetLoansRequest;
use App\Application\Api\Client\Response\MyLoansResponse;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerLoan\CustomerLoanService;

class MyLoansHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerService $customerService, CustomerLoanService $customerLoanService)
    {
        parent::__construct($customerService);
        $this->customerLoanService = $customerLoanService;
    }

    /**
     * @param GetLoansRequest $request
     * @param array $params
     * @return MyLoansResponse
     */
    public function doAction($request, $params = []): MyLoansResponse
    {
        $request->validate();

        $filter = $this->buildFilter($request);

        $pagingObj = $this->customerLoanService->list($filter, $request->getPage(), $request->getSize(), "");
        return new MyLoansResponse($pagingObj->getPaging(), $pagingObj->getLoans());
    }

    private function buildFilter(GetLoansRequest $request): array
    {
        $filter = [
            'customer_id' => $request->getCustomerId()
        ];

        if (!empty($request->getStatus())) {
            $filter['status'] = $request->getStatus();
        }

        return $filter;
    }
}