<?php

namespace App\Application\Api\Client\Handlers;

abstract class HandlerBase implements HandlerAction
{
    public function handle($request, $params = [])
    {
        return $this->doAction($request, $params);
    }
}