<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\RequestBase;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Application\Customer\CustomerService;

abstract class AuthorizedHandlerBase extends HandlerBase implements AuthorizedHandlerAction
{
    protected $customerService;
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * @param RequestBase $request
     * @param array $params
     * @throws BusinessException
     */
    public function authorizeAction($request, $params = [])
    {
        if (empty($request->getCustomerId()) || empty($request->getAccessTokenData()) || !is_array($request->getAccessTokenData())) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }

        $accessTokenData = $request->getAccessTokenData();
        if (empty($accessTokenData['token'])) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }

        $customerSecurity = $this->customerService->getCustomerSecurity($request->getCustomerId());
        if (empty($customerSecurity)) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }

        if ($accessTokenData['token'] != $customerSecurity->getAccountToken()) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }
    }

    /**
     * @param RequestBase $request
     * @param array $params
     * @return mixed
     * @throws BusinessException
     */
    public function handle($request, $params = [])
    {
        $this->authorizeAction($request, $params);
        return $this->doAction($request, $params);
    }
}