<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\ShowMyAccountRequest;
use App\Application\Api\Client\Response\MyAccountResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Auth\AuthService;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\Customer\CustomerServiceImpl;
use App\Infrastructure\Mysql\MysqlCustomerRepository;
use App\Infrastructure\Mysql\MysqlCustomerSecurityRepository;

class MyAccountHandler extends AuthorizedHandlerBase
{
    public function __construct(CustomerService $customerService)
    {
        parent::__construct($customerService);
    }

    /**
     * @param ShowMyAccountRequest $request
     * @param array $params
     * @return MyAccountResponse
     *@throws BusinessException
     */
    public function doAction($request, $params = []): MyAccountResponse
    {
        $request->validate();

        $customer = $this->customerService->showMyAccount($request->getCustomerId());
        return new MyAccountResponse($customer);
    }
}