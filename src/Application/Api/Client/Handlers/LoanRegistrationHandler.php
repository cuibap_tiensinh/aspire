<?php

namespace App\Application\Api\Client\Handlers;

use App\Application\Api\Client\Request\RegisterLoanRequest;
use App\Application\Api\Client\Response\LoanRegistrationResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerLoan\CustomerLoanService;
use App\Core\Domain\ValueObjects\LoanRegistrationData;

class LoanRegistrationHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerService $customerService, CustomerLoanService $customerLoanService)
    {
        parent::__construct($customerService);
        $this->customerLoanService = $customerLoanService;
    }

    /**
     * @param RegisterLoanRequest $request
     * @param array $params
     * @return LoanRegistrationResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): LoanRegistrationResponse
    {
        $request->validate();

        $loanRegistrationData = new LoanRegistrationData(
            $request->getCustomerId(),
            $request->getAmount(),
            $request->getCurrency(),
            $request->getLoanTerm()
        );

        $customerLoan = $this->customerLoanService->register($loanRegistrationData);
        return new LoanRegistrationResponse($customerLoan);
    }
}