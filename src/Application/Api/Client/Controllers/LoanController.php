<?php

namespace App\Application\Api\Client\Controllers;

use App\Application\Api\Client\Handlers\LoanRegistrationHandler;
use App\Application\Api\Client\Handlers\MyLoanHandler;
use App\Application\Api\Client\Handlers\MyLoanRepaymentByCashInOfflineHandler;
use App\Application\Api\Client\Handlers\MyLoanRepaymentByTransferHandler;
use App\Application\Api\Client\Handlers\MyLoansHandler;
use App\Application\Api\Client\Request\GetLoanDetailRequest;
use App\Application\Api\Client\Request\GetLoansRequest;
use App\Application\Api\Client\Request\RegisterLoanRequest;
use App\Application\Api\Client\Request\RepayLoanByCashInOfflineRequest;
use App\Application\Api\Client\Request\RepayLoanByTransferRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\String\StringUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountServiceImpl;
use App\Core\Application\CustomerLoan\CustomerLoanServiceImpl;
use App\Infrastructure\Mysql\MysqlCustomerBalanceHistoryRepository;
use App\Infrastructure\Mysql\MysqlCustomerBankAccountRepository;
use App\Infrastructure\Mysql\MysqlCustomerLoanRepository;
use App\Infrastructure\Mysql\MysqlCustomerRepaymentHistoryRepository;
use App\Infrastructure\Mysql\MysqlCustomerScheduledRepaymentRepository;

class LoanController extends ClientControllerBase
{
    private $customerLoanService;

    public function onConstruct()
    {
        parent::onConstruct();
        $customerBankAccountService = new CustomerBankAccountServiceImpl(
            new MysqlCustomerBankAccountRepository(),
            new MysqlCustomerBalanceHistoryRepository(),
            new CurrencyRateService()
        );

        $this->customerLoanService = new CustomerLoanServiceImpl(
            $this->getCustomerService(),
            $customerBankAccountService,
            new MysqlCustomerLoanRepository(),
            new MysqlCustomerScheduledRepaymentRepository(),
            new MysqlCustomerRepaymentHistoryRepository()
        );
    }

    /**
     * @return mixed
     */
    public function getCustomerLoanService()
    {
        return $this->customerLoanService;
    }

    /**
     * @param mixed $customerLoanService
     */
    public function setCustomerLoanService($customerLoanService): void
    {
        $this->customerLoanService = $customerLoanService;
    }

    public function register()
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $request = new RegisterLoanRequest($this->getParams());
            $handler = new LoanRegistrationHandler($this->getCustomerService(), $this->getCustomerLoanService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getCustomerLoan());
        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function list()
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $params = $this->getParams();
            $request = new GetLoansRequest($params);

            $handler = new MyLoansHandler($this->getCustomerService(), $this->getCustomerLoanService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result);

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function show($accountId)
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $accountId = StringUtils::getSafeString($accountId);

            $params = $this->getParams();
            $params["loan_account_id"] = $accountId;
            $request = new GetLoanDetailRequest($params);

            $handler = new MyLoanHandler($this->getCustomerService(), $this->getCustomerLoanService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getCustomerLoan());

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function repayByTransfer($accountId)
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $accountId = StringUtils::getSafeString($accountId);

            $params = $this->getParams();
            $params["loan_account_id"] = $accountId;
            $request = new RepayLoanByTransferRequest($params);

            $handler = new MyLoanRepaymentByTransferHandler($this->getCustomerService(), $this->getCustomerLoanService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getScheduledRepayment());

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function repayByCashInOffline($accountId)
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $accountId = StringUtils::getSafeString($accountId);

            $params = $this->getParams();
            $params["loan_account_id"] = $accountId;
            $request = new RepayLoanByCashInOfflineRequest($params);

            $handler = new MyLoanRepaymentByCashInOfflineHandler($this->getCustomerService(), $this->getCustomerLoanService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getScheduledRepayment());

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }
}