<?php


namespace App\Application\Api\Client\Controllers;

use App\Application\Api\Client\Handlers\AccountLoginHandler;
use App\Application\Api\Client\Handlers\AccountRegistrationHandler;
use App\Application\Api\Client\Handlers\MyAccountHandler;
use App\Application\Api\Client\Request\LoginAccountRequest;
use App\Application\Api\Client\Request\RegisterAccountRequest;
use App\Application\Api\Client\Request\ShowMyAccountRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorMessage;

/**
 * Before the customer requests anything, they must be authenticated.
 * This class handles the request for
 * - Registration of a new account and returns a response to the customer
 * - Login the account
 *
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class AccountController extends ClientControllerBase
{
    public function register()
    {
        try {
            $request = new RegisterAccountRequest($this->getParams());
            $handler = new AccountRegistrationHandler($this->getCustomerService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result);
        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function login()
    {
        try {
            $request = new LoginAccountRequest($this->getParams());
            $handler = new AccountLoginHandler($this->getCustomerService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result);
        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function showMe()
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $request = new ShowMyAccountRequest($this->getParams());
            $handler = new MyAccountHandler($this->getCustomerService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getCustomer());
        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }
}