<?php

namespace App\Application\Api\Client\Controllers;

class IndexController extends ClientControllerBase
{
    public function index()
    {
        return $this->responseSuccess([
            'name' => 'Aspire Api',
            'version' => '1.0'
        ]);
    }
}