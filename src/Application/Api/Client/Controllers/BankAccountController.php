<?php

namespace App\Application\Api\Client\Controllers;

use App\Application\Api\Client\Handlers\BankAccountDepositHandler;
use App\Application\Api\Client\Handlers\MyBankAccountsHandler;
use App\Application\Api\Client\Request\DepositBankAccountRequest;
use App\Application\Api\Client\Request\GetBankAccountsRequest;
use App\Application\Api\Client\Response\MyBankAccountResponse;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\String\StringUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountServiceImpl;
use App\Infrastructure\Mysql\MysqlCustomerBalanceHistoryRepository;
use App\Infrastructure\Mysql\MysqlCustomerBankAccountRepository;

class BankAccountController extends ClientControllerBase
{
    private $customerBankAccountService;

    public function onConstruct()
    {
        parent::onConstruct();
        $this->customerBankAccountService = new CustomerBankAccountServiceImpl(
            new MysqlCustomerBankAccountRepository(),
            new MysqlCustomerBalanceHistoryRepository(),
            new CurrencyRateService()
        );
    }

    /**
     * @return mixed
     */
    public function getCustomerBankAccountService()
    {
        return $this->customerBankAccountService;
    }

    /**
     * @param mixed $customerBankAccountService
     */
    public function setCustomerBankAccountService($customerBankAccountService): void
    {
        $this->customerBankAccountService = $customerBankAccountService;
    }

    public function list()
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $params = $this->getParams();
            $request = new GetBankAccountsRequest($params);

            $handler = new MyBankAccountsHandler($this->getCustomerService(), $this->getCustomerBankAccountService());
            $result = $handler->handle($request);
            return $this->responseSuccess($result);

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function deposit($accountId)
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $accountId = StringUtils::getSafeString($accountId);

            $params = $this->getParams();
            $params["account_id"] = $accountId;
            $request = new DepositBankAccountRequest($params);

            $handler = new BankAccountDepositHandler($this->getCustomerService(), $this->getCustomerBankAccountService());

            /** @var MyBankAccountResponse $result */
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getBankAccount());

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }
}