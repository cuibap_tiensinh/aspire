<?php

namespace App\Application\Api\Client\Controllers;


use App\Application\Api\ControllerBase;
use App\Core\Application\Auth\AuthServiceImpl;
use App\Core\Application\Customer\CustomerServiceImpl;
use App\Infrastructure\Mysql\MysqlCustomerRepository;
use App\Infrastructure\Mysql\MysqlCustomerSecurityRepository;

/**
 * ClientControllerBase
 * This is the base controller for all controllers in the application
 */
abstract class ClientControllerBase extends ControllerBase
{
    private $authService;
    private $customerService;
    private $customerId = "";
    private $accessTokenData = [];

    public function onConstruct()
    {
        parent::onConstruct();
        $this->authService = new AuthServiceImpl(getenv('CUSTOMER_SECURITY_SECRET_KEY'));

        $accessToken = $this->request->getHeader('access-token');
        $this->accessTokenData = $this->authService->decodeAccessToken($accessToken);
        if ($this->authService->isValidAccessToken($this->accessTokenData)) {
            $this->customerId = $this->accessTokenData['id'];
        }
    }

    public function getAuthService(): AuthServiceImpl
    {
        return $this->authService;
    }

    public function getCustomerService(): CustomerServiceImpl
    {
        if (!isset($this->customerService)) {
            $this->customerService = new CustomerServiceImpl(
                $this->authService,
                new MysqlCustomerRepository(),
                new MysqlCustomerSecurityRepository()
            );
        }

        return $this->customerService;
    }

    public function getParams($key = null, $default = null)
    {
        $params = parent::getParams($key, $default);
        $params['customer_id'] = $this->customerId;
        $params['access_token_data'] = $this->accessTokenData;
        return $params;
    }

    protected function validate(array $params, array $required)
    {
        // TODO: Implement validate() method.
    }

    protected function isAuthentication(): bool
    {
        return !empty($this->customerId);
    }
}