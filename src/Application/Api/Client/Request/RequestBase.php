<?php

namespace App\Application\Api\Client\Request;

use App\Common\Utils\Arrays\ArrayUtils;

abstract class RequestBase
{
    private $customerId;
    private $accessTokenData;

    public function __construct(array $params)
    {
        $this->customerId = (string)ArrayUtils::getOrDefault($params, "customer_id");;
        $this->accessTokenData = (array)ArrayUtils::getOrDefault($params, "access_token_data");;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return array
     */
    public function getAccessTokenData(): array
    {
        return $this->accessTokenData;
    }

    /**
     * Validate inputs
     * @return mixed
     */
    public abstract function validate();
}