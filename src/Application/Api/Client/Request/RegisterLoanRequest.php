<?php

namespace App\Application\Api\Client\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\Arrays\ArrayUtils;

class RegisterLoanRequest extends RequestBase
{
    private $amount;
    private $currency;
    private $loan_term;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->amount = floatval(ArrayUtils::getOrDefault($params, "amount"));
        $this->currency = (string)ArrayUtils::getOrDefault($params, "currency");
        $this->loan_term = intval(ArrayUtils::getOrDefault($params, "loan_term"));
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getLoanTerm(): int
    {
        return $this->loan_term;
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        $invalidKeys = [];
        if (empty($this->amount)) $invalidKeys[] = "amount";
        if (empty($this->currency)) $invalidKeys[] = "currency";
        if (empty($this->loan_term)) $invalidKeys[] = "loan_term";


        if (!empty($invalidKeys)) {
            throw new BusinessException(ErrorMessage::INVALID_REGISTER_LOAN_DATA,ErrorCode::INVALID_DATA);
        }
    }
}