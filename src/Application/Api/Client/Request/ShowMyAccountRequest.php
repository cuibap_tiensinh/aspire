<?php

namespace App\Application\Api\Client\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;

class ShowMyAccountRequest extends RequestBase
{
    public function __construct(array $params)
    {
        parent::__construct($params);
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        if (empty($this->getCustomerId())) {
            throw new BusinessException(ErrorMessage::INVALID_DATA_UNAUTHORIZED, ErrorCode::INVALID_DATA);
        }
    }
}