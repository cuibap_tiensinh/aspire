<?php

namespace App\Application\Api\Client\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\Arrays\ArrayUtils;

class RepayLoanByTransferRequest extends RequestBase
{
    private $loan_account_id;
    private $bank_account_id;
    private $repaid_amount;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->loan_account_id = (string)ArrayUtils::getOrDefault($params, 'loan_account_id');
        $this->bank_account_id = (string)ArrayUtils::getOrDefault($params, 'bank_account_id');
        $this->repaid_amount = floatval(ArrayUtils::getOrDefault($params, 'repaid_amount'));
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loan_account_id;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bank_account_id;
    }

    /**
     * @return float
     */
    public function getRepaidAmount(): float
    {
        return $this->repaid_amount;
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        $invalidKeys = [];
        if (empty($this->loan_account_id)) $invalidKeys[] = "loan_account_id";
        if (empty($this->bank_account_id)) $invalidKeys[] = "bank_account_id";
        if ($this->repaid_amount <= 0.0) $invalidKeys[] = "repaid_amount";

        if (!empty($invalidKeys)) {
            throw new BusinessException(ErrorMessage::INVALID_REPAY_LOAN_BY_TRANSFER_DATA,ErrorCode::INVALID_DATA);
        }
    }


}