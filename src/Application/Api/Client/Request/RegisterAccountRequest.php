<?php

namespace App\Application\Api\Client\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\Arrays\ArrayUtils;
use App\Common\Utils\String\StringUtils;

class RegisterAccountRequest extends RequestBase
{
    private $name;
    // The customer phone must follow the format (+Country-code) XXXXXXXXXX. Example: (+1) 123456789
    private $phone;
    private $email;
    private $password;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->name = (string)ArrayUtils::getOrDefault($params, "name");
        $this->phone = (string)ArrayUtils::getOrDefault($params, "phone");
        $this->email = (string)ArrayUtils::getOrDefault($params, "email");
        $this->password = (string)ArrayUtils::getOrDefault($params, "password");
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        $invalidKeys = [];
        if (empty($this->name)) $invalidKeys[] = "name";
        if (!StringUtils::isValidPhone($this->phone)) $invalidKeys[] = "phone";
        if (!StringUtils::isValidEmail($this->email)) $invalidKeys[] = "email";
        if (!StringUtils::isValidPassword($this->password)) $invalidKeys[] = "password";

        if (!empty($invalidKeys)) {
            throw new BusinessException(ErrorMessage::INVALID_REGISTER_ACCOUNT_DATA,ErrorCode::INVALID_DATA);
        }
    }
}