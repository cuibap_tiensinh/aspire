<?php

namespace App\Application\Api\Client\Request;

use App\Common\Utils\Arrays\ArrayUtils;

class GetLoansRequest extends RequestBase
{
    private $page;
    private $size;
    private $status;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->page = intval(ArrayUtils::getOrDefault($params, "page", 1));
        $this->size = intval(ArrayUtils::getOrDefault($params, "size", 10));
        $this->status = (string)(ArrayUtils::getOrDefault($params, "status"));

        if ($this->page <= 0) $this->page = 1;
        if ($this->size <= 0) $this->size = 10;
        if ($this->size > 50) $this->size = 50;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    public function validate()
    {
    }


}