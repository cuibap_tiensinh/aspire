<?php

namespace App\Application\Api\Client\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\Arrays\ArrayUtils;

class DepositBankAccountRequest extends RequestBase
{
    private $amount;
    private $currency;
    private $account_id;
    private $reason;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->amount = floatval(ArrayUtils::getOrDefault($params, "amount"));
        $this->currency = (string)ArrayUtils::getOrDefault($params, "currency");
        $this->account_id = (string)(ArrayUtils::getOrDefault($params, "account_id"));
        $this->reason = (string)(ArrayUtils::getOrDefault($params, "reason", ""));
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getAccountId(): string
    {
        return $this->account_id;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        $invalidKeys = [];
        if (empty($this->amount)) $invalidKeys[] = "amount";
        if (empty($this->currency)) $invalidKeys[] = "currency";
        if (empty($this->account_id)) $invalidKeys[] = "account_id";


        if (!empty($invalidKeys)) {
            throw new BusinessException(ErrorMessage::INVALID_DEPOSIT_DATA,ErrorCode::INVALID_DEPOSIT_DATA);
        }
    }
}