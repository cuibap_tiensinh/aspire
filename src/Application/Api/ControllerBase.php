<?php

namespace App\Application\Api;

use App\Common\Response\ApiResp;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Response\HTTPStatus;
use App\Common\Response\JSONResponse;
use App\Common\Utils\Arrays\ArrayUtils;
use Phalcon\Mvc\Controller;

/**
 * ClientControllerBase
 * This is the base controller for all controllers in the application
 */
abstract class ControllerBase extends Controller
{
    private $params;

    public function onConstruct()
    {

    }

    protected function getPost($key = null)
    {
        $post = $this->request->getJsonRawBody();

        if (is_object($post)) {
            $post = json_decode(json_encode($post), true);
        } elseif (is_string($post)) {
            $post = json_decode($post, true);
        }

        if ($key !== null) {
            return $post[$key] ?? null;
        }

        return $post ?? [];
    }

    protected function getHeaders()
    {
        return $this->request->getHeaders();
    }

    protected function getParams($key = null, $default = null)
    {
        if (!$this->params) {
            $this->params = $this->request->getQuery();
            $post = $this->getPost();
            $this->params = array_merge($this->params ?? [], !empty($post) ? $post : []);
            $this->params = array_merge($this->params, $this->request->getPost() ?? []);
            $this->params = ArrayUtils::stripTags($this->params);
        }

        if ($key) {
            return $this->params[$key] ?? $default;
        }

        return $this->params;
    }

    protected abstract function validate(array $params, array $required);

    protected function responseUnauthorized()
    {
        $apiResp = new ApiResp(ErrorCode::UNAUTHORIZED,ErrorMessage::UNAUTHORIZED);
        return $this->response(HTTPStatus::HTTP_STATUS_UNAUTHORIZED, $apiResp);
    }

    protected function responseNoContent()
    {
        $apiResp = new ApiResp(ErrorCode::SUCCESS, ErrorMessage::SUCCESS);
        return $this->response(HTTPStatus::HTTP_STATUS_NO_CONTENT, $apiResp);
    }

    protected function responseNotFound($errorCode, $message)
    {
        $apiResp = new ApiResp($errorCode, $message);
        return $this->response(HTTPStatus::HTTP_STATUS_NOT_FOUND, $apiResp);
    }

    protected function responseBadRequest($errorCode, $message, $data = null)
    {
        $apiResp = new ApiResp($errorCode, $message, $data);
        return $this->response(HTTPStatus::HTTP_STATUS_BAD_REQUEST, $apiResp);
    }

    protected function responseInternalServerError($errorCode, $message, $data = null)
    {
        $apiResp = new ApiResp($errorCode, $message, $data);
        return $this->response(HTTPStatus::HTTP_STATUS_INTERNAL_ERROR, $apiResp);
    }

    protected function responseSuccess($data = null)
    {
        $apiResp = new ApiResp(ErrorCode::SUCCESS, ErrorMessage::SUCCESS, $data);
        return $this->response(HTTPStatus::HTTP_STATUS_SUCCESS, $apiResp);
    }

    protected function response(int $httpStatus, ApiResp $apiResp)
    {
        $this->response->setStatusCode($httpStatus);
        $this->response->setJsonContent($apiResp);
        $this->response->setContentType('application/json', 'UTF-8');

        return $this->response;
    }
}