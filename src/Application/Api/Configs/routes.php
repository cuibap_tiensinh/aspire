<?php

use Phalcon\Mvc\Micro\Collection;

$collections = [];

$index = new Collection();
$index->setHandler('\App\Application\Api\Client\Controllers\IndexController', true);
$index->get('/', 'index');
$collections[] = $index;

$account = new Collection();
$account->setHandler('\App\Application\Api\Client\Controllers\AccountController', true);
$account->post('/customers/login', 'login');
$account->post('/customers', 'register');
$account->get('/customers/me', 'showMe');
$collections[] = $account;

$loan = new Collection();
$loan->setHandler('\App\Application\Api\Client\Controllers\LoanController', true);
$loan->post('/loans', 'register');
$loan->get('/loans', 'list');
$loan->get('/loans/{account_id}', 'show');
$loan->post('/loans/{account_id}/repayments/transfer', 'repayByTransfer');
$loan->post('/loans/{account_id}/repayments/cash-in-offline', 'repayByCashInOffline');
$collections[] = $loan;

$bankAccount = new Collection();
$bankAccount->setHandler('\App\Application\Api\Client\Controllers\BankAccountController', true);
$bankAccount->get('/bank-accounts', 'list');
$bankAccount->post('/bank-accounts/{account_id}/deposit', 'deposit');
$collections[] = $bankAccount;


$adminAccount = new Collection();
$adminAccount->setHandler('\App\Application\Api\Admin\Controllers\AccountController', true);
$adminAccount->post('/admin/login', 'login');
$collections[] = $adminAccount;

$adminLoan = new Collection();
$adminLoan->setHandler('\App\Application\Api\Admin\Controllers\LoanController', true);
$adminLoan->get('/admin/loans', 'list');
$adminLoan->get('/admin/loans/{account_id}', 'show');
$adminLoan->put('/admin/loans/{account_id}/approval', 'approve');
$collections[] = $adminLoan;

return $collections;