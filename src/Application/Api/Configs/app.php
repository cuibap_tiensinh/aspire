<?php

return [
    'mysql' => [
        'adapter'  => 'Mysql',
        'host'     => getenv('DB_HOST'),
        'dbname'   => getenv('DB_DATABASE'),
        'username' => getenv('DB_USERNAME'),
        'password' => getenv('DB_PASSWORD'),
        'charset'  => 'utf8',
        'options' => [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \Phalcon\Db::FETCH_OBJ,
        ]
    ],
    'application' => [
        'baseUri'       => '/',
        'staticBaseUri' => '',
        'publicUrl'     => '',
        'profiler'      => false,
        'logDir'        => ROOT_PATH . '/storage/logs/',
        'cacheDir'      => ROOT_PATH . '/storage/cache/',
        'metadataDir'   => ROOT_PATH . '/storage/metadata/',
        'sessionDir'    => ROOT_PATH . '/storage/sessions/',
    ],
    'redis' => [
        'host' => getenv('REDIS_HOST'),
        'port' => '6379',
    ],
    'secretKeys' => [
        'customer' => getenv('CUSTOMER_SECURITY_SECRET_KEY'),
        'admin' => getenv('ADMIN_SECURITY_SECRET_KEY'),
    ]
];