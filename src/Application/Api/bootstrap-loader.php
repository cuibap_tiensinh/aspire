<?php

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use Phalcon\Http\Response;
use Phalcon\Logger;
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\Micro as BaseApplication;
use Phalcon\Mvc\Model\Metadata\Files as MetaDataAdapter;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Http\Response\Cookies;
use Phalcon\Cache\Backend\Memcache;
use Phalcon\Security;
use Phalcon\Config;
use Phalcon\Mvc\Model\Manager as ModelsManager;


class Application extends BaseApplication
{
    protected $config;

    public function main()
    {
        $this->registerConfigs();
        $this->registerAutoloaders();
        $this->registerServices();
        $this->registerRoutes();
        $this->registerDebugging();

        $this->after(function () {
            // Results returned from the route's controller.  All Controllers should return an array
            $response = $this->getReturnedValue();
            $response->send();
        });

        $this->error(
            function ($exception) {
                $message = $exception instanceof BusinessException ? $exception->getMessage() : ErrorMessage::INTERNAL_ERROR;
                echo json_encode(
                    [
                        "code"    => $exception->getCode(),
                        "message" => $message,
                        "dev_message" => $exception->getMessage(),
                        "data" => null
                    ]
                );
                exit;
            }
        );

        $this->notFound(
            function () {
                echo json_encode(
                    [
                        "code"    => ErrorCode::ROUTE_NOT_FOUND,
                        "message" => ErrorMessage::ROUTE_NOT_FOUND,
                        "data" => null
                    ]
                );
                exit;
            }
        );

        $this->handle();
    }

    protected function registerConfigs()
    {
        $this->config = new Config(include CONFIG_PATH . '/app.php');
    }

    protected function registerAutoloaders()
    {
        require ROOT_PATH . '/vendor/autoload.php';

        $loader = new Loader();
        $loader->registerNamespaces(array(
            'App' => APPLICATION_PATH,
            'Tests' => ROOT_PATH . '/tests',
            'Phalcon' => ROOT_PATH . '/vendor/phalcon/incubator/Library/Phalcon/'
        ))->register();
    }

    /**
     * This methods registers the services to be used by the application
     */
    protected function registerServices()
    {
        $config = $this->config;

        /**
         * The FactoryDefault Dependency Injector automatically registers the right services to provide a full stack framework
         */
        $di = new FactoryDefault();

        $di->setShared('app', $this);

        $di->setShared('config', $config);

        /**
         * Start the session the first time some component request the session service
         */
        $di->setShared('session', function () use ($config) {
            // @ini_set('session.save_path', $config->application->sessionDir);
            $session = new SessionAdapter();
            $session->start();
            return $session;
        });

        /**
         * Registering a logger
         */
        $di->set('logger', function () use ($config) {
            if (APPLICATION_ENV == 'local' || isset($_COOKIE['logger'])) {
                if (is_writable($config->application->logDir)) {
                    return new FileLogger($config->application->logDir . date('Ymd') . '.log');
                }
            }
            return new \App\Library\Phalcon\Logger\Adapter\Null();
        });

        /**
         * Registering a router
         */
        $di->set('router', function () {
            $router = new Router(false);
            $router->removeExtraSlashes(true);
            $router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);
            return $router;
        });

        $di->set('url', function () use ($config) {
            $url = new Url();
            $url->setBaseUri($config->application->baseUri);
            $url->setStaticBaseUri($config->application->staticBaseUri);
            return $url;
        });

        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di->set('db', function () use ($config) {
            $db = new DbAdapter($config->mysql->toArray());

            if (APPLICATION_ENV == 'local') {

                $eventsManager = new EventsManager();

                $logger = new FileLogger($config->application->logDir . 'sql_' . date('Ymd') . '.log');

                //Listen all the database events
                $eventsManager->attach('db', function ($event, $db) use ($logger) {
                    if ($event->getType() == 'beforeQuery') {
                        $logger->log($db->getSQLStatement(), Logger::INFO);
                    }
                });

                //Assign the eventsManager to the db adapter instance
                $db->setEventsManager($eventsManager);
            }

            return $db;
        });

        // Set a models manager
        $di->set(
            'modelsManager',
            new ModelsManager()
        );

        /**
         * If the configuration specify the use of metadata adapter use it or use memory otherwise
         */
        if (is_dir($config->application->metadataDir)) {
            $di->set('modelsMetadata', function () use ($config) {
                return new MetaDataAdapter(array(
                    'metaDataDir' => $config->application->metadataDir
                ));
            });
        }

        /**
         * Set the default namespace for dispatcher
         */
        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('App\Application\Api\Client\Controllers');
            return $dispatcher;
        });

        $di->set('cookies', function () {
            $cookies = new Cookies();
            $cookies->useEncryption(false);
            return $cookies;
        });

        $di->set(
            'security',
            function () {
                $security = new Security();

                // Set the password hashing factor to 12 rounds
                $security->setWorkFactor(12);

                return $security;
            },
            true
        );

        if (isset($config->statsd) && isset($config->statsd->enable) && $config->statsd->enable == true) {
            $di->set(
                'statsd',
                function () use ($config) {
                    $statsd = new \League\StatsD\Client();
                    $statsd->configure(array(
                        'host' => $config->statsd->host,
                        'port' => $config->statsd->port,
                        'namespace' => $config->statsd->namespace,
                    ));

                    return $statsd;
                },
                true
            );
        }

        if (isset($config->logger) && isset($config->logger->enable) && $config->logger->enable == true) {
            $di->set(
                'logger',
                function () use ($config) {
                    $transport = new Gelf\Transport\UdpTransport(
                        $config->logger->host,
                        $config->logger->port,
                        0
                    );

                    // While the UDP transport is itself a publisher, we wrap it in a real Publisher for convenience.
                    // A publisher allows for message validation before transmission, and also supports sending
                    // messages to multiple backends at once.
                    $publisher = new Gelf\Publisher();
                    $publisher->addTransport($transport);

                    $logger = new Gelf\Logger($publisher, $config->logger->facility);
                    return $logger;
                },
                true
            );
        }

        $this->setDI($di);
    }

    protected function registerRoutes()
    {
        $collections = require CONFIG_PATH . '/routes.php';
        foreach ($collections as $collection) {
            $this->mount($collection);
        }
    }

    protected function registerDebugging()
    {
        // TODO: profiler & debug
    }

}

try {
    $app = new Application();
    $app->main();
} catch (\Exception $exception){}