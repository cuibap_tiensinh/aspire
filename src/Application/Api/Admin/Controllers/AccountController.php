<?php

namespace App\Application\Api\Admin\Controllers;

use App\Application\Api\Admin\Handlers\AdminLoginHandler;
use App\Application\Api\Admin\Request\AdminLoginRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorMessage;
use App\Core\Application\Admin\AdminServiceImpl;
use App\Infrastructure\Mysql\MysqlAdminSecurityRepository;

class AccountController extends AdminControllerBase
{
    private $adminService;

    public function onConstruct()
    {
        parent::onConstruct();

        $this->adminService = new AdminServiceImpl(
            $this->getAuthService(),
            new MysqlAdminSecurityRepository());
    }

    public function login()
    {
        try {
            $request = new AdminLoginRequest($this->getParams());
            $handler = new AdminLoginHandler($this->adminService);
            $result = $handler->handle($request);
            return $this->responseSuccess($result);
        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }
}