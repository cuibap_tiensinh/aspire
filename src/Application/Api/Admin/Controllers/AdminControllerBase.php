<?php

namespace App\Application\Api\Admin\Controllers;

use App\Application\Api\ControllerBase;
use App\Core\Application\Auth\AuthServiceImpl;

abstract class AdminControllerBase extends ControllerBase
{
    private $authService;
    private $adminId = "";
    private $accessTokenData = [];

    public function onConstruct()
    {
        parent::onConstruct();
        $this->authService = new AuthServiceImpl(getenv('ADMIN_SECURITY_SECRET_KEY'));

        $accessToken = $this->request->getHeader('access-token');
        $this->accessTokenData = $this->authService->decodeAccessToken($accessToken);
        if ($this->authService->isValidAccessToken($this->accessTokenData)) {
            $this->adminId = $this->accessTokenData['id'];
        }
    }

    public function getAuthService()
    {
        return $this->authService;
    }

    public function getParams($key = null, $default = null)
    {
        $params = parent::getParams($key, $default);
        $params['admin_id'] = $this->adminId;
        $params['access_token_data'] = $this->accessTokenData;
        return $params;
    }

    protected function validate(array $params, array $required)
    {
        // TODO: Implement validate() method.
    }

    protected function isAuthentication(): bool
    {
        return !empty($this->adminId);
    }
}