<?php

namespace App\Application\Api\Admin\Controllers;

use App\Application\Api\Admin\Handlers\LoanApprovalHandler;
use App\Application\Api\Admin\Handlers\LoanDetailHandler;
use App\Application\Api\Admin\Request\LoanApprovalRequest;
use App\Application\Api\Admin\Request\GetLoanDetailRequest;
use App\Application\Api\Admin\Request\GetLoansRequest;
use App\Application\Api\Admin\Handlers\LoansHandler;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\String\StringUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Application\Customer\CustomerServiceImpl;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountServiceImpl;
use App\Core\Application\CustomerLoan\CustomerLoanServiceImpl;
use App\Infrastructure\Mysql\MysqlAdminSecurityRepository;
use App\Infrastructure\Mysql\MysqlCustomerBalanceHistoryRepository;
use App\Infrastructure\Mysql\MysqlCustomerBankAccountRepository;
use App\Infrastructure\Mysql\MysqlCustomerLoanRepository;
use App\Infrastructure\Mysql\MysqlCustomerRepaymentHistoryRepository;
use App\Infrastructure\Mysql\MysqlCustomerRepository;
use App\Infrastructure\Mysql\MysqlCustomerScheduledRepaymentRepository;
use App\Infrastructure\Mysql\MysqlCustomerSecurityRepository;

class LoanController extends AdminControllerBase
{
    private $customerLoanService;
    private $adminSecurityRepository;

    public function onConstruct()
    {
        parent::onConstruct();

        $customerService = new CustomerServiceImpl(
            $this->getAuthService(),
            new MysqlCustomerRepository(),
            new MysqlCustomerSecurityRepository()
        );

        $customerBankAccountService = new CustomerBankAccountServiceImpl(
            new MysqlCustomerBankAccountRepository(),
            new MysqlCustomerBalanceHistoryRepository(),
            new CurrencyRateService()
        );

        $this->customerLoanService = new CustomerLoanServiceImpl(
            $customerService,
            $customerBankAccountService,
            new MysqlCustomerLoanRepository(),
            new MysqlCustomerScheduledRepaymentRepository(),
            new MysqlCustomerRepaymentHistoryRepository()
        );

        $this->adminSecurityRepository = new MysqlAdminSecurityRepository();
    }

    /**
     * @param mixed $customerLoanService
     */
    public function setCustomerLoanService($customerLoanService): void
    {
        $this->customerLoanService = $customerLoanService;
    }

    /**
     * @param mixed $adminSecurityRepository
     */
    public function setAdminSecurityRepository($adminSecurityRepository): void
    {
        $this->adminSecurityRepository = $adminSecurityRepository;
    }

    public function list()
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $params = $this->getParams();
            $request = new GetLoansRequest($params);

            $handler = new LoansHandler($this->customerLoanService, $this->adminSecurityRepository);
            $result = $handler->handle($request);
            return $this->responseSuccess($result);

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function show($accountId)
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $accountId = StringUtils::getSafeString($accountId);

            $params = $this->getParams();
            $params["loan_account_id"] = $accountId;
            $request = new GetLoanDetailRequest($params);

            $handler = new LoanDetailHandler($this->customerLoanService, $this->adminSecurityRepository);
            $result = $handler->handle($request);
            return $this->responseSuccess($result->getCustomerLoan());

        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }

    public function approve($accountId)
    {
        try {
            if (!$this->isAuthentication()) {
                return $this->responseUnauthorized();
            }

            $accountId = StringUtils::getSafeString($accountId);

            $params = $this->getParams();
            $params["loan_account_id"] = $accountId;
            $request = new LoanApprovalRequest($params);

            $handler = new LoanApprovalHandler($this->customerLoanService, $this->adminSecurityRepository);
            $handler->handle($request);
            return $this->responseSuccess();
        } catch (BusinessException $e) {
            return $this->responseBadRequest($e->getCode(), $e->getMessage());
        } catch (\Exception $e) {
            return $this->responseInternalServerError($e->getCode(), ErrorMessage::INTERNAL_ERROR);
        }
    }
}