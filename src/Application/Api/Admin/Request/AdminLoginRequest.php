<?php

namespace App\Application\Api\Admin\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\Arrays\ArrayUtils;

class AdminLoginRequest extends RequestBase
{
    private $username;
    private $password;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->username = (string)ArrayUtils::getOrDefault($params, 'username');
        $this->password = (string)ArrayUtils::getOrDefault($params, 'password');
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        $invalidKeys = [];
        if (empty($this->username)) $invalidKeys[] = "username";
        if (empty($this->password)) $invalidKeys[] = "password";

        if (!empty($invalidKeys)) {
            throw new BusinessException(ErrorMessage::INVALID_LOGIN_DATA, ErrorCode::INVALID_DATA);
        }
    }
}