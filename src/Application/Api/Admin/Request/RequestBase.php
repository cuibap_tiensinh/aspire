<?php

namespace App\Application\Api\Admin\Request;

use App\Common\Utils\Arrays\ArrayUtils;

abstract class RequestBase
{
    private $adminId;
    private $accessTokenData;

    public function __construct(array $params)
    {
        $this->adminId = (string)ArrayUtils::getOrDefault($params, "admin_id");;
        $this->accessTokenData = (array)ArrayUtils::getOrDefault($params, "access_token_data");;
    }

    /**
     * @return string
     */
    public function getAdminId(): string
    {
        return $this->adminId;
    }

    /**
     * @return array
     */
    public function getAccessTokenData(): array
    {
        return $this->accessTokenData;
    }

    /**
     * Validate inputs
     * @return mixed
     */
    public abstract function validate();
}