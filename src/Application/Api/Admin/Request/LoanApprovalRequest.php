<?php

namespace App\Application\Api\Admin\Request;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\Arrays\ArrayUtils;

class LoanApprovalRequest extends RequestBase
{
    private $loan_account_id;

    public function __construct(array $params)
    {
        parent::__construct($params);
        $this->loan_account_id = (string)ArrayUtils::getOrDefault($params, 'loan_account_id');
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loan_account_id;
    }

    /**
     * @throws BusinessException
     */
    public function validate()
    {
        if (empty($this->loan_account_id)) {
            throw new BusinessException(ErrorMessage::INVALID_APPROVE_LOAN_DATA,ErrorCode::INVALID_DATA);
        }
    }
}