<?php

namespace App\Application\Api\Admin\Response;

use App\Core\Domain\ValueObjects\Paging;

class GetLoansResponse extends ResponseBase
{
    private $paging;
    private $loans;

    public function __construct(Paging $paging, array $customerLoans)
    {
        $this->paging = $paging;
        $this->loans = $customerLoans;
    }

    /**
     * @return Paging
     */
    public function getPaging(): Paging
    {
        return $this->paging;
    }

    /**
     * @param Paging $paging
     */
    public function setPaging(Paging $paging): void
    {
        $this->paging = $paging;
    }

    /**
     * @return array
     */
    public function getLoans(): array
    {
        return $this->loans;
    }

    /**
     * @param array $loans
     */
    public function setLoans(array $loans): void
    {
        $this->loans = $loans;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}