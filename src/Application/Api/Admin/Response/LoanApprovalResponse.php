<?php

namespace App\Application\Api\Admin\Response;

class LoanApprovalResponse extends ResponseBase
{
    private $is_success;

    public function __construct(bool $is_success)
    {
        $this->is_success = $is_success;
    }

    /**
     * @return bool
     */
    public function isIsSuccess(): bool
    {
        return $this->is_success;
    }

    /**
     * @param bool $is_success
     */
    public function setIsSuccess(bool $is_success): void
    {
        $this->is_success = $is_success;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}