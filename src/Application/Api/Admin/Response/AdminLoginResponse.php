<?php

namespace App\Application\Api\Admin\Response;

class AdminLoginResponse extends ResponseBase
{
    private $access_token;

    public function __construct(string $access_token)
    {
        $this->access_token = $access_token;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    /**
     * @param string $access_token
     */
    public function setAccessToken(string $access_token): void
    {
        $this->access_token = $access_token;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}