<?php

namespace App\Application\Api\Admin\Handlers;

use App\Application\Api\Admin\Request\LoanApprovalRequest;
use App\Application\Api\Admin\Response\LoanApprovalResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\CustomerLoan\CustomerLoanService;
use App\Core\Domain\Repositories\AdminSecurityRepository;

class LoanApprovalHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerLoanService $service, AdminSecurityRepository $repository)
    {
        parent::__construct($repository);
        $this->customerLoanService = $service;
    }

    /**
     * @param LoanApprovalRequest $request
     * @param array $params
     * @return LoanApprovalResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): LoanApprovalResponse
    {
        $request->validate();

        $isSuccess = $this->customerLoanService->approve($request->getLoanAccountId(), $request->getAdminId());
        return new LoanApprovalResponse($isSuccess);
    }
}