<?php

namespace App\Application\Api\Admin\Handlers;

use App\Application\Api\Admin\Request\GetLoanDetailRequest;
use App\Application\Api\Client\Response\MyLoanResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\CustomerLoan\CustomerLoanService;
use App\Core\Domain\Repositories\AdminSecurityRepository;

class LoanDetailHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerLoanService $service, AdminSecurityRepository $repository)
    {
        parent::__construct($repository);
        $this->customerLoanService = $service;
    }

    /**
     * @param GetLoanDetailRequest $request
     * @param array $params
     * @return MyLoanResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): MyLoanResponse
    {
        $request->validate();

        $customerLoan = $this->customerLoanService->fetchOne($request->getLoanAccountId());
        return new MyLoanResponse($customerLoan);
    }
}