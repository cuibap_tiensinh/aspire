<?php

namespace App\Application\Api\Admin\Handlers;

interface HandlerAction
{
    public function doAction($request, $params = []);
}