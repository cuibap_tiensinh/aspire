<?php

namespace App\Application\Api\Admin\Handlers;

abstract class HandlerBase implements HandlerAction
{
    public function handle($request, $params = [])
    {
        return $this->doAction($request, $params);
    }
}