<?php

namespace App\Application\Api\Admin\Handlers;

use App\Application\Api\Admin\Request\AdminLoginRequest;
use App\Application\Api\Admin\Response\AdminLoginResponse;
use App\Common\Exception\BusinessException;
use App\Core\Application\Admin\AdminService;
use App\Core\Application\Admin\AdminServiceImpl;
use App\Core\Application\Auth\AuthService;
use App\Infrastructure\Mysql\MysqlAdminSecurityRepository;

class AdminLoginHandler extends HandlerBase
{
    private $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * @param AdminLoginRequest $request
     * @param array $params
     * @return AdminLoginResponse
     * @throws BusinessException
     */
    public function doAction($request, $params = []): AdminLoginResponse
    {
        $request->validate();

        $accessToken = $this->adminService->login($request);
        return new AdminLoginResponse($accessToken);
    }
}