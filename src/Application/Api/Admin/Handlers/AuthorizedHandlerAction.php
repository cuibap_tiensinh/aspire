<?php

namespace App\Application\Api\Admin\Handlers;

interface AuthorizedHandlerAction
{
    public function authorizeAction($request, $params = []);
}