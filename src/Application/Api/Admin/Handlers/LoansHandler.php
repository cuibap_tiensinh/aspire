<?php

namespace App\Application\Api\Admin\Handlers;

use App\Application\Api\Admin\Request\GetLoansRequest;
use App\Application\Api\Admin\Response\GetLoansResponse;
use App\Core\Application\CustomerLoan\CustomerLoanService;
use App\Core\Domain\Repositories\AdminSecurityRepository;

class LoansHandler extends AuthorizedHandlerBase
{
    private $customerLoanService;

    public function __construct(CustomerLoanService $service, AdminSecurityRepository $repository)
    {
        parent::__construct($repository);
        $this->customerLoanService = $service;
    }

    /**
     * @param GetLoansRequest $request
     * @param array $params
     * @return GetLoansResponse
     */
    public function doAction($request, $params = []): GetLoansResponse
    {
        $request->validate();

        $filter = $this->buildFilter($request);

        $pagingObj = $this->customerLoanService->list($filter, $request->getPage(), $request->getSize(), "");
        return new GetLoansResponse($pagingObj->getPaging(), $pagingObj->getLoans());
    }

    private function buildFilter(GetLoansRequest $request): array
    {
        $filter = [];
        if (!empty($request->getStatus())) {
            $filter['status'] = $request->getStatus();
        }

        return $filter;
    }
}