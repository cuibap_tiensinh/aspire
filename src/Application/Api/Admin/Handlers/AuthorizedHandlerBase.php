<?php

namespace App\Application\Api\Admin\Handlers;

use App\Application\Api\Admin\Request\RequestBase;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Repositories\AdminSecurityRepository;
use App\Infrastructure\Mysql\MysqlAdminSecurityRepository;

abstract class AuthorizedHandlerBase extends HandlerBase implements AuthorizedHandlerAction
{
    protected $adminSecurityRepository;
    public function __construct(AdminSecurityRepository $repository)
    {
        $this->adminSecurityRepository = $repository;
    }

    /**
     * @param RequestBase $request
     * @param array $params
     * @throws BusinessException
     */
    public function authorizeAction($request, $params = [])
    {
        if (empty($request->getAdminId()) || empty($request->getAccessTokenData()) || !is_array($request->getAccessTokenData())) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }

        $accessTokenData = $request->getAccessTokenData();
        if (empty($accessTokenData['token'])) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }

        $adminSecurity = $this->adminSecurityRepository->fetchOne($request->getAdminId());
        if (empty($adminSecurity)) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }

        if ($accessTokenData['token'] != $adminSecurity->getAccountToken()) {
            throw new BusinessException(ErrorMessage::UNAUTHORIZED, ErrorCode::UNAUTHORIZED);
        }
    }

    /**
     * @param RequestBase $request
     * @param array $params
     * @return mixed
     * @throws BusinessException
     */
    public function handle($request, $params = [])
    {
        $this->authorizeAction($request, $params);
        return $this->doAction($request, $params);
    }
}