<?php

namespace App\Core\Application\CustomerBankAccount;


use App\Application\Api\Client\Request\DepositBankAccountRequest;
use App\Application\Api\Client\Request\GetBankAccountsRequest;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\ValueObjects\CustomerBankAccountsWithPagingObj;

interface CustomerBankAccountService
{
    public function list(array $filter, int $page, int $limit, string $order): CustomerBankAccountsWithPagingObj;
    public function isExistedAtLeastOne(string $customerId): bool;
    public function register(CustomerBankAccount $model): CustomerBankAccount;
    public function deposit(string $customerId, string $accountId, float $reqAmount, string $reqCurrency, string $reqReason): CustomerBankAccount;

    public function fetchOneAndCustomer(string $accountId, string $customerId, bool $forUpdate = false): ?CustomerBankAccount;
    public function fetchOneByCustomer(string $customerId, bool $forUpdate = false): ?CustomerBankAccount;
    public function withdraw(CustomerBankAccount $bankAccount, float $amount, string $currency, string $reason): bool;
}