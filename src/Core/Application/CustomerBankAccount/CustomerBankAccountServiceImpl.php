<?php


namespace App\Core\Application\CustomerBankAccount;


use App\Application\Api\Client\Request\DepositBankAccountRequest;
use App\Application\Api\Client\Request\GetBankAccountsRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Application\CustomerBankAccount\Processor\DepositBankAccountProcessor;
use App\Core\Application\CustomerBankAccount\Processor\ListBankAccountProcessor;
use App\Core\Application\CustomerBankAccount\Processor\WithdrawBankAccountProcessor;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Repositories\CustomerBalanceHistoryRepository;
use App\Core\Domain\Repositories\CustomerBankAccountRepository;
use App\Core\Domain\ValueObjects\CustomerBankAccountsWithPagingObj;

class CustomerBankAccountServiceImpl implements CustomerBankAccountService
{
    private $bankAccountRepository;
    private $balanceHistoryRepository;
    private $listBankAccountProcessor;
    private $depositBankAccountProcessor;
    private $withdrawBankAccountProcessor;

    public function __construct(
        CustomerBankAccountRepository $bankAccountRepository,
        CustomerBalanceHistoryRepository $balanceHistoryRepository,
        CurrencyRateService $currencyRateService)
    {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->balanceHistoryRepository = $balanceHistoryRepository;

        $this->listBankAccountProcessor = new ListBankAccountProcessor($bankAccountRepository);
        $this->depositBankAccountProcessor = new DepositBankAccountProcessor(
            $bankAccountRepository,
            $currencyRateService,
            $balanceHistoryRepository
        );
        $this->withdrawBankAccountProcessor = new WithdrawBankAccountProcessor(
            $bankAccountRepository,
            $currencyRateService,
            $balanceHistoryRepository
        );
    }

    /**
     * @throws BusinessException
     */
    public function register(CustomerBankAccount $model): CustomerBankAccount
    {
        try {
            $this->bankAccountRepository->insert($model);
            return $this->bankAccountRepository->fetchOne($model->getId());
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @param string $accountId
     * @param float $reqAmount
     * @param string $reqCurrency
     * @param string $reqReason
     * @return CustomerBankAccount
     * @throws BusinessException
     */
    public function deposit(string $customerId, string $accountId, float $reqAmount, string $reqCurrency, string $reqReason): CustomerBankAccount
    {
        try {
            return $this->depositBankAccountProcessor->handle($customerId, $accountId, $reqAmount, $reqCurrency, $reqReason);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @return bool
     * @throws BusinessException
     */
    public function isExistedAtLeastOne(string $customerId): bool
    {
        try {
            return $this->bankAccountRepository->isExistByCustomerId($customerId);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return CustomerBankAccountsWithPagingObj
     * @throws BusinessException
     */
    public function list(array $filter, int $page, int $limit, string $order): CustomerBankAccountsWithPagingObj
    {
        try {
            return $this->listBankAccountProcessor->handle($filter, $page, $limit, $order);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $accountId
     * @param string $customerId
     * @param bool $forUpdate
     * @return CustomerBankAccount|null
     * @throws BusinessException
     */
    public function fetchOneAndCustomer(string $accountId, string $customerId, bool $forUpdate = false): ?CustomerBankAccount
    {
        try {
            $bankAccount = $this->bankAccountRepository->fetchOneAndCustomer($accountId, $customerId);
            if ($bankAccount) {
                return $bankAccount;
            }

            throw new BusinessException(ErrorMessage::BANK_ACCOUNT_NOT_FOUND,ErrorCode::BANK_ACCOUNT_NOT_FOUND);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @param bool $forUpdate
     * @return CustomerBankAccount|null
     * @throws BusinessException
     */
    public function fetchOneByCustomer(string $customerId, bool $forUpdate = false): ?CustomerBankAccount
    {
        try {
            $bankAccount = $this->bankAccountRepository->fetchOneByCustomer($customerId);
            if ($bankAccount) {
                return $bankAccount;
            }

            throw new BusinessException(ErrorMessage::BANK_ACCOUNT_NOT_FOUND,ErrorCode::BANK_ACCOUNT_NOT_FOUND);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    public function withdraw(CustomerBankAccount $bankAccount, float $amount, string $currency, string $reason): bool
    {
        try {
            $result = $this->withdrawBankAccountProcessor->handle(
                $bankAccount,
                $amount,
                $bankAccount->getCurrency(),
                $reason);

            if ($result) return true;

            throw new BusinessException(ErrorMessage::BANK_ACCOUNT_NOT_FOUND,ErrorCode::BANK_ACCOUNT_NOT_FOUND);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}