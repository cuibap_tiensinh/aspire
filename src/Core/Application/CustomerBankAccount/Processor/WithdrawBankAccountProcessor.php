<?php

namespace App\Core\Application\CustomerBankAccount\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\String\StringUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Domain\Models\CustomerBalanceHistory;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Repositories\CustomerBalanceHistoryRepository;
use App\Core\Domain\Repositories\CustomerBankAccountRepository;
use Phalcon\Db\RawValue;

class WithdrawBankAccountProcessor
{
    private $db;
    private $bankAccountRepository;
    private $currencyRateService;
    private $balanceHistoryRepository;

    public function __construct(CustomerBankAccountRepository $bankAccountRepository,
                                ?CurrencyRateService $currencyRateService,
                                ?CustomerBalanceHistoryRepository $balanceHistoryRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->currencyRateService = $currencyRateService;
        $this->balanceHistoryRepository = $balanceHistoryRepository;
        $this->db = \Phalcon\DI::getDefault()->getShared('db');
    }

    /**
     * @param CustomerBankAccount $bankAccount
     * @param float $amount
     * @param string $currency
     * @param string $reason
     * @return bool
     * @throws BusinessException
     */
    public function handle(CustomerBankAccount $bankAccount,
                           float $amount, string $currency, string $reason): bool
    {
        try {
            $this->db->begin();
            $bankAccountOldVersion = $bankAccount->getVersion();

            $rate = $this->currencyRateService->getExchangeRate($currency, $bankAccount->getCurrency());
            $realAmount = $amount * $rate;
            $bankAccount
                ->setBalance(new RawValue('balance - ' . $realAmount))
                ->setVersion(new RawValue('version + 1'));
            $this->bankAccountRepository->update($bankAccount);

            $balanceHistory = (new CustomerBalanceHistory())
                ->setId(StringUtils::getGUID())
                ->setCustomerId($bankAccount->getCustomerId())
                ->setBankAccountId($bankAccount->getId())
                ->setDepositAmount(0.0)
                ->setWithdrawAmount($realAmount)
                ->setAccountCurrency($bankAccount->getCurrency())
                ->setSubmittedDepositAmount(0.0)
                ->setSubmittedWithdrawAmount($amount)
                ->setSubmittedCurrency($currency)
                ->setExchangeRate($rate)
                ->setReason($reason)
                ->setVersion($bankAccountOldVersion);
            $this->balanceHistoryRepository->insert($balanceHistory);

            $this->db->commit();
            return true;
        } catch (BusinessException $businessException) {
            $this->db->rollback();
            throw $businessException;
        } catch (\Exception $exception) {
            $this->db->rollback();
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}