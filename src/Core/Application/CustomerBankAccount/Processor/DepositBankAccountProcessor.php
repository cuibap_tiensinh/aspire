<?php

namespace App\Core\Application\CustomerBankAccount\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\String\StringUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Domain\Models\CustomerBalanceHistory;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\Repositories\CustomerBalanceHistoryRepository;
use App\Core\Domain\Repositories\CustomerBankAccountRepository;

class DepositBankAccountProcessor
{
    private $db;
    private $bankAccountRepository;
    private $currencyRateService;
    private $balanceHistoryRepository;

    public function __construct(CustomerBankAccountRepository $bankAccountRepository,
                                CurrencyRateService $currencyRateService,
                                CustomerBalanceHistoryRepository $balanceHistoryRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->currencyRateService = $currencyRateService;
        $this->balanceHistoryRepository = $balanceHistoryRepository;
        $this->db = \Phalcon\DI::getDefault()->getShared('db');
    }

    /**
     * @param string $customerId
     * @param string $accountId
     * @param float $reqAmount
     * @param string $reqCurrency
     * @param string $reqReason
     * @return CustomerBankAccount
     * @throws BusinessException
     */
    public function handle(string $customerId, string $accountId, float $reqAmount, string $reqCurrency, string $reqReason): CustomerBankAccount
    {
        try {
            $this->db->begin();
            $bankAccount = $this->bankAccountRepository->fetchOneAndCustomer($accountId, $customerId, true);
            if (empty($bankAccount)) {
                throw new BusinessException(ErrorMessage::BANK_ACCOUNT_NOT_FOUND, ErrorCode::BANK_ACCOUNT_NOT_FOUND);
            }

            $rate = $this->currencyRateService->getExchangeRate($reqCurrency, $bankAccount->getCurrency());
            $realAmount = $reqAmount * $rate;

            $bankAccount->setBalance($bankAccount->getBalance() + $realAmount);
            $this->bankAccountRepository->update($bankAccount);
            $bankAccount = $this->bankAccountRepository->fetchOneAndCustomer($bankAccount->getId(), $bankAccount->getCustomerId());

            $balanceHistory = (new CustomerBalanceHistory())
                ->setId(StringUtils::getGUID())
                ->setCustomerId($bankAccount->getCustomerId())
                ->setBankAccountId($bankAccount->getId())
                ->setDepositAmount($realAmount)
                ->setWithdrawAmount(0.0)
                ->setAccountCurrency($bankAccount->getCurrency())
                ->setSubmittedDepositAmount($reqAmount)
                ->setSubmittedWithdrawAmount(0.0)
                ->setSubmittedCurrency($reqCurrency)
                ->setExchangeRate($rate)
                ->setReason($reqReason)
                ->setVersion($bankAccount->getVersion());
            $this->balanceHistoryRepository->insert($balanceHistory);
            $this->db->commit();

            return $this->bankAccountRepository->fetchOne($accountId);
        } catch (BusinessException $businessException) {
            $this->db->rollback();
            throw $businessException;
        } catch (\Exception $exception) {
            $this->db->rollback();
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}