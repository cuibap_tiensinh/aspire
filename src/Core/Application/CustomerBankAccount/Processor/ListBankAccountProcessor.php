<?php

namespace App\Core\Application\CustomerBankAccount\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Repositories\CustomerBankAccountRepository;
use App\Core\Domain\ValueObjects\CustomerBankAccountsWithPagingObj;
use App\Core\Domain\ValueObjects\Paging;

class ListBankAccountProcessor
{
    private $bankAccountRepository;

    public function __construct(CustomerBankAccountRepository $bankAccountRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return CustomerBankAccountsWithPagingObj
     * @throws BusinessException
     */
    public function handle(array $filter, int $page, int $limit, string $order): CustomerBankAccountsWithPagingObj
    {
        try {
            $offset = $limit * ($page - 1);

            $totalItem = $this->bankAccountRepository->countByFilter($filter);
            $items = $this->bankAccountRepository->fetchAllByFilter($filter, $limit, $offset);

            $totalPage = (int)ceil( $totalItem / (float)$limit);
            $paging = new Paging($totalPage, $totalItem, $limit, $page);
            return new CustomerBankAccountsWithPagingObj($paging, $items);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}