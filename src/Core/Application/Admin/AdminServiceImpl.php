<?php

namespace App\Core\Application\Admin;

use App\Application\Api\Admin\Request\AdminLoginRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Application\Auth\AuthService;
use App\Core\Application\Admin\Processor\LoginProcessor;
use App\Core\Domain\Repositories\AdminSecurityRepository;

class AdminServiceImpl implements AdminService
{
    private $authService;
    private $loginProcessor;

    public function __construct(AuthService $authService,
                                AdminSecurityRepository $adminSecurityRepository)
    {
        $this->authService = $authService;
        $this->loginProcessor = new LoginProcessor($authService, $adminSecurityRepository);
    }

    /**
     * @throws BusinessException
     */
    public function login(AdminLoginRequest $request): string
    {
        try {
            $adminSecurity = $this->loginProcessor->handle($request);
            return $this->authService->encodeAccessTokenData($adminSecurity->getAccessTokenData());
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}