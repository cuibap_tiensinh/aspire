<?php

namespace App\Core\Application\Admin;

use App\Application\Api\Admin\Request\AdminLoginRequest;

interface AdminService
{
    public function login(AdminLoginRequest $request): string;
}