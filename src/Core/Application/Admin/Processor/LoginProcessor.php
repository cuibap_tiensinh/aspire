<?php

namespace App\Core\Application\Admin\Processor;

use App\Application\Api\Admin\Request\AdminLoginRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Application\Auth\AuthService;
use App\Core\Domain\Models\AdminSecurity;
use App\Core\Domain\Repositories\AdminSecurityRepository;

class LoginProcessor
{
    private $authService;
    private $adminSecurityRepository;

    public function __construct(AuthService $authService,
                                AdminSecurityRepository $adminSecurityRepository)
    {
        $this->authService = $authService;
        $this->adminSecurityRepository = $adminSecurityRepository;
    }

    /**
     * @param AdminLoginRequest $request
     * @return AdminSecurity
     * @throws BusinessException
     */
    public function handle(AdminLoginRequest $request): AdminSecurity
    {
        try {
            $existedAdminSecurity = $this->adminSecurityRepository->fetchOneByUsername($request->getUsername());
            if (empty($existedAdminSecurity)) {
                throw new BusinessException(ErrorMessage::CUSTOMER_PHONE_IS_NOT_EXIST, ErrorCode::CUSTOMER_PHONE_IS_NOT_EXIST);
            }

            if (!$this->authService->validatePassword($request->getPassword(), $existedAdminSecurity->getPassword())) {
                throw new BusinessException(ErrorMessage::CUSTOMER_PASSWORD_NOT_MATCH, ErrorCode::CUSTOMER_PASSWORD_NOT_MATCH);
            }

            return $existedAdminSecurity;
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}