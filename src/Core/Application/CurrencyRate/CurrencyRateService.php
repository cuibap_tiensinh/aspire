<?php

namespace App\Core\Application\CurrencyRate;

class CurrencyRateService
{
    private $rateTable;

    public function __construct()
    {
        $this->rateTable = [
            "EUR/USD" => 1.0923,
            "USD/EUR" => 0.9153,
            "GBP/USD" => 1.2703,
            "USD/GBP" => 0.7870,
            "USD/CAD" => 1.3398,
            "CAD/USD" => 0.7461,
            "USD/CHF" => 0.8520,
            "CHF/USD" => 1.1734,
            "USD/JPY" => 144.29,
            "JPY/USD" => 0.0069,
            "EUR/GBP" => 0.8595,
            "GBP/EUR" => 1.1627,
            "EUR/CHF" => 0.9309,
            "CHF/EUR" => 1.0739,
            "AUD/USD" => 0.6681,
            "USD/AUD" => 1.4964,
            "EUR/JPY" => 157.68,
            "JPY/EUR" => 0.0063,
            "GBP/JPY" => 183.36,
            "JPY/GBP" => 0.0054,
        ];
    }

    public function getExchangeRate(string $srcCurrency, string $destCurrency): float
    {
        if ($srcCurrency == $destCurrency) return 1.0;

        $key = $srcCurrency . "/" . $destCurrency;
        return $this->rateTable[$key] ?? 1.0;
    }
}