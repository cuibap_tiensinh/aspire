<?php

namespace App\Core\Application\CustomerLoan\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\DateTimeUtils;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountService;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Repositories\CustomerLoanRepository;
use App\Core\Domain\ValueObjects\LoanRegistrationData;
use App\Core\Domain\ValueObjects\LoanStatus;

class LoanRegistrationProcessor
{
    private $db;
    private $customerLoanRepository;
    private $customerBankAccountService;

    public function __construct(CustomerLoanRepository $customerLoanRepository,
                                CustomerBankAccountService $customerBankAccountService)
    {
        $this->customerBankAccountService = $customerBankAccountService;
        $this->customerLoanRepository = $customerLoanRepository;
        $this->db = \Phalcon\DI::getDefault()->getShared('db');
    }

    /**
     * @param LoanRegistrationData $registrationData
     * @return CustomerLoan
     * @throws BusinessException
     */
    public function handle(LoanRegistrationData $registrationData): CustomerLoan
    {
        try {
            $this->db->begin();
            // if the customer does not have bank account, we will register one for him/her
            if (!$this->customerBankAccountService->isExistedAtLeastOne($registrationData->getCustomerId())) {
                $customerBankAccount = (new CustomerBankAccount())
                    ->setId("")
                    ->setCustomerId($registrationData->getCustomerId())
                    ->setBalance(0)
                    ->setCurrency($registrationData->getCurrency())
                    ->setVersion(1)
                    ->setIsPrimary(true);
                $customerBankAccount->generateAccountId();
                $this->customerBankAccountService->register($customerBankAccount);
            }

            $today = DateTimeUtils::today();
            $customerLoan = (new CustomerLoan())
                ->setId("")
                ->setCustomerId($registrationData->getCustomerId())
                ->setBalance($registrationData->getAmount())
                ->setCurrency($registrationData->getCurrency())
                ->setLoanTerm($registrationData->getLoanTerm())
                ->setStatus(LoanStatus::PENDING)
                ->setApprovedBy("System")
                ->setVersion(1)
                ->setIsPrimary(false)
                ->setStartedAt($today)
                ->setEndedAt($today)
                ->setScheduledAt($today)
                ->setRepaidAmount(0.0)
                ->setRepaidCount(0);
            $customerLoan->generateAccountId();
            $this->customerLoanRepository->insert($customerLoan);
            $this->db->commit();

            return $this->customerLoanRepository->fetchOne($customerLoan->getId());
        } catch (BusinessException $businessException) {
            $this->db->rollback();
            throw $businessException;
        } catch (\Exception $exception) {
            $this->db->rollback();
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}