<?php

namespace App\Core\Application\CustomerLoan\Processor;

use App\Application\Api\Client\Request\GetLoansRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Repositories\CustomerLoanRepository;
use App\Core\Domain\ValueObjects\CustomerLoansWithPagingObj;
use App\Core\Domain\ValueObjects\Paging;

class ListLoanProcessor
{
    private $customerLoanRepository;

    public function __construct(CustomerLoanRepository $customerLoanRepository)
    {
        $this->customerLoanRepository = $customerLoanRepository;
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return CustomerLoansWithPagingObj
     * @throws BusinessException
     */
    public function handle(array $filter, int $page, int $limit, string $order): CustomerLoansWithPagingObj
    {
        try {
            $offset = $limit * ($page - 1);

            $totalItem = $this->customerLoanRepository->countByFilter($filter);
            $items = $this->customerLoanRepository->fetchAllByFilter($filter, $limit, $offset);

            $totalPage = (int)ceil( $totalItem / (float)$limit);
            $paging = new Paging($totalPage, $totalItem, $limit, $page);
            return new CustomerLoansWithPagingObj($paging, $items);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}