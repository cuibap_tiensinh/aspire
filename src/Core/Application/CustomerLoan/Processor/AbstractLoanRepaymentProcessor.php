<?php

namespace App\Core\Application\CustomerLoan\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\Repositories\CustomerLoanRepository;
use App\Core\Domain\Repositories\CustomerScheduledRepaymentRepository;

abstract class AbstractLoanRepaymentProcessor
{
    protected $customerLoanRepository;
    protected $scheduledRepaymentRepository;

    public function __construct(CustomerLoanRepository $customerLoanRepository,
                                CustomerScheduledRepaymentRepository $scheduledRepaymentRepository)
    {
        $this->customerLoanRepository = $customerLoanRepository;
        $this->scheduledRepaymentRepository = $scheduledRepaymentRepository;
    }

    /**
     * @param string $id
     * @param string $customerId
     * @return CustomerLoan
     * @throws BusinessException
     */
    protected function getAndValidateCustomerLoan(string $id, string $customerId): CustomerLoan
    {
        $loan = $this->customerLoanRepository->fetchOneAndCustomer($id, $customerId);
        if (!$loan) {
            throw new BusinessException(ErrorMessage::LOAN_NOT_FOUND,ErrorCode::LOAN_NOT_FOUND);
        }

        if (!$loan->isApproved()) {
            throw new BusinessException(ErrorMessage::LOAN_IS_NOT_APPROVED,ErrorCode::LOAN_IS_NOT_APPROVED);
        }

        return $loan;
    }

    /**
     * @param string $loanId
     * @param string $customerId
     * @return CustomerScheduledRepayment
     * @throws BusinessException
     */
    protected function getAndValidateScheduledRepayment(string $loanId, string $customerId, float $realAmount): CustomerScheduledRepayment
    {
        $scheduleRepayment = $this->scheduledRepaymentRepository->fetchFirstUnpaidSchedule($loanId, $customerId, true);
        if (!$scheduleRepayment) {
            throw new BusinessException(ErrorMessage::SCHEDULED_REPAYMENT_NOT_EXIST,ErrorCode::SCHEDULED_REPAYMENT_NOT_EXIST);
        }

        if ($scheduleRepayment->getDueAmount() <= 0) {
            throw new BusinessException(ErrorMessage::YOU_REPAID_LOAN,ErrorCode::YOU_REPAID_LOAN);
        }

        if ($scheduleRepayment->getDueAmount() > $realAmount) {
            throw new BusinessException(ErrorMessage::REPAID_AMOUNT_IS_LESS_THAN_DEBT,ErrorCode::REPAID_AMOUNT_IS_LESS_THAN_DEBT);
        }

        return $scheduleRepayment;
    }
}