<?php

namespace App\Core\Application\CustomerLoan\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\NumberUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountService;
use App\Core\Domain\Models\CustomerRepaymentHistory;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\Repositories\CustomerLoanRepository;
use App\Core\Domain\Repositories\CustomerRepaymentHistoryRepository;
use App\Core\Domain\Repositories\CustomerScheduledRepaymentRepository;
use App\Core\Domain\ValueObjects\LoanRepaymentData;
use App\Core\Domain\ValueObjects\RepaymentStatus;
use App\Core\Domain\ValueObjects\RepaymentType;
use Phalcon\Db\RawValue;

class LoanRepaymentByCashInOfflineProcessor extends AbstractLoanRepaymentProcessor
{
    private $db;
    private $repaymentHistoryRepository;
    private $bankAccountService;
    private $currencyRateService;

    public function __construct(CustomerLoanRepository $customerLoanRepository,
                                CustomerScheduledRepaymentRepository $scheduledRepaymentRepository,
                                CustomerRepaymentHistoryRepository $repaymentHistoryRepository,
                                CustomerBankAccountService $customerBankAccountService,
                                CurrencyRateService $currencyRateService)
    {
        parent::__construct(
            $customerLoanRepository,
            $scheduledRepaymentRepository
        );

        $this->customerLoanRepository = $customerLoanRepository;
        $this->scheduledRepaymentRepository = $scheduledRepaymentRepository;
        $this->repaymentHistoryRepository = $repaymentHistoryRepository;
        $this->bankAccountService = $customerBankAccountService;
        $this->currencyRateService = $currencyRateService;

        $this->db = \Phalcon\DI::getDefault()->getShared('db');
    }

    /**
     * @param LoanRepaymentData $repaymentData
     * @return CustomerScheduledRepayment
     * @throws BusinessException
     */
    public function handle(LoanRepaymentData $repaymentData): CustomerScheduledRepayment
    {
        try {
            $this->db->begin();

            $loan = $this->getAndValidateCustomerLoan($repaymentData->getLoanAccountId(), $repaymentData->getCustomerId());

            $repaymentRate = $this->currencyRateService->getExchangeRate($repaymentData->getReqCurrency(), $loan->getCurrency());
            $realAmount = $repaymentData->getReqAmount() * $repaymentRate;
            $excessiveAmount = 0.0;

            $scheduleRepayment = $this->getAndValidateScheduledRepayment($repaymentData->getLoanAccountId(), $repaymentData->getCustomerId(), $realAmount);
            if ($scheduleRepayment->getDueAmount() < $realAmount) {
                $excessiveAmount = $realAmount - $scheduleRepayment->getDueAmount();
                $realAmount = $scheduleRepayment->getDueAmount();
            }

            $realAmount = NumberUtils::truncate($realAmount);

            // repay the loan
            $scheduleRepayment
                ->setDueAmount(new RawValue("due_amount - " . $realAmount))
                ->setVersion(new RawValue("version + 1"))
                ->setStatus(RepaymentStatus::PAID)
                ->setRepaidType(RepaymentType::TYPE_CASH_IN);
            $this->scheduledRepaymentRepository->update($scheduleRepayment);
            $scheduleRepayment = $this->scheduledRepaymentRepository->fetchOne($scheduleRepayment->getId());

            // update repaid amount of the loan
            $loan->setRepaidAmount(new RawValue("repaid_amount + " . $realAmount));
            $loan->setRepaidAmount(new RawValue("repaid_count + 1"));
            $this->customerLoanRepository->update($loan);

            if ($excessiveAmount > 0) {
                // deposit bank account
                $bankAccount = $this->bankAccountService->fetchOneByCustomer($repaymentData->getCustomerId(), true);
                if (!$bankAccount) {
                    throw new BusinessException(ErrorMessage::BANK_ACCOUNT_NOT_FOUND,ErrorCode::BANK_ACCOUNT_NOT_FOUND);
                }

                $this->bankAccountService->deposit(
                    $repaymentData->getCustomerId(),
                    $bankAccount->getId(),
                    $excessiveAmount,
                    $loan->getCurrency(),
                    "Deposit excessive money after repay the loan: " . $loan->getId()
                );
            }

            // store history
            $repaymentHistory = (new CustomerRepaymentHistory())
                ->setBankAccountId("CASH000000000000000000")
                ->setLoanAccountId($loan->getId())
                ->setCustomerId($loan->getCustomerId())
                ->setVersion($scheduleRepayment->getVersion() - 1.0)
                ->setRepaidAmount($realAmount)
                ->setCurrency($loan->getCurrency())
                ->setRepaidType(RepaymentType::TYPE_CASH_IN)
                ->setRemainingDueAmountInPeriod($scheduleRepayment->getDueAmount());
            $repaymentHistory->generateHistoryIdIfEmpty();
            $this->repaymentHistoryRepository->insert($repaymentHistory);

            $this->db->commit();

            return $scheduleRepayment;
        } catch (BusinessException $businessException) {
            $this->db->rollback();
            throw $businessException;
        } catch (\Exception $exception) {
            $this->db->rollback();
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

}