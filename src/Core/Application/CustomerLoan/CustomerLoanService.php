<?php

namespace App\Core\Application\CustomerLoan;

use App\Application\Api\Admin\Request\LoanApprovalRequest;
use App\Application\Api\Client\Request\GetLoanDetailRequest;
use App\Application\Api\Admin\Request\GetLoanDetailRequest as AdminGetLoanDetailRequest;
use App\Application\Api\Client\Request\GetLoansRequest;
use App\Application\Api\Admin\Request\GetLoansRequest as AdminGetLoansRequest;
use App\Application\Api\Client\Request\RegisterLoanRequest;
use App\Application\Api\Client\Request\RepayLoanByCashInOfflineRequest;
use App\Application\Api\Client\Request\RepayLoanByTransferRequest;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\ValueObjects\CustomerLoansWithPagingObj;
use App\Core\Domain\ValueObjects\LoanRegistrationData;
use App\Core\Domain\ValueObjects\LoanRepaymentData;

interface CustomerLoanService
{
    public function list(array $filter, int $page, int $limit, string $order): CustomerLoansWithPagingObj;
    public function fetchOneAndCustomer(string $id, string $customerId): ?CustomerLoan;
    public function fetchOne(string $id): ?CustomerLoan;
    public function register(LoanRegistrationData $registrationData): CustomerLoan;
    public function repayByTransferMoney(LoanRepaymentData $repaymentData): CustomerScheduledRepayment;
    public function repayByCashIn(LoanRepaymentData $repaymentData): CustomerScheduledRepayment;
    public function approve(string $accountId, string $adminId): bool;
}