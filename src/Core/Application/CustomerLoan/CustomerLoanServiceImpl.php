<?php

namespace App\Core\Application\CustomerLoan;

use App\Application\Api\Admin\Request\LoanApprovalRequest;
use App\Application\Api\Client\Request\GetLoanDetailRequest;
use App\Application\Api\Admin\Request\GetLoanDetailRequest as AdminGetLoanDetailRequest;
use App\Application\Api\Client\Request\GetLoansRequest;
use App\Application\Api\Admin\Request\GetLoansRequest as AdminGetLoansRequest;
use App\Application\Api\Client\Request\RegisterLoanRequest;
use App\Application\Api\Client\Request\RepayLoanByTransferRequest;
use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\DateTimeUtils;
use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Application\Customer\CustomerService;
use App\Core\Application\CustomerBankAccount\CustomerBankAccountService;
use App\Core\Application\CustomerLoan\Processor\ListLoanForAdminProcessor;
use App\Core\Application\CustomerLoan\Processor\LoanRegistrationProcessor;
use App\Core\Application\CustomerLoan\Processor\ListLoanProcessor;
use App\Core\Application\CustomerLoan\Processor\LoanRepaymentByCashInOfflineProcessor;
use App\Core\Application\CustomerLoan\Processor\LoanRepaymentByTransferProcessor;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\Repositories\CustomerLoanRepository;
use App\Core\Domain\Repositories\CustomerRepaymentHistoryRepository;
use App\Core\Domain\Repositories\CustomerScheduledRepaymentRepository;
use App\Core\Domain\ValueObjects\CustomerLoansWithPagingObj;
use App\Core\Domain\ValueObjects\LoanRegistrationData;
use App\Core\Domain\ValueObjects\LoanRepaymentData;
use App\Core\Domain\ValueObjects\LoanStatus;
use DateInterval;

class CustomerLoanServiceImpl implements CustomerLoanService
{
    private $customerLoanRepository;
    private $loanRegistrationProcessor;
    private $loansProcessor;
    private $loanRepaymentByTransferProcessor;
    private $loanRepaymentByCashInOfflineProcessor;

    public function __construct(CustomerService $customerService,
                                CustomerBankAccountService $bankAccountService,
                                CustomerLoanRepository $customerLoanRepository,
                                CustomerScheduledRepaymentRepository $scheduledRepaymentRepository,
                                CustomerRepaymentHistoryRepository $repaymentHistoryRepository)
    {
        $this->customerLoanRepository = $customerLoanRepository;
        $this->loanRegistrationProcessor = new LoanRegistrationProcessor($customerLoanRepository, $bankAccountService);
        $this->loansProcessor = new ListLoanProcessor($customerLoanRepository);

        $this->loanRepaymentByTransferProcessor = new LoanRepaymentByTransferProcessor(
            $customerLoanRepository,
            $scheduledRepaymentRepository,
            $repaymentHistoryRepository,
            $bankAccountService,
            new CurrencyRateService()
        );

        $this->loanRepaymentByCashInOfflineProcessor = new LoanRepaymentByCashInOfflineProcessor(
            $customerLoanRepository,
            $scheduledRepaymentRepository,
            $repaymentHistoryRepository,
            $bankAccountService,
            new CurrencyRateService()
        );
    }

    /**
     * @param LoanRegistrationData $registrationData
     * @return CustomerLoan
     * @throws BusinessException
     */
    public function register(LoanRegistrationData $registrationData): CustomerLoan
    {
        try {
            $isExistedPendingLoan = $this->customerLoanRepository->isExistedPendingLoanByCustomer($registrationData->getCustomerId());
            if ($isExistedPendingLoan) {
                throw new BusinessException(ErrorMessage::YOU_HAVE_UNAPPROVED_LOAN,ErrorCode::YOU_HAVE_UNAPPROVED_LOAN);
            }

            return $this->loanRegistrationProcessor->handle($registrationData);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return CustomerLoansWithPagingObj
     * @throws BusinessException
     */
    public function list(array $filter, int $page, int $limit, string $order): CustomerLoansWithPagingObj
    {
        try {
            return $this->loansProcessor->handle($filter, $page, $limit, $order);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $id
     * @param string $customerId
     * @return CustomerLoan|null
     * @throws BusinessException
     */
    public function fetchOneAndCustomer(string $id, string $customerId): ?CustomerLoan
    {
        try {
            $loan = $this->customerLoanRepository->fetchOneAndCustomer($id, $customerId);
            if ($loan) {
                return $loan;
            }

            throw new BusinessException(ErrorMessage::LOAN_NOT_FOUND,ErrorCode::LOAN_NOT_FOUND);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $id
     * @return CustomerLoan|null
     * @throws BusinessException
     */
    public function fetchOne(string $id): ?CustomerLoan
    {
        try {
            $loan = $this->customerLoanRepository->fetchOne($id);
            if ($loan) {
                return $loan;
            }

            throw new BusinessException(ErrorMessage::LOAN_NOT_FOUND,ErrorCode::LOAN_NOT_FOUND);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $accountId
     * @param string $adminId
     * @return bool
     * @throws BusinessException
     */
    public function approve(string $accountId, string $adminId): bool
    {
        try {
            $loan = $this->customerLoanRepository->fetchOne($accountId);
            if (!$loan) {
                throw new BusinessException(ErrorMessage::LOAN_NOT_FOUND,ErrorCode::LOAN_NOT_FOUND);
            }

            if ($loan->isApproved()) {
                throw new BusinessException(ErrorMessage::LOAN_IS_APPROVED,ErrorCode::LOAN_IS_APPROVED);
            }

            $loan->setStatus(LoanStatus::APPROVED)
                ->setApprovedBy($adminId)
                ->setStartedAt(DateTimeUtils::today())
                ->setEndedAt(
                    DateTimeUtils::today()->add(
                        DateInterval::createFromDateString($loan->getLoanTerm() . ' week')
                    ))
                ->setScheduledAt(DateTimeUtils::today());
            $this->customerLoanRepository->update($loan);
            return true;
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param LoanRepaymentData $repaymentData
     * @return CustomerScheduledRepayment
     * @throws BusinessException
     */
    public function repayByTransferMoney(LoanRepaymentData $repaymentData): CustomerScheduledRepayment
    {
        try {
            return $this->loanRepaymentByTransferProcessor->handle($repaymentData);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param LoanRepaymentData $repaymentData
     * @return CustomerScheduledRepayment
     * @throws BusinessException
     */
    public function repayByCashIn(LoanRepaymentData $repaymentData): CustomerScheduledRepayment
    {
        try {
            return $this->loanRepaymentByCashInOfflineProcessor->handle($repaymentData);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}