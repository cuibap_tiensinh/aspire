<?php

namespace App\Core\Application\Customer;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Application\Auth\AuthService;
use App\Core\Application\Customer\Processor\LoginProcessor;
use App\Core\Application\Customer\Processor\RegistrationProcessor;
use App\Core\Domain\Models\Customer;
use App\Core\Domain\Models\CustomerSecurity;
use App\Core\Domain\Repositories\CustomerRepository;
use App\Core\Domain\Repositories\CustomerSecurityRepository;
use App\Core\Domain\ValueObjects\AccountRegistrationData;
use App\Core\Domain\ValueObjects\CustomerWithExtraObj;

class CustomerServiceImpl implements CustomerService
{
    private $authService;
    private $customerRepository;
    private $customerSecurityRepository;

    private $registrationProcessor;
    private $loginProcessor;

    public function __construct(AuthService $authService,
                                CustomerRepository $customerRepository,
                                CustomerSecurityRepository $customerSecurityRepository)
    {
        $this->authService = $authService;
        $this->customerRepository = $customerRepository;
        $this->customerSecurityRepository = $customerSecurityRepository;
        $this->registrationProcessor = new RegistrationProcessor($authService, $customerRepository, $customerSecurityRepository);
        $this->loginProcessor = new LoginProcessor($authService, $customerRepository, $customerSecurityRepository);
    }

    /**
     * @param AccountRegistrationData $registrationData
     * @return CustomerWithExtraObj
     * @throws BusinessException
     */
    public function register(AccountRegistrationData $registrationData): CustomerWithExtraObj
    {
        try {
            list($customer, $customerSecurity) = $this->registrationProcessor->handle($registrationData);
            $accessToken = $this->authService->encodeAccessTokenData($customerSecurity->getAccessTokenData());

            return new CustomerWithExtraObj($accessToken, $customer);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @throws BusinessException
     */
    public function login(string $username, string $password): CustomerWithExtraObj
    {
        try {
            list($customer, $customerSecurity) = $this->loginProcessor->handle($username, $password);
            $accessToken = $this->authService->encodeAccessTokenData($customerSecurity->getAccessTokenData());

            return new CustomerWithExtraObj($accessToken, $customer);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @return Customer
     * @throws BusinessException
     */
    public function showMyAccount(string $customerId): Customer
    {
        try {
            $customer = $this->customerRepository->fetchOne($customerId);
            if (!empty($customer)) {
                return $customer;
            }
            throw new BusinessException(ErrorMessage::CUSTOMER_NOT_FOUND,ErrorCode::CUSTOMER_NOT_FOUND);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $customerId
     * @return bool
     * @throws BusinessException
     */
    public function isExisted(string $customerId): bool
    {
        try {
            return $this->customerRepository->isExisted($customerId);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * @param string $id
     * @return CustomerSecurity|null
     * @throws BusinessException
     */
    public function getCustomerSecurity(string $id): ?CustomerSecurity
    {
        try {
            return $this->customerSecurityRepository->fetchOne($id);
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR, ErrorCode::UNKNOWN_ERROR);
        }
    }
}