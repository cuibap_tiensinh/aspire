<?php

namespace App\Core\Application\Customer\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Common\Utils\String\StringUtils;
use App\Core\Application\Auth\AuthService;
use App\Core\Domain\Models\Customer;
use App\Core\Domain\Models\CustomerSecurity;
use App\Core\Domain\Repositories\CustomerRepository;
use App\Core\Domain\Repositories\CustomerSecurityRepository;
use App\Core\Domain\ValueObjects\AccountRegistrationData;

class RegistrationProcessor
{
    private $db;
    private $authService;
    private $customerRepository;
    private $customerSecurityRepository;

    public function __construct(AuthService $authService,
                                CustomerRepository $customerRepository,
                                CustomerSecurityRepository $customerSecurityRepository)
    {
        $this->authService = $authService;
        $this->customerRepository = $customerRepository;
        $this->customerSecurityRepository = $customerSecurityRepository;
        $this->db = \Phalcon\DI::getDefault()->getShared('db');
    }

    /**
     * @param AccountRegistrationData $registeredAccountData
     * @return array
     * @throws BusinessException
     */
    public function handle(AccountRegistrationData $registeredAccountData): array
    {
        try {
            $this->db->begin();
            $username = $this->buildUsernameByPhone($registeredAccountData->getPhone());
            $existedCustomerSecurity = $this->customerSecurityRepository->fetchOneByUsername($username);
            if ($existedCustomerSecurity) {
                throw new BusinessException(ErrorMessage::CUSTOMER_PHONE_IS_EXIST,ErrorCode::CUSTOMER_PHONE_IS_EXIST);
            }

            $customerSecurity = (new CustomerSecurity())
                ->setId($username)
                ->setUsername($username)
                ->setPassword($this->authService->hashPassword($registeredAccountData->getPassword()));
            $customerSecurity->buildAccountTokenIfEmpty();
            $this->customerSecurityRepository->insert($customerSecurity);
            $customerSecurity = $this->customerSecurityRepository->fetchOne($customerSecurity->getId());

            $customer = (new Customer())
                ->setId($customerSecurity->getId())
                ->setCustomerName($registeredAccountData->getName())
                ->setCustomerEmail($registeredAccountData->getEmail())
                ->setCustomerPhone($registeredAccountData->getPhone());
            $this->customerRepository->insert($customer);
            $this->db->commit();

            $customer = $this->customerRepository->fetchOne($customer->getId());
            return [$customer, $customerSecurity];
        } catch (BusinessException $businessException) {
            $this->db->rollback();
            throw $businessException;
        } catch (\Exception $exception) {
            $this->db->rollback();
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }

    /**
     * The phone with format (+1) 123456789 will be formatted to 0001123456789.
     * Split the phone into 2 parts: country code and phone number
     * Replace non-digit letter(s) of country code and trim all space(s) of phone number
     * Concat country code and phone number
     *
     * @param $phone
     * @return string
     */
    private function buildUsernameByPhone($phone): string
    {
        list($countryCode, $phoneNumber) = explode(")", $phone);
        $countryCode = preg_replace('/[^0-9]/', '', $countryCode);
        return str_pad($countryCode,  4, "0", STR_PAD_LEFT) . StringUtils::trimMultipleSpaces($phoneNumber);
    }
}