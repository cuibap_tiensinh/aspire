<?php

namespace App\Core\Application\Customer\Processor;

use App\Common\Exception\BusinessException;
use App\Common\Response\ErrorCode;
use App\Common\Response\ErrorMessage;
use App\Core\Application\Auth\AuthService;
use App\Core\Domain\Repositories\CustomerRepository;
use App\Core\Domain\Repositories\CustomerSecurityRepository;

class LoginProcessor
{
    private $authService;
    private $customerRepository;
    private $customerSecurityRepository;

    public function __construct(AuthService $authService,
                                CustomerRepository $customerRepository,
                                CustomerSecurityRepository $customerSecurityRepository)
    {
        $this->authService = $authService;
        $this->customerRepository = $customerRepository;
        $this->customerSecurityRepository = $customerSecurityRepository;
    }

    /**
     * @param string $username
     * @param string $password
     * @return array
     * @throws BusinessException
     */
    public function handle(string $username, string $password): array
    {
        try {
            $customerSecurity = $this->customerSecurityRepository->fetchOneByUsername($username);
            if (empty($customerSecurity)) {
                throw new BusinessException(ErrorMessage::CUSTOMER_PHONE_IS_NOT_EXIST, ErrorCode::CUSTOMER_PHONE_IS_NOT_EXIST);
            }

            if (!$this->authService->validatePassword($password, $customerSecurity->getPassword())) {
                throw new BusinessException(ErrorMessage::CUSTOMER_PASSWORD_NOT_MATCH, ErrorCode::CUSTOMER_PASSWORD_NOT_MATCH);
            }

            $customer = $this->customerRepository->fetchOne($customerSecurity->getId());
            if (empty($customer)) {
                throw new BusinessException(ErrorMessage::CUSTOMER_NOT_FOUND,ErrorCode::CUSTOMER_NOT_FOUND);
            }

            return [$customer, $customerSecurity];
        } catch (BusinessException $businessException) {
            throw $businessException;
        } catch (\Exception $exception) {
            throw new BusinessException(ErrorMessage::INTERNAL_ERROR,ErrorCode::UNKNOWN_ERROR);
        }
    }
}