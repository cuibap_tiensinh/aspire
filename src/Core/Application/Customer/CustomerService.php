<?php

namespace App\Core\Application\Customer;

use App\Application\Api\Client\Request\LoginAccountRequest;
use App\Application\Api\Client\Request\RegisterAccountRequest;
use App\Application\Api\Client\Request\ShowMyAccountRequest;
use App\Core\Domain\Models\Customer;
use App\Core\Domain\Models\CustomerSecurity;
use App\Core\Domain\ValueObjects\AccountRegistrationData;
use App\Core\Domain\ValueObjects\CustomerWithExtraObj;

interface CustomerService
{
    public function isExisted(string $customerId): bool;
    public function register(AccountRegistrationData $registrationData): CustomerWithExtraObj;
    public function login(string $username, string $password): CustomerWithExtraObj;
    public function showMyAccount(string $customerId): Customer;
    public function getCustomerSecurity(string $id): ?CustomerSecurity;
}