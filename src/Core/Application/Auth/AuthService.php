<?php

namespace App\Core\Application\Auth;

interface AuthService
{
    public function decodeAccessToken($accessToken, array $allowed_algs = ['HS256']): array;
    public function encodeAccessTokenData($accessTokenData, $alg = 'HS256'): string;
    public function validatePassword($clientPassword, $dbPassword): bool;
    public function hashPassword($password): string;
    public function isValidAccessToken(array $accessTokenData): bool;
}