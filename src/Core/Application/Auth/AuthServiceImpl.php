<?php

namespace App\Core\Application\Auth;

use Exception;
use Firebase\JWT\JWT;
use Phalcon\Security;

class AuthServiceImpl implements AuthService
{
    private $security;
    private $secretKey;

    public function __construct(string $secretKey)
    {
        $this->secretKey = $secretKey;
        $di = \Phalcon\DI::getDefault();

        try {
            $this->security = $di->getShared('security');
        } catch (Exception $e) {
            $this->security = new Security();

            // Set the password hashing factor to 12 rounds
            $this->security->setWorkFactor(12);
        }
    }

    public function decodeAccessToken($accessToken, array $allowed_algs = ['HS256']): array
    {
        if (empty($accessToken)) {
            return [];
        }

        $accessTokenData = JWT::decode($accessToken, $this->secretKey, $allowed_algs);
        if (is_object($accessTokenData)) {
            $accessTokenData = (array)$accessTokenData;
        }

        return $accessTokenData;
    }

    public function encodeAccessTokenData($accessTokenData, $alg = 'HS256'): string
    {
        return JWT::encode($accessTokenData, $this->secretKey, $alg);
    }

    public function validatePassword($clientPassword, $dbPassword): bool
    {
        return $this->security->checkHash($clientPassword, $dbPassword);
    }

    public function hashPassword($password): string
    {
        return $this->security->hash($password);
    }

    public function isValidAccessToken(array $accessTokenData): bool
    {
        if (empty($accessTokenData['id'])) {
            return false;
        }

        if (empty($accessTokenData['token'])) {
            return false;
        }

        if (empty($accessTokenData['expired_at']) || $accessTokenData['expired_at'] < time()) {
            return false;
        }

        return true;
    }
}