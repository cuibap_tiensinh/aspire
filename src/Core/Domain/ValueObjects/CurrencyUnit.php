<?php

namespace App\Core\Domain\ValueObjects;

class CurrencyUnit
{
    const USD = "USD";
    const EUR = "EUR";
    const GBP = "GBP";
    const CAD = "CAD";
    const CHF = "CHF";
    const JPY = "JPY";
}