<?php

namespace App\Core\Domain\ValueObjects;

class CustomerLoansWithPagingObj
{
    private $paging;
    private $loans;

    public function __construct(Paging $paging, array $customerLoans)
    {
        $this->paging = $paging;
        $this->loans = $customerLoans;
    }

    /**
     * @return Paging
     */
    public function getPaging(): Paging
    {
        return $this->paging;
    }

    /**
     * @return array
     */
    public function getLoans(): array
    {
        return $this->loans;
    }
}