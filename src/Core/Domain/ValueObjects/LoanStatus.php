<?php

namespace App\Core\Domain\ValueObjects;

class LoanStatus
{
    const APPROVED = "APPROVED";
    const PENDING = "PENDING";
    const PAID = "PAID";
}