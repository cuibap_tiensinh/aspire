<?php

namespace App\Core\Domain\ValueObjects;

use App\Core\Domain\Models\Customer;

class CustomerWithExtraObj
{
    private $access_token;
    private $customer;

    public function __construct(string $access_token, Customer $customer)
    {
        $this->access_token = $access_token;
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }
}