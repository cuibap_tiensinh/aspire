<?php


namespace App\Core\Domain\ValueObjects;


class LoanRepaymentData
{
    private $customerId;
    private $loanAccountId;
    private $bankAccountId;
    private $reqCurrency;
    private $reqAmount;

    public function __construct(string $customerId,
                                string $loanAccountId,
                                string $bankAccountId,
                                float $reqAmount,
                                string $reqCurrency)
    {
        $this->customerId = $customerId;
        $this->loanAccountId = $loanAccountId;
        $this->bankAccountId = $bankAccountId;
        $this->reqAmount = $reqAmount;
        $this->reqCurrency = $reqCurrency;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loanAccountId;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bankAccountId;
    }

    /**
     * @return string
     */
    public function getReqCurrency(): string
    {
        return $this->reqCurrency;
    }

    /**
     * @return float
     */
    public function getReqAmount(): float
    {
        return $this->reqAmount;
    }
}