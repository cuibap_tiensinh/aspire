<?php

namespace App\Core\Domain\ValueObjects;

class CustomerBankAccountsWithPagingObj
{
    private $paging;
    private $bankAccounts;

    public function __construct(Paging $paging, array $customerLoans)
    {
        $this->paging = $paging;
        $this->bankAccounts = $customerLoans;
    }

    /**
     * @return Paging
     */
    public function getPaging(): Paging
    {
        return $this->paging;
    }

    /**
     * @return array
     */
    public function getBankAccounts(): array
    {
        return $this->bankAccounts;
    }
}