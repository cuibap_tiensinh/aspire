<?php

namespace App\Core\Domain\ValueObjects;

class RepaymentType
{
    const TYPE_TRANSFER = "TRANSFER";
    const TYPE_CASH_IN = "CASH_IN";
    const TYPE_NONE = "NONE";
}