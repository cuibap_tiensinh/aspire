<?php

namespace App\Core\Domain\ValueObjects;

class LoanRegistrationData
{
    private $customerId;
    private $amount;
    private $currency;
    private $loanTerm;

    public function __construct(string $customerId, float $amount, string $currency, int $loanTerm)
    {
        $this->customerId = $customerId;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->loanTerm = $loanTerm;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getLoanTerm(): int
    {
        return $this->loanTerm;
    }
}