<?php

namespace App\Core\Domain\ValueObjects;

class Paging implements \JsonSerializable
{
    private $current_page;
    private $size;
    private $total_item;
    private $total_page;

    public function __construct(int $total_page, int $total_item, int $size, int $current_page)
    {
        $this->total_item = $total_item;
        $this->total_page = $total_page;
        $this->size = $size;
        $this->current_page = $current_page;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->current_page;
    }

    /**
     * @param int $current_page
     */
    public function setCurrentPage(int $current_page): void
    {
        $this->current_page = $current_page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getTotalItem(): int
    {
        return $this->total_item;
    }

    /**
     * @param int $total_item
     */
    public function setTotalItem(int $total_item): void
    {
        $this->total_item = $total_item;
    }

    /**
     * @return int
     */
    public function getTotalPage(): int
    {
        return $this->total_page;
    }

    /**
     * @param int $total_page
     */
    public function setTotalPage(int $total_page): void
    {
        $this->total_page = $total_page;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}