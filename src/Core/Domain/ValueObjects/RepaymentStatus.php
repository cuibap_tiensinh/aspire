<?php

namespace App\Core\Domain\ValueObjects;

class RepaymentStatus
{
    const PENDING = "PENDING";
    const PAYING = "PAYING";
    const PAID = "PAID";
}