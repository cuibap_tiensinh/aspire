<?php

namespace App\Core\Domain\Repositories;

use App\Core\Domain\Models\Customer;

interface CustomerRepository
{
    public function insert(Customer $model): bool;
    public function update(Customer $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?Customer;
    public function isExisted(string $id): bool;
}