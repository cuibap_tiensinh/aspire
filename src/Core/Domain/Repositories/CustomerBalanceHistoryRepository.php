<?php

namespace App\Core\Domain\Repositories;

use App\Core\Domain\Models\CustomerBalanceHistory;

interface CustomerBalanceHistoryRepository
{
    public function insert(CustomerBalanceHistory $model): bool;
    public function update(CustomerBalanceHistory $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerBalanceHistory;

}