<?php

namespace App\Core\Domain\Repositories;

use App\Core\Domain\Models\CustomerRepaymentHistory;

interface CustomerRepaymentHistoryRepository
{
    public function insert(CustomerRepaymentHistory $model): bool;
    public function update(CustomerRepaymentHistory $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerRepaymentHistory;
}