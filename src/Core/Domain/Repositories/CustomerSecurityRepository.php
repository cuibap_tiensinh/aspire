<?php

namespace App\Core\Domain\Repositories;

use App\Core\Domain\Models\CustomerSecurity;

interface CustomerSecurityRepository
{
    public function insert(CustomerSecurity $model): bool;
    public function update(CustomerSecurity $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerSecurity;
    public function fetchOneByUsername(string $username): ?CustomerSecurity;
}