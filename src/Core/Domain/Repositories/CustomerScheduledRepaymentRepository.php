<?php


namespace App\Core\Domain\Repositories;


use App\Core\Domain\Models\CustomerScheduledRepayment;

interface CustomerScheduledRepaymentRepository
{
    public function insert(CustomerScheduledRepayment $model): bool;
    public function update(CustomerScheduledRepayment $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerScheduledRepayment;

    /**
     * @param CustomerScheduledRepayment[] $models
     * @return mixed
     */
    public function insertMany(array $models): bool;
    public function fetchFirstUnpaidSchedule(string $loanAccountId, string $customerId, bool $forUpdate = false): ?CustomerScheduledRepayment;
}