<?php

namespace App\Core\Domain\Repositories;

use App\Core\Domain\Models\AdminSecurity;

interface AdminSecurityRepository
{
    public function insert(AdminSecurity $model): bool;
    public function update(AdminSecurity $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?AdminSecurity;
    public function fetchOneByUsername(string $username): ?AdminSecurity;

}