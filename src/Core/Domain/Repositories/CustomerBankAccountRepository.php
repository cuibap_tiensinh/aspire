<?php


namespace App\Core\Domain\Repositories;


use App\Core\Domain\Models\CustomerBankAccount;

interface CustomerBankAccountRepository
{
    public function insert(CustomerBankAccount $model): bool;
    public function update(CustomerBankAccount $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerBankAccount;

    public function fetchOneByCustomer(string $customerId, bool $forUpdate = false): ?CustomerBankAccount;
    public function fetchOneAndCustomer(string $id, string $customerId, bool $forUpdate = false): ?CustomerBankAccount;
    public function isExistByCustomerId(string $customerId): bool;
    public function countByFilter(array $filter): int;
    public function fetchAllByFilter(array $filter, int $limit, int $offset): array;
}