<?php


namespace App\Core\Domain\Repositories;


use App\Core\Domain\Models\CustomerLoan;

interface CustomerLoanRepository
{
    public function insert(CustomerLoan $model): bool;
    public function update(CustomerLoan $model): bool;
    public function fetchOne(string $id, bool $forUpdate = false): ?CustomerLoan;

    public function fetchOneAndCustomer(string $id, string $customerId): ?CustomerLoan;
    public function fetchAllByCustomer(string $customerId): array;
    public function isExistedPendingLoanByCustomer(string $customerId): bool;
    public function countByFilter(array $filter): int;
    public function fetchAllByFilter(array $filter, int $limit, int $offset): array;
}