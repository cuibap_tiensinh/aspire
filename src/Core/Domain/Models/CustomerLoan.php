<?php

namespace App\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Common\Utils\NumberUtils;
use App\Core\Domain\ValueObjects\LoanStatus;
use DateTime;

/**
 * Class CustomerLoan
 *
 * @package App\Core\Domain\Models;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerLoan extends CustomerBankAccount
{
    private $loan_term;
    private $status;
    private $approved_by;
    private $started_at;
    private $ended_at;
    private $scheduled_at;
    private $repaid_count;
    private $repaid_amount;

    public function __construct()
    {
        parent::__construct();
        $this->loan_term = 0;
        $this->status = "";
        $this->approved_by = "";
        $this->repaid_count = 0;
        $this->repaid_amount = 0.0;
    }

    /**
     * @return int
     */
    public function getLoanTerm(): int
    {
        return $this->loan_term;
    }

    /**
     * @param int $loan_term
     * @return CustomerLoan
     */
    public function setLoanTerm(int $loan_term): CustomerLoan
    {
        $this->loan_term = $loan_term;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return CustomerLoan
     */
    public function setStatus(string $status): CustomerLoan
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getApprovedBy(): string
    {
        return $this->approved_by;
    }

    /**
     * @param string $approved_by
     * @return CustomerLoan
     */
    public function setApprovedBy(string $approved_by): CustomerLoan
    {
        $this->approved_by = $approved_by;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getStartedAt(): ?DateTime
    {
        return $this->started_at;
    }

    /**
     * @param DateTime|null $started_at
     * @return CustomerLoan
     */
    public function setStartedAt(?DateTime $started_at): CustomerLoan
    {
        $this->started_at = $started_at;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getEndedAt(): ?DateTime
    {
        return $this->ended_at;
    }

    /**
     * @param DateTime|null $ended_at
     * @return CustomerLoan
     */
    public function setEndedAt(?DateTime $ended_at): CustomerLoan
    {
        $this->ended_at = $ended_at;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getScheduledAt(): ?DateTime
    {
        return $this->scheduled_at;
    }

    /**
     * @param DateTime|null $scheduled_at
     * @return CustomerLoan
     */
    public function setScheduledAt(?DateTime $scheduled_at): CustomerLoan
    {
        $this->scheduled_at = $scheduled_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepaidCount()
    {
        return $this->repaid_count;
    }

    /**
     * @param mixed $repaid_count
     * @return CustomerLoan
     */
    public function setRepaidCount($repaid_count): CustomerLoan
    {
        $this->repaid_count = $repaid_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepaidAmount()
    {
        return $this->repaid_amount;
    }

    /**
     * @param mixed $repaid_amount
     * @return CustomerLoan
     */
    public function setRepaidAmount($repaid_amount): CustomerLoan
    {
        $this->repaid_amount = $repaid_amount;
        if (is_numeric($this->repaid_amount)) {
            $this->repaid_amount = NumberUtils::truncate($this->repaid_amount);
        }
        return $this;
    }

    public function isApproved(): bool
    {
        return $this->status != LoanStatus::PENDING;
    }

    public function jsonSerialize()
    {
        $array = array_merge(parent::jsonSerialize(), get_object_vars($this));
        if ($array['started_at'] instanceof DateTime) {
            $array['started_at'] = $array['started_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if ($array['ended_at'] instanceof DateTime) {
            $array['ended_at'] = $array['ended_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if ($array['scheduled_at'] instanceof DateTime) {
            $array['scheduled_at'] = $array['scheduled_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if (is_numeric($array['repaid_count'])) {
            $array['repaid_count'] = intval($array['repaid_count']);
        }
        if (is_numeric($array['repaid_amount'])) {
            $array['repaid_amount'] = floatval($array['repaid_amount']);
        }

        unset($array['version']);
        unset($array['is_primary']);
        return $array;
    }
}