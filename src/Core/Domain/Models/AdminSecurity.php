<?php

namespace App\Core\Domain\Models;

class AdminSecurity extends ModelBase
{
    const SESSION_EXPIRED_TIME = 86400 * 30;

    private $id;
    private $username;
    private $password;
    private $account_token;

    public function __construct()
    {
        $this->id = "";
        $this->username = "";
        $this->password = "";
        $this->account_token = "";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return (string)$this->id;
    }

    /**
     * @param string $id
     * @return AdminSecurity
     */
    public function setId(string $id): AdminSecurity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    /**
     * @param string $username
     * @return AdminSecurity
     */
    public function setUsername(string $username): AdminSecurity
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @param string $password
     * @return AdminSecurity
     */
    public function setPassword(string $password): AdminSecurity
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountToken(): string
    {
        return (string)$this->account_token;
    }

    /**
     * @param string $account_token
     * @return AdminSecurity
     */
    public function setAccountToken(string $account_token): AdminSecurity
    {
        $this->account_token = $account_token;
        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function buildAccountTokenIfEmpty()
    {
        if (empty($this->account_token)) {
            $this->account_token = md5($this->username . "|" . $this->password);
        }
    }

    public function getAccessTokenData(): array
    {
        return [
            'id' => $this->getId(),
            'token' => $this->getAccountToken(),
            'expired_at' => time() + self::SESSION_EXPIRED_TIME,
        ];
    }
}