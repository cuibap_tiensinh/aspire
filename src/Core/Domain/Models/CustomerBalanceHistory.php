<?php

namespace App\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Common\Utils\NumberUtils;
use DateTime;

/**
 * Class CustomerBalanceHistory
 *
 * @package App\Core\Domain\Models;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerBalanceHistory extends ModelBase
{
    private $id;
    // The Id of the customer. This field identifies who is the owner the bank account balance
    private $customer_id;
    private $bank_account_id;
    private $deposit_amount;
    private $withdraw_amount;
    private $account_currency;
    private $submitted_deposit_amount;
    private $submitted_withdraw_amount;
    private $submitted_currency;
    private $exchange_rate;
    private $reason;
    private $version;
    private $created_at;

    public function __construct()
    {
        $this->id = "";
        $this->customer_id = "";
        $this->bank_account_id = "";
        $this->deposit_amount = 0.0;
        $this->withdraw_amount = 0.0;
        $this->account_currency = "";
        $this->submitted_deposit_amount = 0.0;
        $this->submitted_withdraw_amount = 0.0;
        $this->submitted_currency = "";
        $this->exchange_rate = 1.0;
        $this->reason = "";
        $this->version = 0;
        $this->created_at = null;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return (string)$this->id;
    }

    /**
     * @param string $id
     * @return CustomerBalanceHistory
     */
    public function setId(string $id): CustomerBalanceHistory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return (string)$this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerBalanceHistory
     */
    public function setCustomerId(string $customer_id): CustomerBalanceHistory
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return (string)$this->bank_account_id;
    }

    /**
     * @param string $bank_account_id
     * @return CustomerBalanceHistory
     */
    public function setBankAccountId(string $bank_account_id): CustomerBalanceHistory
    {
        $this->bank_account_id = $bank_account_id;
        return $this;
    }

    /**
     * @return float
     */
    public function getDepositAmount(): float
    {
        return $this->deposit_amount;
    }

    /**
     * @param float $deposit_amount
     * @return CustomerBalanceHistory
     */
    public function setDepositAmount(float $deposit_amount): CustomerBalanceHistory
    {
        $this->deposit_amount = NumberUtils::truncate($deposit_amount);
        return $this;
    }

    /**
     * @return float
     */
    public function getWithdrawAmount(): float
    {
        return $this->withdraw_amount;
    }

    /**
     * @param float $withdraw_amount
     * @return CustomerBalanceHistory
     */
    public function setWithdrawAmount(float $withdraw_amount): CustomerBalanceHistory
    {
        $this->withdraw_amount = NumberUtils::truncate($withdraw_amount);
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountCurrency(): string
    {
        return (string)$this->account_currency;
    }

    /**
     * @param string $account_currency
     * @return CustomerBalanceHistory
     */
    public function setAccountCurrency(string $account_currency): CustomerBalanceHistory
    {
        $this->account_currency = $account_currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubmittedDepositAmount(): float
    {
        return $this->submitted_deposit_amount;
    }

    /**
     * @param float $submitted_deposit_amount
     * @return CustomerBalanceHistory
     */
    public function setSubmittedDepositAmount(float $submitted_deposit_amount): CustomerBalanceHistory
    {
        $this->submitted_deposit_amount = NumberUtils::truncate($submitted_deposit_amount);
        return $this;
    }

    /**
     * @return float
     */
    public function getSubmittedWithdrawAmount(): float
    {
        return $this->submitted_withdraw_amount;
    }

    /**
     * @param float $submitted_withdraw_amount
     * @return CustomerBalanceHistory
     */
    public function setSubmittedWithdrawAmount(float $submitted_withdraw_amount): CustomerBalanceHistory
    {
        $this->submitted_withdraw_amount = NumberUtils::truncate($submitted_withdraw_amount);
        return $this;
    }

    /**
     * @return string
     */
    public function getSubmittedCurrency(): string
    {
        return (string)$this->submitted_currency;
    }

    /**
     * @param string $submitted_currency
     * @return CustomerBalanceHistory
     */
    public function setSubmittedCurrency(string $submitted_currency): CustomerBalanceHistory
    {
        $this->submitted_currency = $submitted_currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->exchange_rate;
    }

    /**
     * @param float $exchange_rate
     * @return CustomerBalanceHistory
     */
    public function setExchangeRate(float $exchange_rate): CustomerBalanceHistory
    {
        $this->exchange_rate = $exchange_rate;
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return CustomerBalanceHistory
     */
    public function setVersion(int $version): CustomerBalanceHistory
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return (string)$this->reason;
    }

    /**
     * @param string $reason
     * @return CustomerBalanceHistory
     */
    public function setReason(string $reason): CustomerBalanceHistory
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerBalanceHistory
     */
    public function setCreatedAt(?DateTime $created_at): CustomerBalanceHistory
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function jsonSerialize()
    {
        $array = get_object_vars($this);
        if ($array['created_at'] instanceof DateTime) {
            $array['created_at'] = $array['created_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        return $array;
    }
}