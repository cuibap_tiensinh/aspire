<?php

namespace App\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Common\Utils\NumberUtils;
use App\Core\Domain\ValueObjects\RepaymentType;
use DateTime;

/**
 * Class CustomerScheduledRepayment
 *
 * @package App\Core\Domain\Models;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerScheduledRepayment extends ModelBase
{
    private $id;
    // The Id of the customer. This field identifies who do repayment
    private $customer_id;
    // This field identifies which the loan the customer repays for
    private $loan_account_id;
    private $due_amount;
    private $currency;
    private $due_at;
    private $status;
    private $version;
    private $repaid_type;
    private $created_at;
    private $updated_at;

    public function __construct()
    {
        $this->id = "";
        $this->customer_id = "";
        $this->loan_account_id = "";
        $this->due_amount = 0.0;
        $this->currency = "";
        $this->status = "";
        $this->version = 0;
        $this->repaid_type = RepaymentType::TYPE_NONE;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerScheduledRepayment
     */
    public function setId(string $id): CustomerScheduledRepayment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerScheduledRepayment
     */
    public function setCustomerId(string $customer_id): CustomerScheduledRepayment
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loan_account_id;
    }

    /**
     * @param string $loan_account_id
     * @return CustomerScheduledRepayment
     */
    public function setLoanAccountId(string $loan_account_id): CustomerScheduledRepayment
    {
        $this->loan_account_id = $loan_account_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDueAmount()
    {
        return $this->due_amount;
    }

    /**
     * @param mixed $due_amount
     * @return CustomerScheduledRepayment
     */
    public function setDueAmount($due_amount): CustomerScheduledRepayment
    {
        $this->due_amount = $due_amount;
        if (is_numeric($this->due_amount)) {
            $this->due_amount = NumberUtils::truncate($this->due_amount);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerScheduledRepayment
     */
    public function setCurrency(string $currency): CustomerScheduledRepayment
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDueAt(): ?DateTime
    {
        return $this->due_at;
    }

    /**
     * @param DateTime|null $due_at
     * @return CustomerScheduledRepayment
     */
    public function setDueAt(?DateTime $due_at): CustomerScheduledRepayment
    {
        $this->due_at = $due_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return CustomerScheduledRepayment
     */
    public function setStatus(string $status): CustomerScheduledRepayment
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return CustomerScheduledRepayment
     */
    public function setVersion($version): CustomerScheduledRepayment
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepaidType(): string
    {
        return $this->repaid_type;
    }

    /**
     * @param string $repaid_type
     * @return CustomerScheduledRepayment
     */
    public function setRepaidType(string $repaid_type): CustomerScheduledRepayment
    {
        $this->repaid_type = $repaid_type;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerScheduledRepayment
     */
    public function setCreatedAt(?DateTime $created_at): CustomerScheduledRepayment
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updated_at;
    }

    /**
     * @param DateTime|null $updated_at
     * @return CustomerScheduledRepayment
     */
    public function setUpdatedAt(?DateTime $updated_at): CustomerScheduledRepayment
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function generateScheduleIdIfEmpty(int $idx)
    {
        if (empty($this->id)) {
            $this->id = $this->getLoanAccountId() .
                "-" . $this->getDueAt()->format("Ymd") .
                "-" . ($idx + 1);
        }
    }

    public function jsonSerialize()
    {
        $array = get_object_vars($this);
        if ($array['created_at'] instanceof DateTime) {
            $array['created_at'] = $array['created_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if ($array['updated_at'] instanceof DateTime) {
            $array['updated_at'] = $array['updated_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if ($array['due_at'] instanceof DateTime) {
            $array['due_at'] = $array['due_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if (is_numeric($array['due_amount'])) {
            $array['due_amount'] = floatval($array['due_amount']);
        }
        return $array;
    }
}