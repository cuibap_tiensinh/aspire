<?php

namespace App\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Common\Utils\NumberUtils;
use App\Core\Domain\ValueObjects\RepaymentType;
use App\Infrastructure\Mysql\Record\CustomerScheduledRepaymentRecord;
use DateTime;

/**
 * Class CustomerRepaymentHistory
 *
 * @package App\Core\Domain\Models;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerRepaymentHistory extends ModelBase
{
    private $id;
    // The Id of the customer. This field identifies who do repayment
    private $customer_id;
    private $bank_account_id;
    private $loan_account_id;
    private $repaid_amount;
    private $currency;
    private $remaining_due_amount_in_period;
    private $version;
    private $repaid_type;
    private $created_at;

    public function __construct()
    {
        $this->id = "";
        $this->customer_id = "";
        $this->bank_account_id = "";
        $this->loan_account_id = "";
        $this->repaid_amount = 0.0;
        $this->currency = "";
        $this->remaining_due_amount_in_period = 0.0;
        $this->version = 0;
        $this->repaid_type = RepaymentType::TYPE_NONE;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CustomerRepaymentHistory
     */
    public function setId(string $id): CustomerRepaymentHistory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerRepaymentHistory
     */
    public function setCustomerId(string $customer_id): CustomerRepaymentHistory
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountId(): string
    {
        return $this->bank_account_id;
    }

    /**
     * @param string $bank_account_id
     * @return CustomerRepaymentHistory
     */
    public function setBankAccountId(string $bank_account_id): CustomerRepaymentHistory
    {
        $this->bank_account_id = $bank_account_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoanAccountId(): string
    {
        return $this->loan_account_id;
    }

    /**
     * @param string $loan_account_id
     * @return CustomerRepaymentHistory
     */
    public function setLoanAccountId(string $loan_account_id): CustomerRepaymentHistory
    {
        $this->loan_account_id = $loan_account_id;
        return $this;
    }

    /**
     * @return float
     */
    public function getRepaidAmount(): float
    {
        return $this->repaid_amount;
    }

    /**
     * @param float $repaid_amount
     * @return CustomerRepaymentHistory
     */
    public function setRepaidAmount(float $repaid_amount): CustomerRepaymentHistory
    {
        $this->repaid_amount = NumberUtils::truncate($repaid_amount);
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerRepaymentHistory
     */
    public function setCurrency(string $currency): CustomerRepaymentHistory
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getRemainingDueAmountInPeriod(): float
    {
        return $this->remaining_due_amount_in_period;
    }

    /**
     * @param float $remaining_due_amount_in_period
     * @return CustomerRepaymentHistory
     */
    public function setRemainingDueAmountInPeriod(float $remaining_due_amount_in_period): CustomerRepaymentHistory
    {
        $this->remaining_due_amount_in_period = NumberUtils::truncate($remaining_due_amount_in_period);
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return CustomerRepaymentHistory
     */
    public function setVersion(int $version): CustomerRepaymentHistory
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepaidType(): string
    {
        return $this->repaid_type;
    }

    /**
     * @param string $repaid_type
     * @return CustomerRepaymentHistory
     */
    public function setRepaidType(string $repaid_type): CustomerRepaymentHistory
    {
        $this->repaid_type = $repaid_type;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerRepaymentHistory
     */
    public function setCreatedAt(?DateTime $created_at): CustomerRepaymentHistory
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function generateHistoryIdIfEmpty()
    {
        if (empty($this->id)) {
            $this->id = $this->loan_account_id . '-' . $this->version;
        }
    }

    public function jsonSerialize()
    {
        $array = get_object_vars($this);
        if ($array['created_at'] instanceof DateTime) {
            $array['created_at'] = $array['created_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        return $array;
    }
}