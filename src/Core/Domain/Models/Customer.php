<?php

namespace App\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use DateTime;

/**
 * Class Customer
 *
 * @package App\Core\Domain\Models;
 * @author thang.dang <dangquocthang0101@gmail.com>
 *
 */
class Customer extends ModelBase
{
    private $id;
    private $customer_name;
    private $customer_phone;
    private $customer_email;
    private $created_at;
    private $updated_at;

    public function __construct()
    {
        $this->id = "";
        $this->customer_name = "";
        $this->customer_email = "";
        $this->customer_phone = "";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return (string)$this->id;
    }

    /**
     * @param string $id
     * @return Customer
     */
    public function setId(string $id): Customer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName(): string
    {
        return (string)$this->customer_name;
    }

    /**
     * @param string $customer_name
     * @return Customer
     */
    public function setCustomerName(string $customer_name): Customer
    {
        $this->customer_name = $customer_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerPhone(): string
    {
        return (string)$this->customer_phone;
    }

    /**
     * @param string $customer_phone
     * @return Customer
     */
    public function setCustomerPhone(string $customer_phone): Customer
    {
        $this->customer_phone = $customer_phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail(): string
    {
        return (string)$this->customer_email;
    }

    /**
     * @param string $customer_email
     * @return Customer
     */
    public function setCustomerEmail(string $customer_email): Customer
    {
        $this->customer_email = $customer_email;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return Customer
     */
    public function setCreatedAt(?DateTime $created_at): Customer
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updated_at;
    }

    /**
     * @param DateTime|null $updated_at
     * @return Customer
     */
    public function setUpdatedAt(?DateTime $updated_at): Customer
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function jsonSerialize()
    {
        $array = get_object_vars($this);
        if ($array['created_at'] instanceof DateTime) {
            $array['created_at'] = $array['created_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if ($array['updated_at'] instanceof DateTime) {
            $array['updated_at'] = $array['updated_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        return $array;
    }
}
