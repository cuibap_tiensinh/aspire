<?php

namespace App\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Common\Utils\NumberUtils;
use DateTime;

/**
 * Class CustomerBankAccount
 *
 * @package App\Core\Domain\Models;
 * @author thang.dang <dangquocthang0101@gmail.com>
 */
class CustomerBankAccount extends ModelBase
{
    // The Id of the customer's bank account.
    // The customer can deposit money, transfer money, and so on.
    private $id;
    // The Id of the customer. This field identifies who is the owner of this bank
    private $customer_id;
    private $balance;
    private $currency;
    private $version;
    // Only the primary bank account can do loan repayment
    private $is_primary;
    private $created_at;
    private $updated_at;

    public function __construct()
    {
        $this->id = "";
        $this->customer_id = "";
        $this->balance = 0.0;
        $this->currency = "";
        $this->version = 1;
        $this->is_primary = false;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return (string)$this->id;
    }

    /**
     * @param string $id
     * @return CustomerBankAccount
     */
    public function setId(string $id): CustomerBankAccount
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customer_id;
    }

    /**
     * @param string $customer_id
     * @return CustomerBankAccount
     */
    public function setCustomerId(string $customer_id): CustomerBankAccount
    {
        $this->customer_id = $customer_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     * @return CustomerBankAccount
     */
    public function setBalance($balance): CustomerBankAccount
    {
        $this->balance = $balance;
        if (is_numeric($this->balance)) {
            $this->balance = NumberUtils::truncate($this->balance);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return CustomerBankAccount
     */
    public function setCurrency(string $currency): CustomerBankAccount
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return CustomerBankAccount
     */
    public function setVersion($version): CustomerBankAccount
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsPrimary(): bool
    {
        return $this->is_primary;
    }

    /**
     * @param bool $is_primary
     * @return CustomerBankAccount
     */
    public function setIsPrimary(bool $is_primary): CustomerBankAccount
    {
        $this->is_primary = $is_primary;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    /**
     * @param DateTime|null $created_at
     * @return CustomerBankAccount
     */
    public function setCreatedAt(?DateTime $created_at): CustomerBankAccount
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updated_at;
    }

    /**
     * @param DateTime|null $updated_at
     * @return CustomerBankAccount
     */
    public function setUpdatedAt(?DateTime $updated_at): CustomerBankAccount
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function jsonSerialize()
    {
        $array = get_object_vars($this);
        if ($array['created_at'] instanceof DateTime) {
            $array['created_at'] = $array['created_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if ($array['updated_at'] instanceof DateTime) {
            $array['updated_at'] = $array['updated_at']->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);
        }
        if (is_numeric($array['balance'])) {
            $array['balance'] = floatval($array['balance']);
        }
        return $array;
    }

    public function generateAccountId()
    {
        if (empty($this->id)) {
            $this->id = $this->customer_id .
                "-" . (floor(microtime(true) * 1000)) .
                "-" . str_pad(rand(0, 1000), 4, "0");
        }
    }
}