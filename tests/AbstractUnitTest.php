<?php

use Phalcon\Di;
use Phalcon\Config;
use Phalcon\Logger;
use Phalcon\Mvc\Model\Metadata\Files as MetaDataAdapter;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Test\Traits\UnitTestCase as UnitTestCaseTrait;
use PHPUnit\Framework\TestCase as TestCase;
use Phalcon\Di\InjectionAwareInterface;

defined('ROOT_PATH') || define('ROOT_PATH', dirname(__DIR__));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', ROOT_PATH . '/src');
defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production');

define('MODULE_NAME', 'Api');
define('MODULE_PATH', APPLICATION_PATH . '/Application/' . MODULE_NAME);
define('CONFIG_PATH', MODULE_PATH . '/Configs');

abstract class AbstractUnitTest extends TestCase implements InjectionAwareInterface
{
    use UnitTestCaseTrait;

    /**
     * @var bool
     */
    private $_loaded = false;

    public function setUp(): void
    {
        $this->setUpPhalcon();

        $di = Di::getDefault();
        $config = new Config(include CONFIG_PATH . '/app.php');

//        /**
//         * Set the default namespace for dispatcher
//         */
//        $di->set('dispatcher', function () {
//            $dispatcher = new Dispatcher();
//            $dispatcher->setDefaultNamespace('App\Application\Api\Client\Controllers');
//            return $dispatcher;
//        });

        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di->set('db', function () use ($config) {
            $db = new DbAdapter($config->mysql->toArray());

            if (APPLICATION_ENV == 'local') {

                $eventsManager = new EventsManager();

                $logger = new FileLogger($config->application->logDir . 'sql_' . date('Ymd') . '.log');

                //Listen all the database events
                $eventsManager->attach('db', function ($event, $db) use ($logger) {
                    if ($event->getType() == 'beforeQuery') {
                        $logger->log($db->getSQLStatement(), Logger::INFO);
                    }
                });

                //Assign the eventsManager to the db adapter instance
                $db->setEventsManager($eventsManager);
            }

            return $db;
        });

        $di->set(
            'modelsManager',
            function()
            {
                return new \Phalcon\Mvc\Model\Manager();
            }
        );

        /**
         * If the configuration specify the use of metadata adapter use it or use memory otherwise
         */
        if (is_dir($config->application->metadataDir)) {
            $di->set('modelsMetadata', function () use ($config) {
                return new MetaDataAdapter(array(
                    'metaDataDir' => $config->application->metadataDir
                ));
            });
        }

        $this->setDi($di);

        $this->_loaded = true;
    }

    protected function tearDown(): void
    {
        $this->tearDownPhalcon();

        parent::tearDown();
    }

    /**
     * Check if the test case is setup properly
     *
     * @throws \PHPUnit_Framework_IncompleteTestError;
     */
    public function __destruct()
    {
        if (!$this->_loaded) {
            throw new \PHPUnit_Framework_IncompleteTestError(
                "Please run parent::setUp()."
            );
        }
    }
}