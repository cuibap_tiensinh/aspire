<?php declare(strict_types=1);

namespace Tests\Unit\Common\Utils\String;

use App\Common\Utils\String\StringUtils;

class StringUtilsTest extends \AbstractUnitTest
{
    public function testGetSafeString()
    {
        $string = "<a>Lorem ips|um !@#$%^&*() do<l>or sit am#et</a>";
        $this->assertEquals("Lorem ipsum !@^&*() door sit amet", StringUtils::getSafeString($string));
        $this->assertEquals(null, StringUtils::getSafeString(null));
    }

    public function testIsValidEmail()
    {
        $validEmail = "abc.2124@xyz.com";
        $invalidEmail1 = "<adcaca>@xyz.com";
        $invalidEmail2 = "abc.2124@xyz";
        $invalidEmail3 = "abc.2124.com";
        $invalidEmail4 = "Lorem ipsum";
        $invalidEmail5 = "";
        $invalidEmail6 = null;
        $invalidEmail7 = "<a>jhoin</a>@xyz.com";

        $this->assertTrue(StringUtils::isValidEmail($validEmail));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail1));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail2));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail3));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail4));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail5));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail6));
        $this->assertFalse(StringUtils::isValidEmail($invalidEmail7));

    }

    public function testIsValidPhone()
    {
        $validPhone1 = "(+1) 12345678";
        $validPhone2 = "(+1)12345678";

        $invalidPhone1 = "+112345678";
        $invalidPhone2 = "(1)12345678";
        $invalidPhone3 = "(1)123 456 78";
        $invalidPhone4 = "112345678";
        $invalidPhone5 = "(1)123-456-78";
        $invalidPhone6 = "(+1)123 456 78";

        $this->assertTrue(StringUtils::isValidPhone($validPhone1));
        $this->assertTrue(StringUtils::isValidPhone($validPhone2));

        $this->assertFalse(StringUtils::isValidPhone($invalidPhone1));
        $this->assertFalse(StringUtils::isValidPhone($invalidPhone2));
        $this->assertFalse(StringUtils::isValidPhone($invalidPhone3));
        $this->assertFalse(StringUtils::isValidPhone($invalidPhone4));
        $this->assertFalse(StringUtils::isValidPhone($invalidPhone5));
        $this->assertFalse(StringUtils::isValidPhone($invalidPhone6));
    }

    public function testIsValidPassword()
    {
        $validPassword1 = "12345678";
        $validPassword2 = "123456789";

        $invalidPassword1 = "123456";

        $this->assertTrue(StringUtils::isValidPassword($validPassword1));
        $this->assertTrue(StringUtils::isValidPassword($validPassword2));

        $this->assertFalse(StringUtils::isValidPassword($invalidPassword1));
    }

    public function testGetGUID()
    {
        $result = StringUtils::getGUID();

        $this->assertTrue(strlen($result) == 36);
    }

    public function testTrimMultipleSpaces()
    {
        $this->assertEquals(" Lorem ipsum ", StringUtils::trimMultipleSpaces("      Lorem      ipsum  "));
        $this->assertEquals(" 99Lor9em ip*su@m ", StringUtils::trimMultipleSpaces("      99Lor9em      ip*su@m  "));
    }
}