<?php declare(strict_types=1);


namespace Tests\Unit\Common\Utils;


use App\Common\Utils\DateTimeUtils;
use DateTime;
use DateTimeZone;

class DateTimeUtilsTest extends \AbstractUnitTest
{
    public function testGetCurrentTimezone()
    {
        $result = DateTimeUtils::getCurrentTimezone();
        $expected = new DateTimeZone(date_default_timezone_get());

        $this->assertEquals($expected, $result);
    }

    public function testGetDefaultTimezone()
    {
        $result = DateTimeUtils::getDefaultTimezone();
        $expected = new DateTimeZone("GMT");

        $this->assertEquals($expected, $result);
    }

    public function testChangeTimezone()
    {
        $originDateTimeZone = new DateTimeZone("GMT");
        $originDateTime = new DateTime("2024-01-01 00:00:00", $originDateTimeZone);

        $vnDateTimeZone = new DateTimeZone("Asia/Ho_Chi_Minh");
        $vnDateTime = new DateTime("2024-01-01 07:00:00", $vnDateTimeZone);

        $this->assertEquals(null, DateTimeUtils::changeTimezone(null, null));
        $this->assertEquals(null, DateTimeUtils::changeTimezone(null, $originDateTimeZone));
        $this->assertEquals($originDateTime, DateTimeUtils::changeTimezone($originDateTime, null));
        $this->assertEquals($originDateTime, DateTimeUtils::changeTimezone($originDateTime, $originDateTimeZone));
        $this->assertEquals($vnDateTime, DateTimeUtils::changeTimezone($originDateTime, $vnDateTimeZone));
    }

    public function testConvertToDefaultTimezone()
    {
        $currentDateTimeZone = new DateTimeZone(date_default_timezone_get());
        $originDateTime = new DateTime("2024-01-01 00:00:00", $currentDateTimeZone);

        $defaultDateTimeZone = new DateTimeZone("GMT");
        $defaultDateTime = (clone $originDateTime)->setTimezone($defaultDateTimeZone);

        $this->assertEquals(null, DateTimeUtils::convertToDefaultTimezone(null));
        $this->assertEquals($defaultDateTime, DateTimeUtils::convertToDefaultTimezone($originDateTime));
    }

    public function testConvertToDefaultTimezoneAsString()
    {
        $currentDateTimeZone = new DateTimeZone(date_default_timezone_get());
        $originDateTime = new DateTime("2024-01-01 00:00:00", $currentDateTimeZone);

        $defaultDateTimeZone = new DateTimeZone("GMT");
        $defaultDateTime = (clone $originDateTime)->setTimezone($defaultDateTimeZone);

        $expected1 = $defaultDateTime->format("Y-m-d H:i:s T");
        $expected2 = $defaultDateTime->format("Y-m-d H:i:s");

        $this->assertEquals("", DateTimeUtils::convertToDefaultTimeZoneAsString(null, true));
        $this->assertEquals("", DateTimeUtils::convertToDefaultTimeZoneAsString(null, false));
        $this->assertEquals($expected1, DateTimeUtils::convertToDefaultTimeZoneAsString($originDateTime, true));
        $this->assertEquals($expected2, DateTimeUtils::convertToDefaultTimeZoneAsString($originDateTime, false));
    }

    public function testConvertToCurrentTimezone()
    {
        $defaultDateTimeZone = new DateTimeZone("GMT");
        $originDateTime = new DateTime("2024-01-01 00:00:00", $defaultDateTimeZone);

        $currentDateTimeZone = new DateTimeZone(date_default_timezone_get());
        $currentDateTime = (clone $originDateTime)->setTimezone($currentDateTimeZone);

        $this->assertEquals(null, DateTimeUtils::convertToCurrentTimezone(null));
        $this->assertEquals($currentDateTime, DateTimeUtils::convertToCurrentTimezone($originDateTime));
    }

    public function testConvertToCurrentTimezoneAsString()
    {
        $defaultDateTimeZone = new DateTimeZone("GMT");
        $originDateTime = new DateTime("2024-01-01 00:00:00", $defaultDateTimeZone);

        $currentDateTimeZone = new DateTimeZone(date_default_timezone_get());
        $currentDateTime = (clone $originDateTime)->setTimezone($currentDateTimeZone);
        $expected1 = $currentDateTime->format("Y-m-d H:i:s T");
        $expected2 = $currentDateTime->format("Y-m-d H:i:s");

        $this->assertEquals("", DateTimeUtils::convertToCurrentTimezoneAsString(null, true));
        $this->assertEquals("", DateTimeUtils::convertToCurrentTimezoneAsString(null, false));
        $this->assertEquals($expected1, DateTimeUtils::convertToCurrentTimezoneAsString($originDateTime, true));
        $this->assertEquals($expected2, DateTimeUtils::convertToCurrentTimezoneAsString($originDateTime, false));
    }

    public function testToday()
    {
        $currentDateTimeZone = new DateTimeZone(date_default_timezone_get());
        $expected1 = new DateTime("today", $currentDateTimeZone);

        $defaultDateTimeZone = new DateTimeZone("GMT");
        $expected2 = new DateTime("today", $defaultDateTimeZone);

        $this->assertEquals($expected1, DateTimeUtils::today());
        $this->assertEquals($expected2, DateTimeUtils::today($defaultDateTimeZone));
    }
}