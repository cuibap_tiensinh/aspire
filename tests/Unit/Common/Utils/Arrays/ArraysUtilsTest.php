<?php declare(strict_types=1);

namespace Tests\Unit\Common\Utils\Arrays;

use App\Common\Utils\Arrays\ArrayUtils;

class ArraysUtilsTest extends \AbstractUnitTest
{
    public function testStripTags()
    {
        $array = [
            "a" => "<a>Lorem ipsum dolor sit amet</a>",
            "b" => [
                "c" => "*7%Praesent non sem volutpat##@#"
            ]
        ];

        $expectedResult = [
            "a" => "Lorem ipsum dolor sit amet",
            "b" => [
                "c" => "*7Praesent non sem volutpat@"
            ]
        ];

        $result = ArrayUtils::stripTags($array);
        $this->assertEquals($expectedResult, $result);
    }

    public function testGetOrDefault()
    {
        $array = [
            "a" => "Lorem ipsum dolor sit amet",
            "b" => [
                "c" => "Praesent non sem volutpat"
            ]
        ];

        $this->assertEquals(
            "Lorem ipsum dolor sit amet",
            ArrayUtils::getOrDefault($array, "a"));

        $this->assertEquals(
            "Lorem ipsum dolor sit amet",
            ArrayUtils::getOrDefault($array, "a", "non sem volutpat"));

        $this->assertEquals(
            [
                "c" => "Praesent non sem volutpat"
            ],
            ArrayUtils::getOrDefault($array, "b", "non sem volutpat"));

        $this->assertEquals(
            null,
            ArrayUtils::getOrDefault($array, "c"));

        $this->assertEquals(
            null,
            ArrayUtils::getOrDefault($array, "c"));

        $this->assertEquals(
            "Lorem ipsum dolor sit amet",
            ArrayUtils::getOrDefault($array, "a", "Lorem ipsum dolor sit amet"));

        $this->assertEquals(
            null,
            ArrayUtils::getOrDefault(null, "a"));

        $this->assertEquals(
            "Lorem ipsum dolor sit amet",
            ArrayUtils::getOrDefault(null, "a", "Lorem ipsum dolor sit amet"));
    }

    public function testGetDeepValue()
    {
        $array = [
            "a" => "Lorem ipsum dolor sit amet",
            "b" => [
                "c" => "Praesent non sem volutpat",
                "z" => [
                    "xxx" => "Phasellus eu molestie ligula"
                ]
            ]
        ];

        $this->assertEquals(
            "Praesent non sem volutpat",
            ArrayUtils::getDeepValue($array, "b.c", "Lorem ipsum dolor sit amet"));

        $this->assertEquals(
            "Praesent non sem volutpat",
            ArrayUtils::getDeepValue($array, "b.c.e", "Praesent non sem volutpat"));

        $this->assertEquals(
            [
                "xxx" => "Phasellus eu molestie ligula"
            ],
            ArrayUtils::getDeepValue($array, "b.z", "Praesent non sem volutpat"));

        $this->assertEquals(
            "Phasellus eu molestie ligula",
            ArrayUtils::getDeepValue($array, "b.z.xxx", "Praesent non sem volutpat"));
    }
}