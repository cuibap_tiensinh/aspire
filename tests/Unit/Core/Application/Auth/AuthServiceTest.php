<?php declare(strict_types=1);

namespace Tests\Unit\Core\Application;

use App\Core\Application\Auth\AuthServiceImpl;
use DomainException;
use Firebase\JWT\SignatureInvalidException;
use InvalidArgumentException;
use UnexpectedValueException;

class AuthServiceTest extends \AbstractUnitTest
{
    private $key;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->key = "vvvvvvvdevvvvvvvv";
    }

    public function testEncodeAccessTokenData()
    {
        $data = [
            'id' => '1234567',
            'expired_at' => 1516239022
        ];

        $service = new AuthServiceImpl($this->key);
        $actual = $service->encodeAccessTokenData($data);
        $expect = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzQ1NjciLCJleHBpcmVkX2F0IjoxNTE2MjM5MDIyfQ.8ao0iQ-Ss601DdPlBLYQPHGtHGeA3ihO24bRyqTjPH4";

        $this->assertEquals($expect, $actual);
    }

    public function testDecodeAccessToken()
    {
        $jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzQ1NjciLCJleHBpcmVkX2F0IjoxNTE2MjM5MDIyfQ.8ao0iQ-Ss601DdPlBLYQPHGtHGeA3ihO24bRyqTjPH4";

        $service = new AuthServiceImpl($this->key);
        $actual = $service->decodeAccessToken($jwt);
        $expect = [
            'id' => '1234567',
            'expired_at' => 1516239022
        ];;

        $this->assertEquals($expect, $actual);
    }

    public function testDecodeAccessTokenWhenKeyEmpty()
    {
        $this->expectException(InvalidArgumentException::class);

        $jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzQ1NjciLCJleHBpcmVkX2F0IjoxNTE2MjM5MDIyfQ.8ao0iQ-Ss601DdPlBLYQPHGtHGeA3ihO24bRyqTjPH4";
        $service = new AuthServiceImpl("");
        $service->decodeAccessToken($jwt);
    }

    public function testDecodeAccessTokenWhenJWTEmpty()
    {
        $jwt = "";
        $service = new AuthServiceImpl("");
        $result = $service->decodeAccessToken($jwt);
        $this->assertEquals([], $result);
    }

    public function testDecodeAccessTokenWhenWrongJWT()
    {
        $this->expectException(DomainException::class);

        $jwt = "a.c.c";
        $service = new AuthServiceImpl($this->key);
        $service->decodeAccessToken($jwt);
    }

    public function testDecodeAccessTokenWhenWrongKey()
    {
        $this->expectException(SignatureInvalidException::class);

        $jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzQ1NjciLCJleHBpcmVkX2F0IjoxNTE2MjM5MDIyfQ.8ao0iQ-Ss601DdPlBLYQPHGtHGeA3ihO24bRyqTjPH4";
        $service = new AuthServiceImpl("abc");
        $service->decodeAccessToken($jwt);
    }

    public function testValidatePassword()
    {
        $orgPassword = "123456";

        $service = new AuthServiceImpl($this->key);

        $hashedPassword = $service->hashPassword($orgPassword);
        $this->assertTrue($orgPassword != $hashedPassword);
        $this->assertTrue($service->validatePassword($orgPassword, $hashedPassword));
    }

    public function testIsValidAccessToken()
    {
        $rightData = [
            'id' => '1234567',
            'token' => 'adfdsgfdhgfjh',
            'expired_at' => time()
        ];

        $service = new AuthServiceImpl($this->key);
        $actual = $service->isValidAccessToken($rightData);
        $this->assertTrue($actual);
    }

    public function testIsValidAccessTokenWhenMissingField()
    {
        $missingData1 = [
            'token' => 'adfdsgfdhgfjh',
            'expired_at' => time()
        ];

        $missingData2 = [
            'id' => '123456',
            'expired_at' => time()
        ];

        $missingData3 = [
            'id' => '123456',
            'token' => 'adfdsgfdhgfjh',
        ];

        $service = new AuthServiceImpl($this->key);
        $actual1 = $service->isValidAccessToken($missingData1);
        $this->assertFalse($actual1);

        $actual2 = $service->isValidAccessToken($missingData2);
        $this->assertFalse($actual2);

        $actual3 = $service->isValidAccessToken($missingData3);
        $this->assertFalse($actual3);
    }

    public function testIsValidAccessTokenWhenExpired()
    {
        $rightData = [
            'id' => '1234567',
            'token' => 'adfdsgfdhgfjh',
            // 365 days ago
            'expired_at' => time() - (86400 * 365)
        ];

        $service = new AuthServiceImpl($this->key);
        $actual = $service->isValidAccessToken($rightData);
        $this->assertFalse($actual);
    }
}