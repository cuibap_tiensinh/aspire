<?php declare(strict_types=1);

namespace Tests\Unit\Core\Application;

use App\Core\Application\CurrencyRate\CurrencyRateService;
use App\Core\Domain\ValueObjects\CurrencyUnit;

class CurrencyRateServiceTest extends \AbstractUnitTest
{
    public function testGetExchangeRate()
    {
        $service = new CurrencyRateService();

        $this->assertEquals(1.0, $service->getExchangeRate(CurrencyUnit::CAD, CurrencyUnit::CAD));
        $this->assertTrue(1.0 != $service->getExchangeRate(CurrencyUnit::CAD, CurrencyUnit::USD));
    }
}