<?php declare(strict_types=1);

namespace Tests\Unit\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\ValueObjects\CurrencyUnit;

class CustomerBankAccountTest extends \AbstractUnitTest
{
    public function testGenerateAccountIdWhenEmpty()
    {
        $model = (new CustomerBankAccount())
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->assertTrue(empty($model->getId()));

        $model->generateAccountId();
        $this->assertTrue(!empty($model->getId()));
    }

    public function testGenerateAccountIdWhenNotEmpty()
    {
        $id = "123456";
        $model = (new CustomerBankAccount())
            ->setId($id)
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model->generateAccountId();
        $this->assertEquals($id, $model->getId());
    }
}