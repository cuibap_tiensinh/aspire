<?php declare(strict_types=1);

namespace Tests\Unit\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerRepaymentHistory;
use App\Core\Domain\ValueObjects\CurrencyUnit;

class CustomerRepaymentHistoryTest extends  \AbstractUnitTest
{
    public function testGenerateHistoryIdWhenEmpty()
    {
        $model = (new CustomerRepaymentHistory())
            ->setCustomerId("123456")
            ->setBankAccountId("123456")
            ->setLoanAccountId("123456")
            ->setRepaidAmount(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setRemainingDueAmountInPeriod(12121.21)
            ->setVersion(1)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->assertTrue(empty($model->getId()));

        $model->generateHistoryIdIfEmpty();
        $this->assertEquals($model->getLoanAccountId() . "-" . $model->getVersion(), $model->getId());
    }

    public function testGenerateHistoryIdWhenEmptyNotEmpty()
    {
        $id = "1234567";
        $model = (new CustomerRepaymentHistory())
            ->setId($id)
            ->setCustomerId("123456")
            ->setBankAccountId("123456")
            ->setLoanAccountId("123456")
            ->setRepaidAmount(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setRemainingDueAmountInPeriod(12121.21)
            ->setVersion(1)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model->generateHistoryIdIfEmpty();
        $this->assertEquals($id, $model->getId());
    }
}