<?php declare(strict_types=1);

namespace Tests\Unit\Core\Domain\Models;

use App\Core\Domain\Models\CustomerSecurity;

class CustomerSecurityTest extends \AbstractUnitTest
{
    public function testBuildAccountTokenIfEmptyWhenEmpty()
    {
        $username = "123456";
        $password = "zxcvbnm";

        $model = (new CustomerSecurity())
            ->setId("123456")
            ->setUsername($username)
            ->setPassword($password);
        $model->buildAccountTokenIfEmpty();

        $this->assertEquals(md5($model->getUsername() . "|" . $model->getPassword()), $model->getAccountToken());
    }

    public function testBuildAccountTokenIfEmptyWhenNotEmpty()
    {
        $username = "123456";
        $password = "zxcvbnm";

        $model = (new CustomerSecurity())
            ->setId("123456")
            ->setUsername($username)
            ->setPassword($password)
            ->setAccountToken("123456");
        $model->buildAccountTokenIfEmpty();

        $this->assertEquals("123456", $model->getAccountToken());
    }

    public function testGetAccessTokenData()
    {
        $username = "123456";
        $password = "zxcvbnm";

        $model = (new CustomerSecurity())
            ->setId("123456")
            ->setUsername($username)
            ->setPassword($password)
            ->setAccountToken("123456");

        $accessTokenData = $model->getAccessTokenData();
        $this->assertTrue(is_array($accessTokenData));
        $this->assertTrue(isset($accessTokenData['id']) && $accessTokenData['id'] == $model->getId());
        $this->assertTrue(isset($accessTokenData['token']) && $accessTokenData['id'] == $model->getAccountToken());
        $this->assertTrue(isset($accessTokenData['expired_at']));
    }
}