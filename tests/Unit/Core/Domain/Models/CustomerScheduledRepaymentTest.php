<?php declare(strict_types=1);

namespace Tests\Unit\Core\Domain\Models;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Core\Domain\ValueObjects\RepaymentStatus;

class CustomerScheduledRepaymentTest extends \AbstractUnitTest
{
    public function testGenerateScheduleIdWhenEmpty()
    {
        $model = (new CustomerScheduledRepayment())
            ->setCustomerId("123456")
            ->setLoanAccountId("123456")
            ->setDueAmount(9999.1)
            ->setCurrency(CurrencyUnit::USD)
            ->setDueAt(DateTimeUtils::today())
            ->setStatus(RepaymentStatus::PENDING)
            ->setVersion(1)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $this->assertTrue(empty($model->getId()));

        $idx = 100;
        $expectedId = $model->getLoanAccountId() .
            "-" . $model->getDueAt()->format("Ymd") .
            "-" . ($idx + 1);

        $model->generateScheduleIdIfEmpty($idx);
        $this->assertEquals($expectedId, $model->getId());
    }

    public function testGenerateScheduleIdWhenNotEmpty()
    {
        $id = "123456";
        $model = (new CustomerScheduledRepayment())
            ->setId($id)
            ->setCustomerId("123456")
            ->setLoanAccountId("123456")
            ->setDueAmount(9999.1)
            ->setCurrency(CurrencyUnit::USD)
            ->setDueAt(DateTimeUtils::today())
            ->setStatus(RepaymentStatus::PENDING)
            ->setVersion(1)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());


        $idx = 100;
        $model->generateScheduleIdIfEmpty($idx);
        $this->assertEquals($id, $model->getId());
    }
}