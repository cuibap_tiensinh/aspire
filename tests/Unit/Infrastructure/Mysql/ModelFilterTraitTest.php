<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Core\Domain\ValueObjects\LoanStatus;
use App\Infrastructure\Mysql\ModelFilterTrait;
use Phalcon\Db\Column;

class ModelFilterTraitTest extends \AbstractUnitTest
{
    use ModelFilterTrait;

    public function testBuildModelQuery()
    {
        $array = [
            'customer_id' => "123456",
            'status' => LoanStatus::APPROVED,
            'amount' => 5.6,
            'version' => 1,
        ];

        $expectedCondition = "customer_id = :customer_id: AND status = :status: AND amount = :amount: AND version = :version:";
        $expectedBind = [
            'customer_id' => "123456",
            'status' => LoanStatus::APPROVED,
            'amount' => 5.6,
            'version' => 1,
        ];
        $expectedBindTypes = [
            'customer_id' => Column::BIND_PARAM_STR,
            'status' => Column::BIND_PARAM_STR,
            'amount' => Column::BIND_PARAM_INT,
            'version' => Column::BIND_PARAM_INT,
        ];

        $this->assertEquals($expectedCondition, $this->buildModelQuery($array)[0]);
        $this->assertEquals($expectedBind, $this->buildModelQuery($array)[1]);
        $this->assertEquals($expectedBindTypes, $this->buildModelQuery($array)[2]);

        $this->assertEquals("", $this->buildModelQuery([])[0]);
        $this->assertEquals([], $this->buildModelQuery([])[1]);
        $this->assertEquals([], $this->buildModelQuery([])[2]);
    }
}