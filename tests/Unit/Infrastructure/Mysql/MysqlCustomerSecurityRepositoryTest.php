<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Core\Domain\Models\CustomerSecurity;
use App\Infrastructure\Mysql\Mapper\CustomerSecurityRecordMapper;
use App\Infrastructure\Mysql\MysqlCustomerSecurityRepository;

class MysqlCustomerSecurityRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new CustomerSecurity())
            ->setId("123456")
            ->setUsername("123456")
            ->setPassword("12345678")
            ->setAccountToken(md5("123456"));

        $mysqlRepository = new MysqlCustomerSecurityRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $model->setUsername("000000");
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $this->assertEquals($model, $fetchedModel);

        $record = (new CustomerSecurityRecordMapper())->toRecord($model);
        $record->delete();
    }
}