<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Core\Domain\ValueObjects\RepaymentStatus;
use App\Core\Domain\ValueObjects\RepaymentType;
use App\Infrastructure\Mysql\Mapper\CustomerScheduledRepaymentRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerScheduledRepaymentRecord;

class CustomerScheduledRepaymentRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new CustomerScheduledRepayment())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setLoanAccountId("123456")
            ->setDueAmount(9999.1)
            ->setCurrency(CurrencyUnit::USD)
            ->setDueAt(DateTimeUtils::today())
            ->setStatus(RepaymentStatus::PENDING)
            ->setVersion(1)
            ->setRepaidType(RepaymentType::TYPE_NONE)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $record = (new CustomerScheduledRepaymentRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setLoanAccountId($model->getLoanAccountId())
            ->setDueAmount($model->getDueAmount())
            ->setCurrency($model->getCurrency())
            ->setDueAt($model->getDueAt())
            ->setStatus($model->getStatus())
            ->setVersion($model->getVersion())
            ->setRepaidType($model->getRepaidType())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());

        $mapper = new CustomerScheduledRepaymentRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerScheduledRepaymentRecord())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setLoanAccountId("123456")
            ->setDueAmount(9999.1)
            ->setCurrency(CurrencyUnit::USD)
            ->setDueAt(DateTimeUtils::today())
            ->setStatus(RepaymentStatus::PENDING)
            ->setVersion(1)
            ->setRepaidType(RepaymentType::TYPE_NONE)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model = (new CustomerScheduledRepayment())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setLoanAccountId($record->getLoanAccountId())
            ->setDueAmount($record->getDueAmount())
            ->setCurrency($record->getCurrency())
            ->setDueAt($record->getDueAtAsDateTime())
            ->setStatus($record->getStatus())
            ->setVersion($record->getVersion())
            ->setRepaidType($record->getRepaidType())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime());

        $mapper = new CustomerScheduledRepaymentRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}