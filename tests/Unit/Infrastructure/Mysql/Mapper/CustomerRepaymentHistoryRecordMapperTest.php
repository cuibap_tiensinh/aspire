<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerRepaymentHistory;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Core\Domain\ValueObjects\RepaymentType;
use App\Infrastructure\Mysql\Mapper\CustomerRepaymentHistoryRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerRepaymentHistoryRecord;

class CustomerRepaymentHistoryRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new CustomerRepaymentHistory())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBankAccountId("123456")
            ->setLoanAccountId("123456")
            ->setRepaidAmount(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setRemainingDueAmountInPeriod(12121.21)
            ->setVersion(1)
            ->setRepaidType(RepaymentType::TYPE_CASH_IN)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $record = (new CustomerRepaymentHistoryRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setBankAccountId($model->getBankAccountId())
            ->setLoanAccountId($model->getLoanAccountId())
            ->setRepaidAmount($model->getRepaidAmount())
            ->setRemainingDueAmountInPeriod($model->getRemainingDueAmountInPeriod())
            ->setCurrency($model->getCurrency())
            ->setVersion($model->getVersion())
            ->setRepaidType($model->getRepaidType())
            ->setCreatedAt($model->getCreatedAt());

        $mapper = new CustomerRepaymentHistoryRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerRepaymentHistoryRecord())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBankAccountId("123456")
            ->setLoanAccountId("123456")
            ->setRepaidAmount(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setRemainingDueAmountInPeriod(12121.21)
            ->setVersion(1)
            ->setRepaidType(RepaymentType::TYPE_CASH_IN)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model = (new CustomerRepaymentHistory())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setBankAccountId($record->getBankAccountId())
            ->setLoanAccountId($record->getLoanAccountId())
            ->setRepaidAmount($record->getRepaidAmount())
            ->setRemainingDueAmountInPeriod($record->getRemainingDueAmountInPeriod())
            ->setCurrency($record->getCurrency())
            ->setVersion($record->getVersion())
            ->setRepaidType($record->getRepaidType())
            ->setCreatedAt($record->getCreatedAtAsDateTime());

        $mapper = new CustomerRepaymentHistoryRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}