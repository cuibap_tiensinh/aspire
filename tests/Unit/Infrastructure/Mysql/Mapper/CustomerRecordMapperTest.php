<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\Customer;
use App\Infrastructure\Mysql\Mapper\CustomerRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerRecord;

class CustomerRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new Customer())
            ->setId("123456")
            ->setCustomerName("Tester")
            ->setCustomerPhone("(+1)12345678")
            ->setCustomerEmail("abc@gmail.com")
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $record = (new CustomerRecord())
            ->setId($model->getId())
            ->setCustomerName($model->getCustomerName())
            ->setCustomerPhone($model->getCustomerPhone())
            ->setCustomerEmail($model->getCustomerEmail())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());

        $mapper = new CustomerRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerRecord())
            ->setId("123456")
            ->setCustomerName("Tester")
            ->setCustomerPhone("(+1)12345678")
            ->setCustomerEmail("abc@gmail.com")
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model = (new Customer())
            ->setId($record->getId())
            ->setCustomerName($record->getCustomerName())
            ->setCustomerPhone($record->getCustomerPhone())
            ->setCustomerEmail($record->getCustomerEmail())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime());

        $mapper = new CustomerRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}