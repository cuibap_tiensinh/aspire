<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerBalanceHistory;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Infrastructure\Mysql\Mapper\CustomerBalanceHistoryRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerBalanceHistoryRecord;

class CustomerBalanceHistoryRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new CustomerBalanceHistory())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setVersion(1)
            ->setBankAccountId("123456")
            ->setAccountCurrency("EUR")
            ->setWithdrawAmount(910.3)
            ->setDepositAmount(910.3)
            ->setExchangeRate(9.153)
            ->setSubmittedCurrency(CurrencyUnit::USD)
            ->setSubmittedWithdrawAmount(1000.1)
            ->setSubmittedDepositAmount(10000.1)
            ->setReason("Unit Test")
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $record = (new CustomerBalanceHistoryRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setVersion($model->getVersion())
            ->setBankAccountId($model->getBankAccountId())
            ->setAccountCurrency($model->getAccountCurrency())
            ->setWithdrawAmount($model->getWithdrawAmount())
            ->setDepositAmount($model->getDepositAmount())
            ->setExchangeRate($model->getExchangeRate())
            ->setSubmittedCurrency($model->getSubmittedCurrency())
            ->setSubmittedWithdrawAmount($model->getSubmittedWithdrawAmount())
            ->setSubmittedDepositAmount($model->getSubmittedDepositAmount())
            ->setReason($model->getReason())
            ->setCreatedAt($model->getCreatedAt());

        $mapper = new CustomerBalanceHistoryRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerBalanceHistoryRecord())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setVersion(1)
            ->setBankAccountId("123456")
            ->setAccountCurrency("EUR")
            ->setWithdrawAmount(910.3)
            ->setDepositAmount(910.3)
            ->setExchangeRate(9.153)
            ->setSubmittedCurrency(CurrencyUnit::USD)
            ->setSubmittedWithdrawAmount(1000.1)
            ->setSubmittedDepositAmount(10000.1)
            ->setReason("Unit Test")
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model = (new CustomerBalanceHistory())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setVersion($record->getVersion())
            ->setBankAccountId($record->getBankAccountId())
            ->setAccountCurrency($record->getAccountCurrency())
            ->setWithdrawAmount($record->getWithdrawAmount())
            ->setDepositAmount($record->getDepositAmount())
            ->setExchangeRate($record->getExchangeRate())
            ->setSubmittedCurrency($record->getSubmittedCurrency())
            ->setSubmittedWithdrawAmount($record->getSubmittedWithdrawAmount())
            ->setSubmittedDepositAmount($record->getSubmittedDepositAmount())
            ->setReason($record->getReason())
            ->setCreatedAt($record->getCreatedAtAsDateTime());

        $mapper = new CustomerBalanceHistoryRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}