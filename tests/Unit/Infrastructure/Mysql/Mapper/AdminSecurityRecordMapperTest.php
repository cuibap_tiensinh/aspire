<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Core\Domain\Models\AdminSecurity;
use App\Infrastructure\Mysql\Mapper\AdminSecurityRecordMapper;
use App\Infrastructure\Mysql\Record\AdminSecurityRecord;

class AdminSecurityRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new AdminSecurity())
            ->setId("123456")
            ->setUsername("123456")
            ->setPassword("12345678")
            ->setAccountToken(md5("123456"));

        $record = (new AdminSecurityRecord())
            ->setId($model->getId())
            ->setUsername($model->getUsername())
            ->setPassword($model->getPassword())
            ->setAccountToken($model->getAccountToken());

        $mapper = new AdminSecurityRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new AdminSecurityRecord())
            ->setId("123456")
            ->setUsername("123456")
            ->setPassword("12345678")
            ->setAccountToken(md5("123456"));

        $model = (new AdminSecurity())
            ->setId($record->getId())
            ->setUsername($record->getUsername())
            ->setPassword($record->getPassword())
            ->setAccountToken($record->getAccountToken());

        $mapper = new AdminSecurityRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}