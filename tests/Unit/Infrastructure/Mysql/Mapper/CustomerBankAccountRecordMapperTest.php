<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Infrastructure\Mysql\Mapper\CustomerBankAccountRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerBankAccountRecord;

class CustomerBankAccountRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new CustomerBankAccount())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $record = (new CustomerBankAccountRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setBalance($model->getBalance())
            ->setCurrency($model->getCurrency())
            ->setVersion($model->getVersion())
            ->setIsPrimary($model->getIsPrimary())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt());

        $mapper = new CustomerBankAccountRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerBankAccountRecord())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $model = (new CustomerBankAccount())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setBalance($record->getBalance())
            ->setCurrency($record->getCurrency())
            ->setVersion($record->getVersion())
            ->setIsPrimary($record->getIsPrimary())
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime());

        $mapper = new CustomerBankAccountRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}