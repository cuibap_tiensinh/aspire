<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Core\Domain\ValueObjects\LoanStatus;
use App\Infrastructure\Mysql\Mapper\CustomerLoanRecordMapper;
use App\Infrastructure\Mysql\Record\CustomerLoanRecord;

class CustomerLoanRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::APPROVED)
            ->setLoanTerm(3)
            ->setApprovedBy("System")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.999999)
            ->setRepaidCount(1);

        $record = (new CustomerLoanRecord())
            ->setId($model->getId())
            ->setCustomerId($model->getCustomerId())
            ->setAmount($model->getBalance())
            ->setCurrency($model->getCurrency())
            ->setCreatedAt($model->getCreatedAt())
            ->setUpdatedAt($model->getUpdatedAt())
            ->setStatus($model->getStatus())
            ->setLoanTerm($model->getLoanTerm())
            ->setApprovedBy($model->getApprovedBy())
            ->setStartedAt($model->getStartedAt())
            ->setEndedAt($model->getEndedAt())
            ->setScheduledAt($model->getScheduledAt())
            ->setRepaidAmount($model->getRepaidAmount())
            ->setRepaidCount($model->getRepaidCount());

        $mapper = new CustomerLoanRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerLoanRecord())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setAmount(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::APPROVED)
            ->setLoanTerm(3)
            ->setApprovedBy("System")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.999999)
            ->setRepaidCount(1);

        $model = (new CustomerLoan())
            ->setId($record->getId())
            ->setCustomerId($record->getCustomerId())
            ->setBalance($record->getAmount())
            ->setCurrency($record->getCurrency())
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt($record->getCreatedAtAsDateTime())
            ->setUpdatedAt($record->getUpdatedAtAsDateTime())
            ->setStatus($record->getStatus())
            ->setLoanTerm($record->getLoanTerm())
            ->setApprovedBy($record->getApprovedBy())
            ->setStartedAt($record->getStartedAtAsDateTime())
            ->setEndedAt($record->getEndedAtAsDateTime())
            ->setScheduledAt($record->getScheduledAtAsDateTime())
            ->setRepaidAmount($record->getRepaidAmount())
            ->setRepaidCount($record->getRepaidCount());

        $mapper = new CustomerLoanRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}