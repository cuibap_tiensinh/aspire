<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Mapper;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\Customer;
use App\Core\Domain\Models\CustomerSecurity;
use App\Infrastructure\Mysql\Mapper\CustomerSecurityRecordMapper;
use App\Infrastructure\Mysql\Mapper\MapperBase;
use App\Infrastructure\Mysql\Record\CustomerRecord;
use App\Infrastructure\Mysql\Record\CustomerSecurityRecord;

class CustomerSecurityRecordMapperTest extends \AbstractUnitTest
{
    public function testToRecord()
    {
        $model = (new CustomerSecurity())
            ->setId("123456")
            ->setUsername("Tester")
            ->setPassword("123456")
            ->setAccountToken("123456");

        $record = (new CustomerSecurityRecord())
            ->setId($model->getId())
            ->setUsername($model->getUsername())
            ->setPassword($model->getPassword())
            ->setAccountToken($model->getAccountToken());

        $mapper = new CustomerSecurityRecordMapper();
        $this->assertEquals($record, $mapper->toRecord($model));
    }

    public function testToModel()
    {
        $record = (new CustomerSecurityRecord())
            ->setId("123456")
            ->setUsername("Tester")
            ->setPassword("123456")
            ->setAccountToken("123456");

        $model = (new CustomerSecurity())
            ->setId($record->getId())
            ->setUsername($record->getUsername())
            ->setPassword($record->getPassword())
            ->setAccountToken($record->getAccountToken());

        $mapper = new CustomerSecurityRecordMapper();
        $this->assertEquals($model, $mapper->toModel($record));
    }
}