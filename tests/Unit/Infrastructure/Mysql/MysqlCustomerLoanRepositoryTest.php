<?php

namespace Tests\Unit\Infrastructure\Mysql;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerLoan;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Core\Domain\ValueObjects\LoanStatus;
use App\Infrastructure\Mysql\Mapper\CustomerLoanRecordMapper;
use App\Infrastructure\Mysql\Mapper\MapperBase;
use App\Infrastructure\Mysql\MysqlCustomerLoanRepository;

class MysqlCustomerLoanRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::APPROVED)
            ->setLoanTerm(3)
            ->setApprovedBy("Tester")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.9999)
            ->setRepaidCount(1);

        $mysqlRepository = new MysqlCustomerLoanRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $model->setApprovedBy("System")
            ->setStatus(LoanStatus::PENDING);
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $this->assertEquals($model, $fetchedModel);

        $record = (new CustomerLoanRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testFetchOneAndCustomer()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::APPROVED)
            ->setLoanTerm(3)
            ->setApprovedBy("Tester")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.9999)
            ->setRepaidCount(1);

        $mysqlRepository = new MysqlCustomerLoanRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOneAndCustomer($model->getId(), $model->getCustomerId());
        $this->assertEquals($model, $fetchedModel);

        $fetchedModel = $mysqlRepository->fetchOneAndCustomer($model->getId(), "test0000000000000000");
        $this->assertEquals(null, $fetchedModel);

        $record = (new CustomerLoanRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testFetchAllByCustomer()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::PENDING)
            ->setLoanTerm(3)
            ->setApprovedBy("Tester")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.9999)
            ->setRepaidCount(1);

        $mysqlRepository = new MysqlCustomerLoanRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $result = $mysqlRepository->fetchAllByCustomer($model->getCustomerId());
        $this->assertTrue(count($result) == 1);

        $result = $mysqlRepository->fetchAllByCustomer("test000000000000");
        $this->assertTrue(count($result) == 0);

        $record = (new CustomerLoanRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testIsExistedPendingLoanByCustomer()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::PENDING)
            ->setLoanTerm(3)
            ->setApprovedBy("Tester")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.9999)
            ->setRepaidCount(1);

        $mysqlRepository = new MysqlCustomerLoanRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $result = $mysqlRepository->isExistedPendingLoanByCustomer($model->getCustomerId());
        $this->assertTrue($result);

        $result = $mysqlRepository->isExistedPendingLoanByCustomer("test0000000000000000");
        $this->assertFalse($result);

        $model->setStatus(LoanStatus::APPROVED);
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $result = $mysqlRepository->isExistedPendingLoanByCustomer($model->getCustomerId());
        $this->assertFalse($result);

        $result = $mysqlRepository->isExistedPendingLoanByCustomer("test0000000000000000");
        $this->assertFalse($result);

        $record = (new CustomerLoanRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testCountByFilter()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::APPROVED)
            ->setLoanTerm(3)
            ->setApprovedBy("Tester")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.9999)
            ->setRepaidCount(1);

        $mysqlRepository = new MysqlCustomerLoanRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $filter = [
            'customer_id' => $model->getCustomerId(),
            'status' => LoanStatus::APPROVED
        ];

        $result = $mysqlRepository->countByFilter($filter);
        $this->assertTrue($result == 1);

        $filter = [
            'customer_id' => "test0000000000000000"
        ];
        $result = $mysqlRepository->countByFilter($filter);
        $this->assertTrue($result == 0);

        $record = (new CustomerLoanRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testFetchAllByFilter()
    {
        $model = (new CustomerLoan())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(false)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setStatus(LoanStatus::APPROVED)
            ->setLoanTerm(3)
            ->setApprovedBy("Tester")
            ->setStartedAt(DateTimeUtils::today())
            ->setEndedAt(DateTimeUtils::today())
            ->setScheduledAt(DateTimeUtils::today())
            ->setRepaidAmount(1000.9999)
            ->setRepaidCount(1);

        $mysqlRepository = new MysqlCustomerLoanRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $filter = [
            'customer_id' => $model->getCustomerId(),
            'status' => LoanStatus::APPROVED
        ];

        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 0);
        $this->assertTrue(count($result) == 1);

        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 1);
        $this->assertTrue(count($result) == 0);

        $filter = [
            'customer_id' => "test0000000000000000"
        ];
        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 0);
        $this->assertTrue(count($result) == 0);

        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 1);
        $this->assertTrue(count($result) == 0);

        $record = (new CustomerLoanRecordMapper())->toRecord($model);
        $record->delete();
    }
}