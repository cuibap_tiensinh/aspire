<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerBankAccount;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Infrastructure\Mysql\Mapper\CustomerBankAccountRecordMapper;
use App\Infrastructure\Mysql\MysqlCustomerBankAccountRepository;

class MysqlCustomerBankAccountRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new CustomerBankAccount())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerBankAccountRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $model->setBalance(9.8888);
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $model->setVersion($model->getVersion() + 1);
        $this->assertEquals($model, $fetchedModel);

        $record = (new CustomerBankAccountRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testFetchOneAndCustomer()
    {
        $model = (new CustomerBankAccount())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerBankAccountRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOneAndCustomer($model->getId(), $model->getCustomerId());
        $this->assertEquals($model, $fetchedModel);

        $fetchedModel = $mysqlRepository->fetchOneAndCustomer($model->getId(), "test0000000000000000");
        $this->assertEquals(null, $fetchedModel);

        $record = (new CustomerBankAccountRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testIsExistByCustomerId()
    {
        $model = (new CustomerBankAccount())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerBankAccountRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $result = $mysqlRepository->isExistByCustomerId($model->getCustomerId());
        $this->assertTrue($result);

        $result = $mysqlRepository->isExistByCustomerId("test0000000000000000");
        $this->assertFalse($result);

        $record = (new CustomerBankAccountRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testCountByFilter()
    {
        $model = (new CustomerBankAccount())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerBankAccountRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $filter = [
            'customer_id' => $model->getCustomerId()
        ];

        $result = $mysqlRepository->countByFilter($filter);
        $this->assertTrue($result == 1);

        $filter = [
            'customer_id' => "test0000000000000000"
        ];
        $result = $mysqlRepository->countByFilter($filter);
        $this->assertTrue($result == 0);

        $record = (new CustomerBankAccountRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testFetchAllByFilter()
    {
        $model = (new CustomerBankAccount())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBalance(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setVersion(1)
            ->setIsPrimary(true)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerBankAccountRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $filter = [
            'customer_id' => $model->getCustomerId()
        ];

        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 0);
        $this->assertTrue(count($result) == 1);

        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 1);
        $this->assertTrue(count($result) == 0);

        $filter = [
            'customer_id' => "test0000000000000000"
        ];
        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 0);
        $this->assertTrue(count($result) == 0);

        $result = $mysqlRepository->fetchAllByFilter($filter, 10, 1);
        $this->assertTrue(count($result) == 0);

        $record = (new CustomerBankAccountRecordMapper())->toRecord($model);
        $record->delete();
    }
}