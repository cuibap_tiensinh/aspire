<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Record;

use App\Common\Utils\DateTimeUtils;
use App\Infrastructure\Mysql\Record\CustomerBalanceHistoryRecord;
use DateTime;

class CustomerBalanceHistoryRecordTest extends \AbstractUnitTest
{
    public function testGetSetCreatedAt()
    {
        $sDateTime = "2024-01-01 00:00:00";
        $dateTime = new DateTime($sDateTime);

        $defaultDateTime = DateTimeUtils::convertToDefaultTimezone($dateTime);
        $sDefaultDateTime = $defaultDateTime->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);

        $record = (new CustomerBalanceHistoryRecord())
            ->setCreatedAt($dateTime);

        $this->assertEquals($defaultDateTime, $record->getCreatedAtAsDateTime());
        $this->assertEquals($sDefaultDateTime, $record->getCreatedAt());

        $record->setCreatedAt(null);
        $this->assertEquals(null, $record->getCreatedAtAsDateTime());
        $this->assertEquals(null, $record->getCreatedAt());
    }
}