<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql\Record;


use App\Common\Utils\DateTimeUtils;
use App\Infrastructure\Mysql\Record\CustomerRecord;
use DateTime;

class CustomerRecordTest extends \AbstractUnitTest
{
    public function testGetSetCreatedAt()
    {
        $sDateTime = "2024-01-01 00:00:00";
        $dateTime = new DateTime($sDateTime);

        $defaultDateTime = DateTimeUtils::convertToDefaultTimezone($dateTime);
        $sDefaultDateTime = $defaultDateTime->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);

        $record = (new CustomerRecord())
            ->setCreatedAt($dateTime);

        $this->assertEquals($defaultDateTime, $record->getCreatedAtAsDateTime());
        $this->assertEquals($sDefaultDateTime, $record->getCreatedAt());

        $record->setCreatedAt(null);
        $this->assertEquals(null, $record->getCreatedAtAsDateTime());
        $this->assertEquals(null, $record->getCreatedAt());
    }

    public function testGetSetUpdatedAt()
    {
        $sDateTime = "2024-01-01 00:00:00";
        $dateTime = new DateTime($sDateTime);

        $defaultDateTime = DateTimeUtils::convertToDefaultTimezone($dateTime);
        $sDefaultDateTime = $defaultDateTime->format(DateTimeUtils::ASPIRE_DATETIME_FORMAT);

        $record = (new CustomerRecord())
            ->setUpdatedAt($dateTime);

        $this->assertEquals($defaultDateTime, $record->getUpdatedAtAsDateTime());
        $this->assertEquals($sDefaultDateTime, $record->getUpdatedAt());

        $record->setUpdatedAt(null);
        $this->assertEquals(null, $record->getUpdatedAtAsDateTime());
        $this->assertEquals(null, $record->getUpdatedAt());
    }
}