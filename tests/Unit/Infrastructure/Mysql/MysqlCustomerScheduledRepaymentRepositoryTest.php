<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerScheduledRepayment;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Core\Domain\ValueObjects\RepaymentStatus;
use App\Infrastructure\Mysql\Mapper\CustomerScheduledRepaymentRecordMapper;
use App\Infrastructure\Mysql\MysqlCustomerScheduledRepaymentRepository;
use App\Infrastructure\Mysql\Record\CustomerScheduledRepaymentRecord;

class MysqlCustomerScheduledRepaymentRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new CustomerScheduledRepayment())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setLoanAccountId("123456")
            ->setDueAmount(9999.1)
            ->setCurrency(CurrencyUnit::USD)
            ->setDueAt(DateTimeUtils::today())
            ->setStatus(RepaymentStatus::PENDING)
            ->setVersion(1)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
            ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerScheduledRepaymentRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchFirstUnpaidSchedule($model->getLoanAccountId(), $model->getCustomerId());
        $this->assertEquals($model, $fetchedModel);

        $fetchedModel = $mysqlRepository->fetchFirstUnpaidSchedule($model->getLoanAccountId(), "test0000000000");
        $this->assertEquals(null, $fetchedModel);

        $model->setStatus(RepaymentStatus::PAID);
        $model->setVersion($model->getVersion() + 1);
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $this->assertEquals($model, $fetchedModel);

        $fetchedModel = $mysqlRepository->fetchFirstUnpaidSchedule($model->getLoanAccountId(), $model->getCustomerId());
        $this->assertEquals(null, $fetchedModel);

        $record = (new CustomerScheduledRepaymentRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testInsertMany()
    {
        $models = [];
        $customerId = "test00000000000000";
        $batchLength = 5;

        for ($i = 0; $i < 5; $i++) {
            $models[] = (new CustomerScheduledRepayment())
                ->setId("123456" . $i)
                ->setCustomerId($customerId)
                ->setLoanAccountId("123456")
                ->setDueAmount(9999.1)
                ->setCurrency(CurrencyUnit::USD)
                ->setDueAt(DateTimeUtils::today())
                ->setStatus(RepaymentStatus::PENDING)
                ->setVersion(1)
                ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec())
                ->setUpdatedAt(DateTimeUtils::nowWithoutMilliSec());
        }

        $mysqlRepository = new MysqlCustomerScheduledRepaymentRepository();
        $result = $mysqlRepository->insertMany($models);
        $this->assertTrue($result);

        $count = CustomerScheduledRepaymentRecord::countByCustomerId($customerId);
        $this->assertTrue($count == $batchLength);

        $mapper = new CustomerScheduledRepaymentRecordMapper();
        foreach ($models as $model) {
            $record = $mapper->toRecord($model);
            $record->delete();
        }
    }
}