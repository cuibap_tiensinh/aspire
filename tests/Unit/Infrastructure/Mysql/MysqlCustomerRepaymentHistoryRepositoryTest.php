<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerRepaymentHistory;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Infrastructure\Mysql\Mapper\CustomerRepaymentHistoryRecordMapper;
use App\Infrastructure\Mysql\MysqlCustomerRepaymentHistoryRepository;

class MysqlCustomerRepaymentHistoryRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new CustomerRepaymentHistory())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setBankAccountId("123456")
            ->setLoanAccountId("123456")
            ->setRepaidAmount(9999.2345)
            ->setCurrency(CurrencyUnit::USD)
            ->setRemainingDueAmountInPeriod(12121.21)
            ->setVersion(1)
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerRepaymentHistoryRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $model->setRepaidAmount(0);
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $this->assertEquals($model, $fetchedModel);

        $record = (new CustomerRepaymentHistoryRecordMapper())->toRecord($model);
        $record->delete();
    }
}