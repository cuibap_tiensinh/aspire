<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Core\Domain\Models\AdminSecurity;
use App\Infrastructure\Mysql\Mapper\AdminSecurityRecordMapper;
use App\Infrastructure\Mysql\MysqlAdminSecurityRepository;

class MysqlAdminSecurityRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new AdminSecurity())
            ->setId("123456")
            ->setUsername("123456")
            ->setPassword("12345678")
            ->setAccountToken(md5("123456"));

        $mysqlRepository = new MysqlAdminSecurityRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $model->setUsername("000000");
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $this->assertEquals($model, $fetchedModel);

        $record = (new AdminSecurityRecordMapper())->toRecord($model);
        $record->delete();
    }

    public function testFetchOneByUsername()
    {
        $model = (new AdminSecurity())
            ->setId("123456")
            ->setUsername("123456")
            ->setPassword("12345678")
            ->setAccountToken(md5("123456"));

        $mysqlRepository = new MysqlAdminSecurityRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);;

        $fetchedModel = $mysqlRepository->fetchOneByUsername($model->getUsername());
        $this->assertEquals($model, $fetchedModel);

        $record = (new AdminSecurityRecordMapper())->toRecord($model);
        $record->delete();
    }
}