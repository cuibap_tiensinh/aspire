<?php declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Mysql;

use App\Common\Utils\DateTimeUtils;
use App\Core\Domain\Models\CustomerBalanceHistory;
use App\Core\Domain\ValueObjects\CurrencyUnit;
use App\Infrastructure\Mysql\Mapper\CustomerBalanceHistoryRecordMapper;
use App\Infrastructure\Mysql\MysqlCustomerBalanceHistoryRepository;

class MysqlCustomerBalanceHistoryRepositoryTest extends \AbstractUnitTest
{
    public function testInsertUpdateSelectDelete()
    {
        $model = (new CustomerBalanceHistory())
            ->setId("123456")
            ->setCustomerId("123456")
            ->setVersion(1)
            ->setBankAccountId("123456")
            ->setAccountCurrency("EUR")
            ->setWithdrawAmount(910.3)
            ->setDepositAmount(910.3)
            ->setExchangeRate(9.153)
            ->setSubmittedCurrency(CurrencyUnit::USD)
            ->setSubmittedWithdrawAmount(1000.1)
            ->setSubmittedDepositAmount(10000.1)
            ->setReason("Unit Test")
            ->setCreatedAt(DateTimeUtils::nowWithoutMilliSec());

        $mysqlRepository = new MysqlCustomerBalanceHistoryRepository();
        $result = $mysqlRepository->insert($model);
        $this->assertTrue($result);

        $model->setReason("000000");
        $result = $mysqlRepository->update($model);
        $this->assertTrue($result);

        $fetchedModel = $mysqlRepository->fetchOne($model->getId());
        $this->assertEquals($model, $fetchedModel);

        $record = (new CustomerBalanceHistoryRecordMapper())->toRecord($model);
        $record->delete();
    }
}