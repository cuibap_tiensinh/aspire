<?php

defined('ROOT_PATH') || define('ROOT_PATH', dirname(__DIR__));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', ROOT_PATH . '/src');
defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production');

define('MODULE_NAME', 'Api');
define('MODULE_PATH', APPLICATION_PATH . '/Application/' . MODULE_NAME);
define('CONFIG_PATH', MODULE_PATH . '/Configs');

require MODULE_PATH . '/bootstrap-loader.php';
